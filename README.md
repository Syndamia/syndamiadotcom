# syndamiadotcom

Repository for my website.
Pages are written in Markdown, converted to HTML with [ssg](https://www.romanzolotarev.com/ssg.html) and [lowdown](https://kristaps.bsd.lv/lowdown/), and edited with AWK scripts.
Currently, all content is static.

For more information on how it works, refer to [S.S. Law](https://syndamia.gitlab.io/ss-law/).

## Layout and personal setup

- `.hooks`: Scripts which are executed before git commit.
  + `pre-commit.sh`: Most (blog, tutorial, ...) pages are accessed without the ending `.html`, which is done by moving the contents of `page.html` to `page/index.html`.
    I would still like for certain pages to be accessible also with `page.html`, so I create a hardlink from one to the other.
    However, git (when pulling) removes the appropriate hardlink and the files aren't "synced", this script brings back those hardlinks.
- `awk-edits`: After HTML generation of pages, AWK scripts do modifications.
  For more information, read `awk-edits/README.md`.
- `src`: All website content
- `generate.sh`: Generates the whole website.
  + You can give it arguments:
    + With no argument, the website is created in a "production" way.
    + With the `dev` parameter (`./generate.sh dev`), you'll get duration information for different stuff.
    + With the `devs` argument, you'll get duration information, but AWK scripts will be ran individually, rather than being combined (useful for individual performance evaluation; name comes from dev + separated)
  + There are some variables which handle how the script works:
    + `REPO_FOLDER`: specifies where the repository is, useful if you want to run the script outside of the repo.
      awk-edits and src folders are always relative to the repo folder.
    + `GEN_FOLDER`: folder in which to put the final generated webpages
    + `GEN_TINY_FOLDER`: folder in which to put the webpages, that have removed features for better accessibility
    + `AWK_FILE`: where to put the temporary AWK script, which is created by combining all awk-edits scripts into one.
      Obviously doesn't do anything with `devs` parameter.
- `install-ssg-lowdown.sh`: Installs ssg and lowdown on the current system.
