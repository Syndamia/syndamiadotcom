#!/bin/sh

cd /tmp

sudo apt install -y curl git make gcc 

echo "Installing ssg (latest version)"

curl -s https://rgz.ee/bin/ssg > ssg
chmod +x ssg
sudo mv ssg /usr/bin/

echo "Installing lowdown"

if [ -d "lowdown" ]; then
  sudo rm -rf lowdown
fi
git clone https://github.com/kristapsdz/lowdown
cd lowdown/
sudo ./configure
sudo make
sudo make install

echo "Done"

