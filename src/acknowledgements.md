# Acknowledgements

[tabler-icons](https://github.com/tabler/tabler-icons), licensed under [MIT](https://github.com/tabler/tabler-icons/blob/master/LICENSE)
: All UI icons

[github-markdown-css](https://github.com/sindresorhus/github-markdown-css), licensed under [MIT](https://github.com/sindresorhus/github-markdown-css/blob/main/license)
: Some of the styling, especially for tables and code/pre tags

[flag-icons](https://github.com/lipis/flag-icons), licensed under [MIT](https://github.com/lipis/flag-icons/blob/main/LICENSE)
: Icons of flags

[mint-y-icons](https://github.com/linuxmint/mint-y-icons), licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
: Certain icons in articles/pages

## Special thanks to

[danuluu's programming blogs list](https://danluu.com/programming-blogs/)
: For the excellent list of awesome blogs, from which I drew a good amount of inspiration.

[Aleksey Shipilëv's](https://shipilev.net/), [Nikita's](https://tonsky.me/), [Julia Evan's](https://jvns.ca/) and [Haelwenn Monnier's](https://hacktivis.me) blogs
: From which I drew the most inspiration

[Peter Pentchev's personal page](http://www.ringlet.net/roam/)
: For igniting my love for website badges and the somewhat rare "Created with Vim" badge

[Cari D. Burstein](https://www.anybrowser.org/cdaveb/)
: For his [Viewable with Any Browser](https://www.anybrowser.org/campaign/index.html) comapign and the awesome [provided badges](https://www.anybrowser.org/campaign/abgraphics.html)

[RusticCyberpunk/cypnk](https://sh2.us/) <img class="borderless" src="https://sh2.us/buttons/btn-cypnk-at-sh2.png" alt="">
: For his [archive](https://sh2.us/buttons/) of [Taylor McKnight's](https://gtmcknight.com/) collection of buttons

[Adam Kalsey](https://kalsey.com/)
: For his awesome [button maker](https://kalsey.com/tools/buttonmaker/)
