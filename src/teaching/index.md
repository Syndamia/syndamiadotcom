# Teaching

This is a list of all the subjects I've taught professionally.

<br>

&.bg 2023-2024 &emsp; [З520 Обектно-ориентрано програмиране - практикум, КН, 8 група](/teaching/oop-2023)
: Хоноруван преподавател във [ФМИ](https://www.fmi.uni-sofia.bg/) на специалност [Компютърни науки](https://www.fmi.uni-sofia.bg/bg/kompyutrni-nauki)

&.bg 2023-2024 &emsp; [3107 Увод в програмирането - практикум, КН, 8 група](/teaching/upp-2023)
: Хоноруван преподавател във [ФМИ](https://www.fmi.uni-sofia.bg/) на специалност [Компютърни науки](https://www.fmi.uni-sofia.bg/bg/kompyutrni-nauki)
