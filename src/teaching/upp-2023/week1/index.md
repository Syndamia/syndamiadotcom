# Задачи - УПП, Седмица 1, 06.10.2023

GitHub classroom: [classroom.github.com/a/W7hZDMgC](https://classroom.github.com/a/W7hZDMgC)

Решения: [GitHub](https://github.com/Syndamia/upp-2023-solutions/tree/main/week01)

### 1 задача - Hello, World!

Още от "древни" времена програмистите са си измисляли малки програмки, с които да тестват дали всичко работи.
Традиция в модерното време е да изкараш на екрана съобщението ["Hello, World!"](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program#History).
Това е главно популяризирано от книгата [The C Programming Language](https://en.wikipedia.org/wiki/The_C_Programming_Language), централния материал върху езика С (един от най-значимите езици за програмиране някога).

Изкарайте на екрана съобщението "Hello, World!".

### 2 задача - Двойна цифра

Във входа получавате цифра, трябва да върнете нейната числена стойност, умножена по две.

**Примери:**

|Вход|Изход|
|----|-----|
|2   |4    |
|8   |16   |

**Упътване:**
Цифрата е просто знак, и за да извършите коректно умножението трябва да превърнете знака в число.

### 3 задача - От Фаренхайт в Целзий

Във входа получавате цяло число, зачитайте го като градуси по Фаренхайт.
Трябва да върнете съответните градуси по Целзий (пак като цяло число).
Формулата е:

```
C = (F - 32) * 5/9
```

**Примери:**

|Вход|Изход|
|----|-----|
|32  |0    |
|107 |41   |
