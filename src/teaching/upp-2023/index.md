# Увод в програмирането - практикум, Компютърни науки, 8 група

Материали на проведените през 2023/2024 практикуми по Увод в програмирането на Компютърни науки (2 поток), 8 група.

&:warn От сега нататък ще може да продължите да предавате решения след крайния срок в classroom.
       Не мога да гарантирам кога ще погледна предадени решения след срока, но ще ги погледна!

**Зала:** ФМИ 309 ||
**Време:** 10:15 - 12:00 ||
**Ден:** Петък
<br>
**Директни връзки:**
[Контакт с мен](./week1/welcome.html?a#section-4) |
[Решения на задачи](https://github.com/Syndamia/upp-2023-solutions) |
[Материали от семинари](https://github.com/ivanahristova/introduction-to-programming-2023-2024) |
[Курс в мудъл](https://learn.fmi.uni-sofia.bg/course/view.php?id=9567)

|&#8470;|Дата|Материали|
|-------|----|---------|
|1 |06.10.2023|[Добре дошли](./week1/welcome.html) \|\| [Конфигуриране на GitHub classroom и Visual Studio](./week1/setup.html?a) \|\| [Задачи](./week1/?a) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week01)|
|2 |13.10.2023|[Преговор](./week2/revision.html?a) \|\| [Задачи](./week2) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week02)|
|3 |20.10.2023|[Преговор](./week3/revision.html?a) \|\| [Задачи](./week3) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week03)|
|4 |27.10.2023|[Преговор](./week4/revision.html?a) \|\| [Задачи](./week4) \|\| [GitHub classroom](https://classroom.github.com/a/jqKaO1ny) \|\| [Решения (само 1ва)](https://github.com/Syndamia/upp-2023-solutions/tree/main/week04)|
|5 |03.11.2023|[Преговор](./week5/revision.html) \|\| [Задачи](./week5) \|\| [GitHub classroom](https://classroom.github.com/a/3KoIQ71H) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week05)|
||04.11.2023|[Допълнително упражнение 1](./additional1/) \|\| [GitHub classroom](https://classroom.github.com/a/nlFuKALw) \|\| [Решение](https://github.com/Syndamia/upp-2023-solutions/blob/main/additional1/exercise.cpp)|
|6 |10.11.2023|[Задачи](./week6) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week06) \|\| [GitHub classroom](https://classroom.github.com/a/BBLlWMOy)|
|7 |17.11.2023|[Задачи](./week7) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week07) \|\| [GitHub classroom](https://classroom.github.com/a/m5M2_CWD)|
|8 |24.11.2023|[Задачи](./week8) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week08) \|\| [GitHub classroom](https://classroom.github.com/a/Big7FRUG)|
|9 |01.12.2023|[Задачи](./week9) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week09) \|\| [GitHub classroom](https://classroom.github.com/a/lGXxCRZO)|
|  |04.12.2023|[Допълнително упражнение 2](./additional2/) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/additional2) \|\| [GitHub classroom](https://classroom.github.com/a/CrerXpf6)|
|10|15.12.2023|[Задачи](./week10) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week10) \|\| [GitHub classroom](https://classroom.github.com/a/EJcTQRz4)|
|11|22.12.2023|[Задачи](./week11) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week11) \|\| [GitHub classroom](https://classroom.github.com/a/cZRnZj9y)|
|12|05.01.2024|[Задачи](./week12) \|\| [Решения](https://github.com/Syndamia/upp-2023-solutions/tree/main/week12) \|\| [GitHub classroom](https://classroom.github.com/a/KEDSxDjl)|
|13|12.01.2024|[Задачи](./week13) \|\| [GitHub classroom](https://classroom.github.com/a/bgiFD6SB)|
|14|19.01.2024|[Задачи](./week14) \|\| [GitHub classroom](https://classroom.github.com/a/B08TtnnW)|
