# Обектно-ориентирано програмиране - практикум, Компютърни науки, 8 група

Материали на проведените през 2023/2024 практикуми по Обектно-ориентирано програмиране на Компютърни науки (2 поток), 8 група.

**Зала:** ФМИ 120 ||
**Време:** 13:15 - 15:00 ||
**Ден:** Четвъртък
<br>
**Директни връзки:**
[Контакт с мен](../upp-2023/week1/welcome.html?a#section-4) |
[Решения на задачи](https://github.com/Syndamia/oop-2023-solutions) |
[Материали от семинари](https://github.com/Aleksis99/Object-Oriented-Programming_FMI_2023-2024) |
[Курс в мудъл](https://learn.fmi.uni-sofia.bg/course/view.php?id=10041)

|&#8470;|Дата|Материали|
|-------|----|---------|
|1 |22.02.2024|[Организационно](./week1/organization.html?a) \|\| [Основни добри софтуерни практики](./week1/good-software.html?a) \|\| [Задачи](./week1/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week01) \|\| [GitHub classroom](https://classroom.github.com/a/UPJOzHty)|
|2 |29.02.2024|[Задачи](./week2/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week02) \|\| [GitHub classroom](https://classroom.github.com/a/3qPsfyyI)|
|3 |07.03.2024|[Задачи](./week3/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week03) \|\| [GitHub classroom](https://classroom.github.com/a/KPMqXsFp)|
|4 |14.03.2024|[Задачи](./week4/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week04) \|\| [GitHub classroom](https://classroom.github.com/a/qa_FfWeG)|
|5 |21.03.2024|[Задачи](./week5/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week05) \|\| [GitHub classroom](https://classroom.github.com/a/42ZZ56Mg)|
|6 |28.03.2024|[Задачи](./week6/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week06) \|\| [GitHub classroom](https://classroom.github.com/a/rzSTn5pu)|
|7 |04.04.2024|[Задачи](./week7/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week07) \|\| [GitHub classroom](https://classroom.github.com/a/vvZENbkX)|
|8 |11.04.2024|[Задачи](./week8/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week08) \|\| [GitHub classroom](https://classroom.github.com/a/7d0MBjcK)|
|9 |18.04.2024|[Задачи](./week9/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week09) \|\| [GitHub classroom](https://classroom.github.com/a/wlL1oUg2)|
|10|25.04.2024|[Задачи](./week10/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week10) \|\| [GitHub classroom](https://classroom.github.com/a/PbkITWhF)|
|11|09.05.2024|[Задачи](./week11/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week11) \|\| [GitHub classroom](https://classroom.github.com/a/oYQehcnN) **Това упражнение е от 11:15 до 13:15**|
|12|09.05.2024|[Задачи](./week12/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week12) \|\| [GitHub classroom](https://classroom.github.com/a/o7hNz1pr)|
|13|16.05.2024|[Задачи](./week13/) \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week13) \|\| [GitHub classroom](https://classroom.github.com/a/GvV1wrC1)|
|14|23.05.2024|[Задачи](./week14/) <!-- \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week12) --> \|\| [GitHub classroom](https://classroom.github.com/a/K0XomKEP)|
|15|30.05.2024|[Задачи](./week15/) <!-- \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week12) --> \|\| [GitHub classroom](https://classroom.github.com/a/z2QadyEE) **Това упражнение е от 11:15 до 13:15**|
|16|06.06.2024|[Задачи](./week16/) <!-- \|\| [Решения](https://github.com/Syndamia/oop-2023-solutions/tree/main/week12) --> \|\| [GitHub classroom](https://classroom.github.com/a/BWjw2dUC)|
