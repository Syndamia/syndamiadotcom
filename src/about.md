# About Me

**Contact me casually**
: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#115;&#121;&#110;&#100;&#97;&#109;&#105;&#97;&#64;&#112;&#114;&#111;&#116;&#111;&#110;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;">&#115;&#121;&#110;&#100;&#97;&#109;&#105;&#97;&#64;&#112;&#114;&#111;&#116;&#111;&#110;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;</a> <a class="borderless" href="/files/publickey-&#115;&#121;&#110;&#100;&#97;&#109;&#105;&#97;&#64;&#112;&#114;&#111;&#116;&#111;&#110;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;-2c9b555f61c33e30acbe477dcb2956a33e10cc8c.asc"><img class="borderless" src="/img/pgpkey.gif" alt="PGP key"></a><br><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#97;&#109;&#101;&#110;&#46;&#115;&#64;&#115;&#121;&#110;&#100;&#97;&#109;&#105;&#97;&#46;&#99;&#111;&#109;">&#107;&#097;&#109;&#101;&#110;&#046;&#115;&#064;&#115;&#121;&#110;&#100;&#097;&#109;&#105;&#097;&#046;&#099;&#111;&#109;</a> <a class="borderless" href="/files/publickey-&#107;&#97;&#109;&#101;&#110;&#46;&#115;&#64;&#115;&#121;&#110;&#100;&#97;&#109;&#105;&#97;&#46;&#99;&#111;&#109;-0xDFC9D42E.asc"><img class="borderless" src="/img/pgpkey.gif" alt="PGP key"></a> *(for this one, make sure to check your spam folder!)*

**Contact me about work and projects I'm maintaining**
: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#97;&#109;&#101;&#110;&#46;&#100;&#46;&#109;&#108;&#97;&#100;&#101;&#110;&#111;&#118;&#64;&#112;&#114;&#111;&#116;&#111;&#110;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;">&#107;&#97;&#109;&#101;&#110;&#46;&#100;&#46;&#109;&#108;&#97;&#100;&#101;&#110;&#111;&#118;&#64;&#112;&#114;&#111;&#116;&#111;&#110;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;</a> <a class="borderless" href="/files/publickey-&#107;&#97;&#109;&#101;&#110;&#46;&#100;&#46;&#109;&#108;&#97;&#100;&#101;&#110;&#111;&#118;&#64;&#112;&#114;&#111;&#116;&#111;&#110;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;-62ca52e87475c874b9e6ebf3c4c9f716b6ce4c9d.asc"><img class="borderless" src="/img/pgpkey.gif" alt="PGP key"></a> <br> <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#097;&#109;&#101;&#110;&#064;&#115;&#121;&#110;&#100;&#097;&#109;&#105;&#097;&#046;&#099;&#111;&#109;">&#107;&#097;&#109;&#101;&#110;&#064;&#115;&#121;&#110;&#100;&#097;&#109;&#105;&#097;&#046;&#099;&#111;&#109;</a> <a class="borderless" href="/files/publickey-&#107;&#097;&#109;&#101;&#110;&#064;&#115;&#121;&#110;&#100;&#097;&#109;&#105;&#097;&#046;&#099;&#111;&#109;-0x354C5F95.asc"><img class="borderless" src="/img/pgpkey.gif" alt="PGP key"></a> *(for this one, make sure to check your spam folder!)*<br>[linkedin.com/in/kamen-mladenov](https://www.linkedin.com/in/kamen-mladenov/)

**Resume**
: [Български](/files/resume-bg.pdf) | [English](/files/resume-en.pdf)

<br>

### Antibio [(opposite of bio, and much more fun)](https://www.brendangregg.com/antibio.html)

<img id="about-image" class="borderless" src="/img/face.jpeg" alt="">

I'm a [university](https://www.fmi.uni-sofia.bg/en) student and general computer nerd.
My current interests include: operating systems, functional programming, webdev (without **any** libraries, pure JavaScript), old tech, some amount of server administration and a teeny weeny bit of computer history.

I have a good amount of [projects under my belt](https://gitlab.com/users/Syndamia/projects), although very few are actually useful.
I've put down the more notable ones [here](/software), but [filite-list](https://github.com/Syndamia/filite-list) and [min-youtube-element-blocker](https://github.com/Syndamia/min-youtube-element-blocker) seem to have gotten the most attention (if any).

As for what exactly you can expect in the future, anything goes really.
I've done a lot of low(er)-level programming, a bit less functional programming, professional-ish web development (back-end + front-end), a whole lot of scripting and more server/system administration that I would ever want.
So, who knows what's gonna be next.

Unfortunately there isn't too much I can show now, especially as content on this website...
But hey, you gotta start somewhere!

### Software

This is a curated list of the more relevant software projects I've made or heavily contributed to

<br>

#### *2023*

[gitlab / texty-office](https://gitlab.com/Syndamia/texty-office) + [gitlab / texty-office.vim](https://gitlab.com/Syndamia/texty-office.vim)
: An imperfect converter from DOCX, PPTX and XLSX to normal text with it's associated [Vim](https://www.vim.org/) plugin. <br> `GNU GPLv3` & `MIT` | `C++` `vimscript` | `Office file` `converter` `plugin` `Vim` `CLI tool`

[gitlab / small-web-utilities](https://gitlab.com/Syndamia/small-web-utilities) &emsp; *maintained*
: A collection of small but fast utilities, like a checklist, that work locally on your browser. Also hosted [here](https://utils.syndamia.com). <br> `GNU GPLv3` | `HTML` `Javascript` | `checklist` `utilities`

[gitlab / ss-law](https://gitlab.com/Syndamia/ss-law) + [github / ss-law](https://github.com/Syndamia/ss-law) &emsp; *maintained*
: A small, lightweight and simple static site generation system, extracted as a template from my own <br> `Markdown` `bash` `awk` | `ssg` `lowdown` `static site generator` `pipelines`

[gitlab / homelander](https://gitlab.com/Syndamia/homelander) &emsp; *maintained*
: A very small and good looking, handmade prompt for zsh <br> `GPLv3` | `bash` | `zsh` `shell` `prompt` `script` `Linux`

[gitlab / goodwolf](https://gitlab.com/Syndamia/goodwolf) &emsp; *abandoned*
: Fork of the [badwolf](https://hacktivis.me/projects/badwolf) browser, which aims to add features <br> `BSD-3-Clause` | `C` | `web browser` `GTK` `WebKitGTK` `Linux`

#### *2021*

[gitlab / RepoSync](https://gitlab.com/Syndamia/reposync) &emsp; *completed*
: Script and some config files for synchronizing a git repository with a server <br> `MIT` | `python` | `server` `git` `systemd`

[github / filite-list](https://github.com/Syndamia/filite-list) &emsp; *completed*
: Script for managing [filite](https://github.com/raftario/filite) links <br> `GPLv3` | `bash` | `filite` `server` `script` `Linux`

#### *2020*

[github / DevHive](https://github.com/Team-Kaleidoscope/DevHive) and [github / DevHive-Angular](https://github.com/Team-Kaleidoscope/DevHive-Angular) &emsp; *completed*
: DevHive is a social network oriented towards programmers. It's FOSS, built from the ground up, and made from a team of three for a course. <br> `GPLv3` | `C#` `TypeScript` | `Angular` `ASP.NET Core` `social media` `mvc` `Docker` `CSS`

[github / min-youtube-element-blocker](https://github.com/Syndamia/min-youtube-element-blocker) &emsp; *maintained*
: Userscript for hiding certain UI elements in YouTube <br> `MIT` | `JavaScript` | `youtube` `userscript` `css`
