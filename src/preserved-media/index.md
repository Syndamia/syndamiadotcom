# Preserved media

An assortment of documents, videos, etc. which are **not** made by me, but aren't archived on the internet or have the potential of permanently disappearing.

If you are an owner of something listed here, [contact me](/about) and I'll remove it immediately.

<br>

&.ft-zip 22.09.2022 &emsp; [Reconstructed source](/files/media/BrilliantButtonMaker-LucaZappa-updated.zip) of Luca Zappa's Brilliant Button Maker
: Using what archive.org had, I reconstructed most of the source code for the "Brilliant Button Maker" by [Luca Zappa](https://www.linkedin.com/in/lucazappa/). I also host it [here](https://hob.syndamia.com/brilliant-button-maker/).

&.ft-text 14.10.1991 &emsp; [Прибор электроизмерительный комбинированный типа 43208-У](/files/media/43208-%D0%A3/43208-%D0%A3.pdf)
: A scanned manual (in Russian) for the Soviet Analog Multimeter [43208-У](http://radioamator.ru/izmereniya/multimetry-testery/221-kombinirovannyj-pribor-43208-u-osobennosti-skhemy-i-remonta)
