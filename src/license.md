# License

This project uses two licenses, depending on the item.

For articles and pictures/diagrams/etc. that have been made by me, the license is [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) (also found in [LICENSE-CONTENT](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/LICENSE-CONTENT)).
Everything under [assets](/assets), unless stated otherwise, also falls in this category.

For source code, like all JavaScript or AWK generation scripts (those aren't on the website but are in the [repository](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/awk-edits)), the license is [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) (also found in [LICENSE-CODE](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/LICENSE-CODE)).

Content in [Archives](/archives) and [Archived Web](/archived-websites) **isn't made by me and it's respective creator must be asked for licensing**. This content is not saved in the [GitLab repository](https://gitlab.com/Syndamia/syndamiadotcom).

*If you feel like something is infringing on your own Copyright, please contact me immediately with any listed contact channel inside [/about](/about)!*
