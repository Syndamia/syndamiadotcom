# Setup

Everyone that is really interested in technology has custom configurations for everything they use.
Here are mine.

## Hardware I own

|Designation|OS |Motherboard / device|CPU|GPU|RAM|
|-----------|---|------------------|---|---|---|
|Main desktop tower|NixOS (24.11)|ASRock B550 Phantom Gaming 4|[AMD Ryzen 9 5900X](https://www.amd.com/en/products/processors/desktops/ryzen/5000-series/amd-ryzen-9-5900x.html)|[AMD Radeon RX 6800 XT](https://www.amd.com/en/products/graphics/desktops/radeon/6000-series/amd-radeon-rx-6800-xt.html)|1x Corsair CMK32GX4M2E3200C16|
|||Bought: 2024|[39125 mark](https://www.cpubenchmark.net/cpu.php?cpu=AMD+Ryzen+9+5900X&id=3870), 12 cores, 24 threads, 3.70 GHz, L1: 768 KB, L2: 6 MB, L3: 64 MB, 64-bit|[25027 mark](https://www.videocardbenchmark.net/gpu.php?gpu=Radeon+RX+6800+XT&id=4312), 2000 MHz, 16 GB memory|DDR4, 32 GB (2x 16GB), 3200 MHz|
|Old desktop tower|NixOS (24.05)|ASRock Z97 Anniversary|[Intel Core i3-4160](https://www.intel.com/content/www/us/en/products/sku/77488/intel-core-i34160-processor-3m-cache-3-60-ghz/specifications.html)|AMD Sapphire Radeon R7 265|2x Corsair CMV8GX3M1A1600C11|
|||Bought: 2015|[3510 mark](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i3-4160+%40+3.60GHz&id=2317), 2 cores, 4 threads, 3.60 GHz, L1: 128 KB, L2: 512 KB, L3: 3 MB, 64-bit|[3849 mark](https://www.videocardbenchmark.net/gpu.php?gpu=Radeon+HD+7850&id=323), 900 MHz, 2 GB memory|DDR3, 16 GB (2x 8GB), 1600 MHz
|Main laptop|NixOS (24.11)|Lenovo ThinkPad X270|Intel Core i5-6300U|Intel HD Graphics 520|1x Samsung M471A2k43BB1-CRC|
|||Bought: 2023<br>Released: 2017|[3240 mark](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-6300U+%40+2.40GHz&id=2609), 2 cores, 4 threads, 2.40 GHz, L1: 128 KB, L2: 512 KB, L3: 3 MB, 64-bit|[871 mark](https://www.videocardbenchmark.net/gpu.php?gpu=Intel+HD+520&id=3255), 300 MHz|DDR4, SODIMM, 16 GB, 2133 MHz|
|Second laptop|NixOS (23.05)|Lenovo Yoga S730|Intel Core i7-8565U|Intel UHD Graphics 620|2x Samsung K4EBE304EC-EGCG|
|||Bought: 2019<br>Released: 2018|[6152 mark](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-8565U+%40+1.80GHz&id=3308), 4 cores, 8 threads, 4.60 GHz, L1: 128 KB, L2: 1 MB, L3: 8 MB, 64-bit|[1039 mark](https://www.videocardbenchmark.net/gpu.php?gpu=Intel+UHD+Graphics+620&id=3805), 300 MHz|LPDDR3 (soldered), 16 GB, 2133 MHz|
<!--
|Main smartphone|Fresh Android ROM|Samsung Galaxy A50|
|Old smartphone|?|Samsung Galaxy A5 (2016)
|Old smartphone|Android ?|Samsung ?
|Retro PC|Windows 98 SE|?|Intel Pentium ?|GeForce GTX 100|256 MB
|Retro laptop|Dell ?
|VPS|?
-->

*Yes, I stole the idea for this from [Haelwenn Monnier](https://hacktivis.me/about)*

## OS setup

|Distro|Fully switched|Configuration files/scripts|
|------|--------------|-------------|
|**Desktop OS**|||
|NixOS|11.2023|[NixOS configuration](https://gitlab.com/Syndamia/nix)
|Gentoo|03.2023|[portage configuration files](https://gitlab.com/Syndamia/dotfiles/-/tree/main/.b/etc/portage), [Installation Makefile](https://gitlab.com/Syndamia/application-setup/-/blob/main/gentoo/Makefile)
|Funtoo|12.2021|[portage configuration files](https://gitlab.com/Syndamia/dotfiles/-/tree/ef18469c90c8e90d3db436a1a204aec5c92cac73/.b/etc/portage)
|Fedora 33-34|12.2020|[Installation from Minimal](https://gitlab.com/Syndamia/application-setup/-/blob/main/scratch-fedora-setup.sh), [Installation from Cinnamon](https://gitlab.com/Syndamia/application-setup/-/blob/main/fedora-setup.sh), [Cinnamon setup](https://gitlab.com/Syndamia/application-setup/-/tree/main/cinnamon-setup), [openbox setup](https://gitlab.com/Syndamia/dotfiles/-/blob/main/.c/fedora-openbox.sh)
|Linux Mint 19.3-20|01.2020|[Installation script](https://gitlab.com/Syndamia/application-setup/-/blob/main/linux-mint-setup.sh), [Cinnamon setup](https://gitlab.com/Syndamia/application-setup/-/tree/main/cinnamon-setup)
|Manjaro Cinnamon 18.1.4-18.1.5|12.2019|-|
|**Server OS**|||
|Debian 10|06.2021|-|
|Ubuntu 20.04 LTS|07.2020|-|

[Dotfiles here](https://gitlab.com/Syndamia/dotfiles).
Primary shell is [zsh](https://www.zsh.org/).

### Desktop environment

|Name|Fully switched|List of apps|
|----|--------------|------------|
|[openbox wm](http://openbox.org/wiki/Main_Page) + custom apps|06.2021|[Application Stack](https://gitlab.com/Syndamia/dotfiles/-/blob/main/.c/Application%20Stack.md)
|[Cinnamon](https://projects.linuxmint.com/cinnamon/)|12.2019|-|

## Text editor

|Name|Fully switched|Configuration files|
|----|--------------|-------------------|
|[neovim](https://neovim.io/)|08.2024|[anderson](https://gitlab.com/Syndamia/anderson/)|
|[vim](https://www.vim.org/)|10.2020|[dotfiles](https://gitlab.com/Syndamia/dotfiles/-/tree/main/.vim)|
|[Atom](https://en.wikipedia.org/wiki/Atom_%28text_editor%29)|05.2020 - 07.2020|Switched back after|
|[Monodevelop](https://www.monodevelop.com/)|01.2020|-
|[Visual Studio 2017](https://visualstudio.microsoft.com/)|11.2018|-
