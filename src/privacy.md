# Privacy policy

[syndamia.com](https://syndamia.com) and [tiny.syndamia.com](https://tiny.syndamia.com) offer no user data submission (user accounts, reply forms, etc.).
Also, there is [no form of data collection with Javascript](#no-collection-with-javascript).

Some [third-party services](#third-party-services) as well as some [essential server-side programs](#http-logs) may collect data.
Any data I'm responsible for **is NEVER collected to be sold!**

### HTTP logs

General HTTP requests to the server (and data surrounding them) are saved, which may include: IP address, timestamps, exact page request, user agent, etc.
My current web server software (program, which handles website requests) of choice is [apache2](https://httpd.apache.org/), and the data format (mostly) corresponds to it's defaults.

One such data line would look something like:

```
1.1.1.1 - - [11/Jul/2023:14:28:11 +0300] "GET / HTTP/1.1" 200 668 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/113.0"
```

These logs could be useful for certain issues, like bad server performance or malicious webpage access.

### Third-party services

My domain is bought with [namecheap](https://www.namecheap.com/) and it's associated DNS entries are managed with namecheap servers.
This means that DNS entries, or in simpler terms - domain configurations, first go through namecheap, and that may or may not mean they can collect data from you.

Analogously, my website is hosted on [hetzner](https://www.hetzner.com/) servers, which may or may not be doing some sort of data collection, outside of my control.

### No collection with Javascript

[This site uses Javascript](/regarding-js.html), so some feature configuration information may be temporarily stored in your browser.
For what exactly that data is, read [here](/regarding-js.html#data-in-browser-storage).
However, for now there are no plans on integrating any sort of statistics or personal data collection.
