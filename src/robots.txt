User-agent: *
Disallow: /css/
Disallow: /img/
Disallow: /js/
Disallow: /files/ephemeral/
Sitemap: https://syndamia.com/sitemap.xml
