# Linuxgraphy by Strabo

### Custom assets

*Licensing information in the last slide, at the bottom of the page*

- [UNIX layers image](./img/unix-layers.png)
- [UNIX directory tree](./img/unix-dir-tree.png)
- [mll-set.sh](./mll-set.sh)

### Presented at

&.bg Unofficial [FMI](https://fmi.uni-sofia.bg/en) Computer Science Discord server &emsp; *04.03.2023 || 14:50 - 17:20*
: In front of a tiny audience of other uni students. Exact revision [here](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/6feed223ff48fe00cf12fcd63f3f56179c298cae/src/talks/linuxgraphy-by-strabo/index.md).

&.bg Unofficial [FMI](https://fmi.uni-sofia.bg/en) Computer Science Discord server &emsp; *22.01.2023 || 18:00 - 21:00*
: In front of a tiny audience of other uni students. Exact revision [here](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/6eb7a8fff01c6814253a0e738efb53734d297be9/src/talks/linuxgraphy-by-strabo/index.md).

&.presentation



&[title

# Linuxgraphy by Strabo

*Made by [Syndamia](https://syndamia.com)*

&.br4
&{sqrow

&.br1

&(font22
2000 years ago [Strabo](https://en.wikipedia.org/wiki/Strabo) published [Geography](https://en.wikipedia.org/wiki/Geography), in which he laid out the world as it was known to the Romans and Greeks.
&.br1
Today, I'll lay out Linux as it is known to me.
&)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Strabo.jpg/199px-Strabo.jpg)

&.br1

&} &]



&[slide35

# Contents

&{sqrow
&{column

1. Historical context
2. Structure of UNIX-likes
   1. Shell
   2. UNIX-style file system
   2. Kernel
&.br2
3. Linux from the inside
   1. Shells
   2. Files and directories
   3. Common configuration files
   4. System management
4. Software licensing

&} &{column

5. What makes up a useful OS
   1. Bootloader
   1. Init system
   2. Package manager
   3. Desktop manager
6. War on distros
&.br4
7. Demos - virtual machine and setup from zero

&} &}

&]



&[slide

# 1. Historical context

&.br1

#### 1945 - 1955

You directly loaded instructions into memory and let it execute your "code" (processor instructions).
Nothing else ran on the machine.

This process was labor intensive: a qualified operator loaded your program, dumped the memory contents, remove any external media, reset the machine and load the next job.

#### 1956 - 1959

Computers became faster, but a lot of time was spend on managing jobs.

[resident monitors](https://en.wikipedia.org/wiki/Resident_monitor): very small programs, which always *resided* in memory, and *monitored* what the state of the current job was.  
Jobs were loaded in series ([batches](https://en.wikipedia.org/wiki/Batch_processing)).
When the current job finished, memory would be dumped and the next one would automatically be loaded and started.

&]



&[slide

#### 1960s

&.br1

Peripherals (tapes, punch cards, ...) were extremely slow.  
[Multiprogramming](https://en.wikipedia.org/wiki/Computer_multitasking#Multiprogramming): the current job is waiting for a peripheral, another job would be started.

[Travel reservation system](https://en.wikipedia.org/wiki/Sabre_%28travel_reservation_system%29) by American Airlines: where travel agents would search, price and book services.
A computer system now had to support:

- User accounts with concurrent access
- Fast response time and ease of use
- Storage organization, also following the rise of disks
- Data communication links (often phone lines)

&.br1

More and more business started using computers, thanks to [minicomputers](https://en.wikipedia.org/wiki/Minicomputer), so demand for OS software increased.

&]



&[slide

#### 1964 - 1969

IBM's [System/360](https://en.wikipedia.org/wiki/IBM_System/360) line of computers, each with expansion capabilities, backwards compatibility, all under one instruction set and operating system.

[Multics](https://en.wikipedia.org/wiki/Multics), an influential operating system that was designed for a General Electric mainframe.
Some of it's novel ideas include, but are not limited to:

- [Time-sharing](https://en.wikipedia.org/wiki/Time-sharing): sharing resources of a singular machine among many users (via [multiprogramming](https://en.wikipedia.org/wiki/Multiprogramming) and [multi-tasking](https://en.wikipedia.org/wiki/Computer_multitasking))
- [Single level store](https://en.wikipedia.org/wiki/Single-level_store) (aka [virtual memory](https://en.wikipedia.org/wiki/Virtual_memory)): Processes would write into contiguous memory, but in reality it is scattered and managed by the operating system.
- [Protection rings](https://en.wikipedia.org/wiki/Protection_ring): Allowing different levels of access to different resources (programs). In general, this also often requires a [CPU protected mode](https://en.wikipedia.org/wiki/Protected_mode).
- [Hierarchical file system](https://en.wikipedia.org/wiki/Directory_%28computing%29): Arbitrary directories with subdirectories, rather than just having files next to each other or having directories which could only contain files.
- [Command processor](https://en.wikipedia.org/wiki/Shell_%28computing%29) (that is separated from the OS): More on that later

&]



&[slide

#### 1969: Death of Multics and birth of UNIX

Ultimately Multics grew too large, becoming unusable and unmaintainable.  
In the beginning, programming was done mostly by [Bell Labs](https://en.wikipedia.org/wiki/Bell_Labs), with [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) being one of the developers.

&.br1
&{sqrow

&(font18
![](https://upload.wikimedia.org/wikipedia/commons/f/f8/Ken-Thompson-2019.png =160x)  
*Ken Thompson image by National Inventors Hall of Fame*
![](https://upload.wikimedia.org/wikipedia/commons/1/1b/Ken_Thompson_and_Dennis_Ritchie--1973.jpg)  
*Ken Thompson and Dennis Ritchie in 1973*
&)

&{column

Thompson still had some desire to work on operating systems, after Bell Labs pulled out.
The tools he made while rewriting [his own video game](https://en.wikipedia.org/wiki/Space_Travel_%28video_game%29) on a PDP-7 lead to a whole operating system: UNIX.

Based, expanded on and improved upon many of Multic's ideas, UNIX became the father of modern OSs.

&}

&} &]



&[slide

# 2. Structure of UNIX-likes

Ignoring [standards](https://unix.stackexchange.com/questions/14368/difference-between-posix-single-unix-specification-and-open-group-base-specifi/14415) and [specifications](https://unix.stackexchange.com/questions/87686/what-makes-an-operating-system-unix-like), in my opinion, from the point of a user, a UNIX-like OS has the following components:

&.br4

1. **shell**: A programmable replaceable command-line interpreter, with utilities for managing the whole system and support for pipelines and I/O redirection
2. **UNIX-style file system**: [Filesystem](https://en.wikipedia.org/wiki/Unix_filesystem) as a single rooted tree, objects (nodes) in the file system are [inodes](https://en.wikipedia.org/wiki/Inode) and an inode can be [(at least)](https://en.wikipedia.org/wiki/Unix_file_types) either a regular file, directory or devices. Permissions per users and group.
3. **kernel**: Program that manages all communication and operations between the hardware and software

&.br4

&(font18 *For now we'll generally just look over some of the common stuff and go into detail later, while in Linux.* &)

&]



&[slide35

## 2.1. shell

&.br2 &{sqrow

&{column
The operating system is divided into layers, [like an onion](https://youtu.be/-FtCTW2rVFM?t=57).  
Only the kernel has access to the hardware.  
shells and executables (binaries) have access to the kernel.

shells provide user interfaces, [both](http://en.wikipedia.org/wiki/Shell_%28computing%29) command-line and graphical.
While on the topic of UNIX, we'll only discuss the former.
&}

&(centered ![](./img/unix-layers.png) &)

&} &]



&[slide

### Command-line shells

Command-line shells operate solely with text, often only with ASCII characters.
Usually connecting to a computer (to the shell) is said to be done with either a "terminal" ("console") or "teleprinter" ("teletype").

&{row

Video displays became widely available in the late 1970s, so before that access was done with a "teleprinter", a typewriter-printer combo.

&(font18
![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/ASR-33_at_CHM.agr.jpg/1280px-ASR-33_at_CHM.agr.jpg =860x)  
*Teletype Model 33 by [Arnold Reinhold](https://commons.wikimedia.org/wiki/User:ArnoldReinhold)*
&)


&.newrow

With the advent of computer displays, teleprinters were replaced with terminals, video display-keyboard combo.
Thanks to the widely popular [VT100](https://en.wikipedia.org/wiki/VT100), almost all terminals support [ANSI escape codes](https://en.wikipedia.org/wiki/ANSI_escape_code).

&(font18
![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/DEC_VT100_terminal.jpg/675px-DEC_VT100_terminal.jpg?20131106192408 =860x)  
*DEC VT100 by [Jason Scott](https://www.flickr.com/people/54568729@N00)*
&)

&} &]



&[slide

Today we don't usually use such specialised hardware, but the names persist with slightly different meanings.

[Virtual Terminal](https://en.wikipedia.org/wiki/Virtual_console) (Console, tty): on some modern UNIX-likes (like Linux), the kernel/OS provides special devices, where a console is directly implemented/simulated with the connected computer and display.

- systems that don't provide it usually expose another way to connect a console (now external hardware and/or software is required), like a [serial](https://en.wikipedia.org/wiki/Serial_port) port

[Terminal Emulator](https://en.wikipedia.org/wiki/Terminal_emulator): a special program that emulates a virtual terminal (within another display system)

[Pseudoterminal](https://en.wikipedia.org/wiki/Pseudoterminal) (pty): a program that sits between a terminal and a shell (or other program), and makes them both think they're directly connected

- ssh, a program to remotely connect to a device's shell, establishes a pseudoterminal. Your local terminal thinks it's connected to a local shell and vice versa with the remote shell, however the pty in between handles the true communication.

&]



&[middle

### Command location and parameters

Most commands are regular programs, which exist somewhere on the system.
When entering a command name, the shell searches for an executable with that name.
Searching the whole drive is very inefficient, so the search is limited to the few directories, listed in a variable called "PATH".

You can give a full or relative path, and that will be executed directly.

Each command can be given parameters, separated by spaces, each (by convention) being either text values or options (switches).

Options usually start with a hyphen `-` (like `-c` or `-current-time`) and often support abbreviations (`-a -b -c` to `-abc`).
Newer conventions also allow start with two hyphens `--` (`--current-time`), though if shown as-is indicate the rest is a string.
More rarely you might see `--OPTION=VALUE` or just `OPTION=VALUE`.

&]



&[middle

### Thompson shell

The first UNIX shell is the [Thompson shell](https://en.wikipedia.org/wiki/Thompson_shell).
The two major features that every other shell supports are:

- I/O redirection: redirection of input and output, allowing insertion of input from a file or storage of output into a file:

  ```
  command [args...] < filepath
  command [args...] > filepath
  ```

- pipelines: being able to redirect the output from one command to another without limit

  ```
  command1 [args...] | command2 [args...] | ...
  ```

&]



&[slide

## 2.2. UNIX-style file system

- Rooted singular tree of directories (the root folder of which is called "root", denoted with a "/")
- Each tree object (node) is an [i(ndex)node](https://en.wikipedia.org/wiki/Inode), containing type, ownership, access, internal data and any other metadata, but **not** name!
- Directory inodes (files) contain a table with rows of names and inode numbers, mapping a file name to it's contents. Each row is called a [hard link](https://en.wikipedia.org/wiki/Hard_link). Every directory has a hard link to itself, called ".", and a hard link to it's parent, called "..".

![](./img/unix-dir-tree.png)

&]



&[middle

### Attributes of an inode

- [file type](https://en.wikipedia.org/wiki/Unix_file_types): Each inode represents some sort of file (data), but a "file" can also be a directory or device, so we need to know how to handle it.  
  There are 7 main types (but there can exist more, depending on OS):
  &.br1
  + **regular**: just a plain old file
  + **directory**: as explained, a file containing hard links to other files. Each directory is allowed to appear *only once* in a single parent.
  + **symbolic link**: points to any file (or directory). It contains the (relative) path to that object (as a simple string), so a symlink not could even be valid. 
    Think of a C++ pointer (pseudocode).

    ```
    symlink /bin/mprog = "/usr/local/bin/mprog";
    exec(/bin/mprog); -> Executes /usr/local/bin/mprog
    ```

&]



&[slide

  + **FIFO special**: aka [named pipe](https://en.wikipedia.org/wiki/Named_pipe), a special type of regular file, where at any moment only one process can read from it and one can write to it. It takes the concept of shell pipes to inter-process communication.

    ```
    [Process]     --write->     [FIFO]     x-write--     [Process]
    [   A   ]     x--read--     [    ]     --read-->     [   B   ]
    ```

  + **block special**: a device which is randomly accessible, like a hard drive or cdrom drive.
    Think of a C++ array (pseudocode):

    ```
    BlockSpecial[438] = 1 ; var = BlockSpecial[438]
    ```

  + **character special**: devices which are accessed via serial streams of input or output, like keyboards, mice, graphics cards and teletypes.  
    Think of a C++ stream (pseudocode):

    ```
    CharacterSpecial << 1 ; CharacterSpecial >> var
    ```

&]



&[middle

  + **sockets**: file for inter-process communication. Compared to FIFO specials, they can be used by more than two processes, used in both directions and support [file descriptors](https://en.wikipedia.org/wiki/File_descriptor) and packets

    ```
    [Process A]  <--read&write-->  [Socket 1]
    [Process B]  <--read&write-->  [Socket 1]
    ...

    [Process A]  --write-->  [Socket 1]  --read-->  [Process B]
                                         \-read-->  [Process C]
    ```

&]



&[slide

#### Side-step into users and groups

&.br3

A user in UNIX is essentially a small collection of data, most important of it being a **unique ID** (number), a **name** (string), a **group id** (number; files created by the user are in that group) and a **password**.

Groups are also small collections of data, but much simpler, comprised only of a **unique ID** (number), a **name** (string) and a **list of users** that are "in" the group. Their main purpose is to simplify access control.

User with ID 0 is called root, it is the "system administrator", all actions made by the system itself are done as that user.
Every user has their own "home" folder (under "/home/USERNAME/", except for root, which is "/root/"), in which they store personal files, as well as user-specific configuration files.

&]



&[slide

- [file permissions](https://en.wikipedia.org/wiki/File_system_permissions): control the freedom of a user to read (navigate for directories), change and execute files  
  File permissions are split into three sets: what the **owner** can do, what **group** members can do and what **anyone else** can do.  
  Each set contains a mix of three permissions: **r**ead, **w**rite and e**x**ecute, and each set stores some combination of them.
  &.br1
  Each is represented by a bit in the order above (`101` means you *can* read, *cannot* write, *can* execute) and the overall permissions are often shown as a three digit number or character string.  
  Example: `111101100` will be shown as `754` or `rwxr-xr--`

- link count: the count of hard links that point to the inode. A file is deleted **only when there are no hard links left to it**. Every directory inode has at least two - one from parent and one from itself ("."), though the latter **isn't** counted. Child and parent directories also **add** to the count.

- User ID of owner, Group ID, file size and timestamps

&]



&[slide

## 2.3. Kernel

&(font18 &.br1 &)

The kernel has the core functionality of the operating system and bridges the gap between programs and hardware.
Some important subsystems include scheduling, file, device, process and memory management.

To preserve the [everything is a file](https://en.wikipedia.org/wiki/Everything_is_a_file) methodology, devices and processes can be handled as files (inside `/dev` and `/proc`).
They (most of the time, in modern kernels) aren't actual files on a hard drive, but "virtual" files, where operations on them are handled in a different manner than normal by the kernel.

&.br2

&(font35 Linux is **not** an operating system, it is a kernel!!! &)

&.br2

Two of the important things a kernel does is manage **processes** and **memory**.

&]



&[slide35

### 2.3.1 Processes

&.br1

A program is some collection of code and other data, stored in a file (or noncompiled code).  
A process is the program in motion, the program itself *alongside* certain system states.

The system states include (but are not limited to):

- the processor state, including the program counter and register values
- memory map, indicating what regions of memory are allocated (to the process)
- [file descriptors](https://en.wikipedia.org/wiki/File_descriptor), unique identifiers for files


&]



&[middle35

On a multitasking (multiprocessing) system, which is any modern desktop system, multiple processes are always running "at once".

In reality, constantly one process is stopped and another is ran, switching between all of them ([context switching](https://en.wikipedia.org/wiki/Context_switch)).
All "waiting" processes are put in a prioritized queue ([run queue](https://en.wikipedia.org/wiki/Run_queue)).

Stopping is either done by the process itself (more on that later), or by the system when the process has been actively running for too much time.

&]



&[slide35

#### Process Control Block

All process information is stored in a data structure, called a *process control block* (or process descriptor).
Not to be confused with a [printed circuit board](https://en.wikipedia.org/wiki/Printed_circuit_board)!  
A PCB's data can be split into three categories:

1. **Process identification** - the ID of the process itself, of it's parent process, of it's owner (user), etc.
2. **Process state** - as described, the processor state, etc.  
   On a context switch, values in registers are put into the stopped PCB and then are loaded from the running PCB.
3. **Process control information** - process state, memory map, privileges, etc.

A process can be **forked**, meaning a new process is created, with the same underlying code (but not process states and ...).

&]



&[middle35

#### Process states

- **Created**: This is the initial state given to a new process. In this state, the process awaits a "ready" state.
- **Ready** (Waiting): A process is put in the run queue and awaits being executed ("running" state).
- **Running**: When the process's program is being currently executed by the CPU. The process can run in either *kernel mode*, having access to the kernel and user addresses, or *user mode*, having access only to it's own code and data.
- **Blocked**: When a process cannot continue without external change, usually when waiting for an I/O device.
- **Terminated**: The process has either completed execution or has been killed. The process itself is called a ["zombie process"](https://en.wikipedia.org/wiki/Zombie_process) (until it is removed).

&]



&[middle35

#### Exit status

Every process, before being deleted (and after being in a Terminated state) passes a small number, called the [exit status](https://en.wikipedia.org/wiki/Exit_status) (exit code), to it's parent process.
For all intents and purposes, this number is an 8bit unsigned integer, meaning it's value is between 0 and 255.

Pretty much universally, an exit code of 0 signifies a successful termination, and anything else specifies some sort of error code.
Different programs specify the meaning of an exit code in different ways, some assign each number to a specific error, other use the bits representing the number as flags.

&]



&[slide35

### 2.3.2 Memory management

&.br2

In ye olde days, you would save a bit of memory for the operating system and give all of the rest to the current process ([monoprogramming](https://en.wiktionary.org/wiki/monoprogramming)), which we wait until completion.

This doesn't allow for context switching (disks are slow) and a blocked state forces the CPU to wait.

Some bad ideas include: depending on the code of all processes to not use the same addresses, use only [relative addresses](https://en.wikipedia.org/wiki/Position-independent_code) (`-fPIC` option in `gcc`) or use a [table](https://en.wikipedia.org//wiki/Relocation_%28computing%29#Relocation_table) that the kernel fills out with addresses.

&]



&[slide35

### Virtual memory

&.br2

&{fright
![](./img/virtual-memory-by-Ehamberg.png)  
&(font18 *Image by [Ehamberg](https://commons.wikimedia.org/wiki/File:Virtual_memory.svg)* &)
&}

A program (process) works with some (sequential) addresses, and the OS (with the help of [hardware](https://en.wikipedia.org/wiki/Memory_management_unit)) translates/maps the process' addresses (logical/[virtual addresses](https://en.wikipedia.org/wiki/Virtual_address_space)) to real ones (physical addresses).

Simplified memory management for the program, protected address space for each process and improved flexibility.

&]



&[slide35

#### Segmentation (without paging)

&.br2

Initially, virtual memory was implemented with segmentation, where a process' memory is divided into segments for code, stack, heap, etc.
Each segment is given a chunk of contiguous memory, which can be resized, and addresses are defined as the base segment (physical) address with an offset.

In the modern day, segmentation isn't used by itself, since resizing segments could require reordering of memory and [external fragmentation](https://en.wikipedia.org/wiki/Fragmentation_%28computing%29#External_fragmentation), where there simply isn't enough contiguous memory, could occur.


&]



&[slide35

#### Paging

&.br3

The virtual and physical memory is divided into fixed-sized chunks of consecutive addresses.
A virtual memory chunk is called a **page**, while a physical memory chunk is called a (page) **frame**.
Usually both have the same size of (at least) 4 KiB.

Every process has a [page table](https://en.wikipedia.org/wiki/Page_table), which maps the process' pages to system frames.
Thus every memory address is translated from it's page location to a frame location.

&]



&[slide35

Advantages include:

- elimination of [external fragmentation](https://en.wikipedia.org/wiki/Fragmentation_%28computing%29#External_fragmentation)
- increased flexibility, allowed sharing of memory between processes (useful for shared libraries)
- finding free memory is fast and easy, just use the first free pages you see
- frames can be scattered
- more efficient swapping, moving of memory from RAM to disk and back

Disadvantages include:

- longer memory access, because of the page table
- [internal fragmentation](https://en.wikipedia.org/wiki/Fragmentation_%28computing%29#Internal_fragmentation), where one or multiple pages might be partially empty, leaving free space that cannot be used

&]



&[slide

# 3. Linux from the inside

Enough theory, time to have some fun and learn Linux!

We'll be using [Linux Minimal Live](https://ivandavidov.github.io/minimal/#home) (just a bootable ISO) because it's:

- very small, comprised of only the Linux kernel, GNU C library and Busybox  
- made by a fellow Bulgarian ![](/img/bg.svg =30)

We're going to explore and look around the following stuff:

- **Shells**: What a modern shell would often support on a Linux based OS
- **Files and directories**: Naming conventions and commands to work with them
- **Common configuration files**: Relevant configuration files, their purpose and syntax
- **System management**: Other commands with which to modify your system

&]



&[middle18

#### If you're following at home

Owing to it's size, not everything we need comes out of the box, so I created [a script](./mll-set.sh) which installs what's needed.
If you want to try it out yourself, after booting up Minimal Linux Live, type out and run:

```
wget -q http://unsecure.syndamia.com/mll-set.sh && chmod +x mll-set.sh && ./mll-set.sh
```

You might've noticed that the command above doesn't use the normal [https://syndamia.com/talks/linuxgraphy-by-strabo/mll-set.sh](./mll-set.sh) link.
That is because, out of the box, MLL doesn't really support secure connections (https), so I've made only that file available without it.

The script, in summary, installs bash, coreutils and shadow "packages" (from [http://s.minos.io/archive/bifrost/x86\_64](http://s.minos.io/archive/bifrost/x86_64)) to /usr/local/bin, creates /etc/passwd, /etc/bashrc and /etc/profile with some values in them and starts bash.

&]



&[slide

## 3.1. Shells

&.br2

### Variables and data types

Before talking about the different features newer shells support, we'll roughly cover variables and data types (mostly [bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Shell-Parameters) specifics).

A variable, also called parameter, is created with the syntax (NO spaces between the =):

```
name=value
```

Where `name` is either a combination of letters (upper and lower case), numbers and the underscore character, which cannot begin with a number, or one of select few **positional** and **special** parameters.

&]



&[slide35

#### Positional and special parameters

[Positional parameters](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Positional-Parameters) are numbers from 1 to N, where each one corresponds to a shell (or function, or script) argument.

Some of the [special parameters](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Special-Parameters) are:

- `0`, corresponding to the shell's or script's name,
- `@` is all positional parameters, separated by spaces
- `#` is the number of positional parameters
- `?` is the last executed (foreground) process
- `$` is process ID of the current shell.

`value`, without any special surrounding characters (more on that later), is treated as a string (but it mustn't have spaces!), however it may also be omitted, in which case it is the empty string `""`.

&]



&[slide35

#### Data types

In most shells, there are 3 data types: strings, integers and (one-dimensional) indexed arrays.
[bash has more](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#index-declare), like references and associative arrays, but those aren't universal.

A value is a **string** when surrounded by single `'` or double `"` quotes,  
an **integer** when it isn't surrounded by anything and composed only of digits (if there are letters, it is a string; it's [quite tricky](https://tldp.org/LDP/abs/html/untyped.html) when a value is interpreted as a string or integer) and  
an **array** when surrounded by braces `()`, where inside elements are strings or integers, separated by spaces.
An indexed array can also be created by specifying the value at any index:

```
name[index]=value
```

&]



&[slide

#### Mixing types and operations

```
name=value
```

| Type  |Value with|           string operation            |           integer operation           |               array operation               |
|-------|----------|---------------------------------------|---------------------------------------|---------------------------------------------|
|string |          |value                                  |0                                      |value when index is 0, empty string otherwise|
|integer|          |empty string, but sometimes value      |value                                  |value when index is 0, empty string otherwise|
|array  |          |operation is done on element at index 0|operation is done on element at index 0|value                                        |

&]



&[slide

#### Parameter expansion

With [parameter expansion](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Shell-Parameter-Expansion), actions are done with the general variable (entity) or on strings and arrays values.
Operations on integers are done with [arithmetic expansion](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Arithmetic-Expansion). 

Parameter expansion is started with the character `$` and either a name or curly braces.
Retrieving a value is done with the forms `$name`, `${name}` and `${name[index]}` for arrays.

Some parameter expansions in bash:

- `${#parameter}` length of characters
- `${parameter:offset}` and `${parameter:offset:length}` return substring from index offset
- `${parameter/pattern/string}` return a string, where every match on pattern is replaced by string
- `${name[@]}` a (space separated) sequence of indices from 0 to length (for defined values) of array name

&]



&[slide

#### Arithmetic expansion

Arithmetic expansions allow evaluation of [arithmetic expressions](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Shell-Arithmetic).
They're in the form `$(( expression ))`.

Some useful expressions are:

- `++name`, `name++`, `--name`, `name++` postfix and prefix incrementation and decrementation
- `+`, `-`, `*`, `/`, `%` normal arithmetic operators, `**` exponentiation
- `~`, `<<`, `>>`, `&`, `^`, `|` bitwise operators
- `==`, `!=`, `>`, `>=`, `<`, `<=` logical comparison
- `!`, `&&`, `||` logical not, and, or
- `expr ? exprTrue : exprFalse` conditional
- `=`, `+=`, ..., `<<=`, ... assignment operators
- `expr1 , expr2` consecutive expressions and `(expr)` bracing

Expressions work with both variable names and integers.

&]



&[middle

### Bourne shell

Most modern shells support a lot of the more defying features of the [Bourne Shell](https://en.wikipedia.org/wiki/Bourne_shell), which include:

- [Job control](https://en.wikipedia.org/wiki/Job_control_%28Unix%29): Management of currently running groups of processes  
  + `&`: A process can be ran in the background with an ampersand (&) at the end of the command:

    ```
    command [args...] &
    ```

  + `Ctrl+z`: suspend the currently running (foreground) job
  + `bg`: start a suspended job in the background
  + `fg`: resume the last job to be put in the background and make it the current job with which we're interacting
  + `jobs`: list all active (background) jobs

&]



&[middle

- &emsp;
  + Every job is identified with a **Job ID** (number), which you can use by prepending a percentage sign. Example, to resume the job with JID 4:

    ```
    fg %4
    ```
  + For managing everything, usually there is a *job table*.
    Upon shell termination, the shell tells all jobs in that table to terminate and waits for them.  
    `disown`: remove a job from the job table
  + `kill`: send a signal to the process or job. Then that process will have to handle it accordingly.  
    Common ones are `-KILL` to immediately stop the process, `-QUIT` to quit it, `-ABRT` to cancel the current action, `-TERM` shut down (orderly), `-STOP` shut down (forcefully)

&]



&[middle27

- [heredoc](https://en.wikipedia.org/wiki/Here_document): File literal, meaning it is a user "string" which is interpreted as a file
  + start with `<< NAME`, where NAME can be anything you want, it is used to mark the beginning and end of a heredoc. `<<` is also a redirection symbol
  + on every new line write out your text, all characters will be preserved
  + to end it, write out `NAME` on the beginning of a new empty line. Example:

  ```
  cat << MYFILE
  This is
    some
        text
  MYFILE
  ```

- control operators: Control what command is executed, depending on [exit status](https://en.wikipedia.org/wiki/Exit_status)
  + `expr1 && expr2`: Run expr2, only if expr1 exited successfully
  + `expr1 || expr2`: Run expr2 if expr1 exited unsuccessfully
  + `expr1 ; expr2`: Run expr2 after expr1 (sequential execution)

&]


&[middle27

- [redirection](https://en.wikipedia.org/wiki/Redirection_%28computing%29): Outside of having `<` and `>` for I/O redirection, often there is also
  + `>>`: Acts like `>`, putting text into a file, but rather than overwriting everything, it appends it
  + `<<`: As explained, for heredocs
  + `<<<`: To the right is a string ([herestring](https://en.wikipedia.org/wiki/Here_document#Here_strings)), and it is interpreted as a file

    ```
    cat <<< "Hello World!"
    ```

  + **Standard input**, **standard output** and **standard error** are all **files** with which the shell works.
    Typing text in the shell puts it into stdin, command output are put into stdout (and shown to the user) and errors are put into stderr (also shown to the user).  
    Each one of them is numbered from 0 to 2, and you can specify redirection by appending or prepending it to the redirection symbol.  
    * `N>outputfile`: redirects the output from standard stream N into outputfile
    * `N>&M`: redirects the output from standard stream N into standard stream M
    * `N<inputfile`: redirects input file contents to standard stream N
    * `N<&M`: redirects standard stream M to standard stream N

&]



&[middle27

- [built-in test command](https://en.wikipedia.org/wiki/Test_%28Unix%29): With `test` you can do conditional expressions. You can also often use brackets `[ args... ]` instead of `test args...`
  + Some of the available file checks
    * `-e FILENAME`: FILENAME exists
    * `-d FILENAME`: if FILENAME is a directory
    * `-h FILENAME`: if FILENAME is a symbolic link
    * `-w FILENAME`: you can write in FILENAME
  + Some of the available string checks
    * `-n STR`: string STR has nonzero length
    * `-z STR`: string STR has length zero
    * `STR1 = STR2` and `STR1 != STR2`: self explanatory
  + Some of the available number checks
    * `INT1 -eq INT2`: equal integers
    * `INT1 -gt INT2`: INT1 > INT2
    * `INT1 -gt INT2`: INT1 >= INT2
  + Operators: `!` - negation, `-a` - binary AND, `-o` - binary OR, parentheses for grouping (escaped with `\`)


&]



&[slide

- [scripting support](https://en.wikipedia.org/wiki/Shell_script): Running commands from top to bottom of a file
  + `#`: Everything between a # and the end of the line denotes a comment
  + `#!`: If on the first line of the file, is treated as a [shebang](https://en.wikipedia.org/wiki/Shebang_%28Unix%29), which specifies what interpreter to be used for the script

- compound commands: Structures for flow control  
  In all examples, `test-command` is any command (or combination with control operators), and logic is based on exit code.

  + conditional

    ```
    if test-command ; then commands-if-true
    elif test-command ; then commands-if-true
    else commands-if-true
    fi
    ```

    elif statements can appear multiple times, both elif and else are optional

&]



&[slide

- &emsp;
    &.br1

    ```
    case word in (pattern | ...) commands-if-true ;; ... esac
    ```

    Where the beginning brace can be ommited.  
    Patterns are according to [bash's pattern matching](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Pattern-Matching)

  + loops

    ```
    while test-command; do commands-if-true; done
    ```

    ```
    for name in words...; do commands-if-true; done
    ```
    If `in words...` is ommited, positional arguments are used

&]



&[middle35

## 3.2. Files and directories

- `/dev`: devices
- `/proc`: process information

&.br3

- `/bin`: main command programs (binaries), used by users
- `/sbin`: command programs, used for the system operation
- `/usr`: other system resources
  + `/usr/bin`: all other user command binaries

&]



&[slide

#### Everyday file system commands

- `mkdir`: create directories
- `rm`: remove a (hardlink to) file or directory
- `mv`: move file (or directory) to another directory
- `cp`: copy a file
- `ln`: create hard (or soft, with `-s`) links to files (inodes)
- `ls`: list directory contents

#### Other useful file system commands

- `shred`: overwrite file contents
- `dd`: do many things with files, like modifying and duplicating data across devices and partitions, (basic) data recovery and generating files
- `du`: estimate file usage
- `stat`: display status for file or file system
- `chwon`: change owner of an inode
- `chgrp`: change group of an inode
- `chmod`: change file modes
- `find`: search for a file inside a directory (recursively)

&]



&[slide

#### Everyday text (file) commands

- `echo`: prints a given string to standard output
- `cat`: sequentially prints files to standard output
- `less`: view files with support for scrolling
- `head`: print the first lines (or characters) of a file
- `tail`: print the last lines (or characters) of a file

#### Other useful text (file) commands

- `grep`: searches for matches in a file, using simplified regular expressions. `egrep` for extended regex and `fgrep` for string match (as-is).
- `sed`: filter and transform text from files, according to regex
- `tr`: translate, map (more than one to one for example) and delete characters
- `cut`: remove parts of text from each line
- `strings`: find embedded text inside binary files
- `truncate`: shrink or extend the size of a file
- `wc`: count words inside file
- `diff`: compare files and show their differences

&]



&[middle

#### Device commands

- `mount`: bind another file system to a location in the file system
- `umount`: remove binding to a file system
- `lsusb`: lists USB devices
- `lspci`: lists PCI devices

#### System information commands

- `date`: display current date and time
- `hostname`: show (or set) the systems hostname (network name)
- `free`: display free memory on the system

&]



&[slide

#### User and group management

- `useradd`: creates a new user (with a password)
- `userdel`: remove a user and all of his files
- `usermod`: modify a user account, including his name and groups
- `passwd`: change password of a user. With `-l` user could be locked (impossible to log in)

&.br3

- `groupadd`: create a new group
- `groupdel`: remove a group
- `groupmod`: modify an existing group

&.br3

- `w`: show who is logged in and what they are doing
- `who`: show who is logged in
- `whomai`: print username
- `login`: establish a new connection to the system

&]



&[slide

- `/etc`: system configuration files
  &.br2

  + `/etc/group`: the data for all groups is stored here, in the format

    ```
    GROUPNAME:PASSWORD:GID:USERS...
    ```

  + `/etc/passwd`: the data for all users is stored here, in the format

    ```
    USERNAME:PASSWORD:UID:GID:USERINFO:HOMEDIR:SHELL
    ```

  + `/etc/shadow`: stores other user data, especially the password as an encrypted string, in the format

    ```
    USERNAME:PASSWORD:LASTCHANGED:MINBETWEENCHANGES:MAXBETWEENCHANGES:WARNEXPIRE:INACTIVEDAYS:EXPIRE
    ```

&]



&[slide27

- &emsp;

  + `/etc/fstab`: read on boot by mount to setup devices in fs

    ```
    # device-spec   mount-point  fs-type  options              dump  pass
    /dev/sda1       /            ext4     defaults             1     1
    UUID=123A-456B  /boot        vfat     noauto,noatime       1     2
    LABEL=Vault     /mnt/Vault   auto     nosuid,nodev,nofail  0     0
    ```

  + `/etc/hosts`: list of host names (domain names) and their corresponding IP address, when a DNS server doesn't do the job
    
    ```
    # ipaddress    domains
    127.0.0.1      localhost mywebsite.com something.else
    62.44.101.138  my.uni
    ```

  + `/etc/bashrc`: global defaults and aliases used by the bash shell
  + `/etc/motd`: message of the day (unexecuted text), shown after login but before the shell is ran

&]



&[middle35

- `/usr`: other system resources
  + `/usr/lib`: library files
  + `/usr/local`: local, system software
  + `/usr/share`: architecture independent data, like manuals
- `/var`: variable data, like logs and cache
- `/home`: home directories of all users
- `/lib`: libraries and kernel modules
- `/mnt`: mounted temporary filesystems
- `/root`: home directory for the root user

&]



&[slide

# 4. Software licensing

It is important to note that most software in Linux, including the kernel itself, is under a variety of open licenses.
An open license is a license which allows others to reuse the original work, under some restrictions.

For software, such a license is applied to the source code from which the original application was made.
Some commonly used ones, with a (NON-LEGALLY BINDING) summary include:

- [MIT](https://en.wikipedia.org/wiki/MIT_License): the author is not responsible for anything that the code does
- [BSD-3-Clause](https://en.wikipedia.org/wiki/BSD_licenses): use the same license (copyleft), the author is not responsible for anything that the code does
- [GNU GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License): disclose the original project and use the same license ([copyleft](https://en.wikipedia.org/wiki/Copyleft))

This is important, since pretty much everything can be freely modified by anyone (for the better or worse, but generally better).

&]



&[slide35

# 5. What makes up a useful OS

A Linux-based operating system is nothing more than the Linux kernel, combined with an assortment of programs.  
Overall, the main elements that make up a useful *desktop* operating system (alongside a command-line shell) are:

&.br1

1. **Bootloader**: load the kernel and operating system
2. **Init system**: well managed way to start everything inside the OS
3. **Package manager**: way to manage our binaries
4. **Desktop manager**: graphical shell

&]



&[slide

## 5.1. Bootloader

The computer boot process is a relay race.

In a "standard" BIOS-MBR boot configuration:

```
BIOS -> Master Boot Record -> Active Parition -> Bootloader ->
-> Boot menu (optional) -> Kernel -> Everything else
```

- [BIOS](https://en.wikipedia.org/wiki/BIOS): a small chip with code that is ran upon power up
- [MBR](https://en.wikipedia.org/wiki/Master_boot_record): small amount of space at the very start of the disk, that contains some (bootstrap) code and partition information. The BIOS hands off execution to the bootstrap code.
- Active partition: MBR loads and runs code from the beginning of the partition (boot sector), which starts the bootloader
- [Bootloader](https://en.wikipedia.org/wiki/Bootloader): the first complex program, which manages kernels, shows a menu, allows for debugging and so on

&]



&[middle

The BIOS is baked into the motherboard, the MBR is pretty universal (and it's less than 512 bytes) and the kernel we know and love.

That leaves us with the need for a bootloader.
On Linux, the most used one is called [GRUB](https://en.wikipedia.org/wiki/GNU_GRUB).

It's config is found in `/etc/default/grub` (which is then fully generated with `grub-mkconfig`) and can be "installed in the partition" with `grub-install`.

```
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_TIMEOUT_STYLE=menu
GRUB_CMDLINE_LINUX=""
GRUB_CMDLINE_LINUX_DEFAULT=""
GRUB_GFXMODE=640x480
```

&]



&[slide27

## 5.2. Init system

Now we need to load the user stuff, like a network manager, the graphical user interface and so on.  
In Minimal Linux Live, this is done by some hard written scripts, which aren't very modular or extensible.

[Init systems](https://en.wikipedia.org/wiki/Init) use configuration files, called service, to start a program in some way.

- [systemd](https://en.wikipedia.org/wiki/Systemd), which also aims to also provide easy system configuration
- [openrc](https://en.wikipedia.org/wiki/OpenRC), a simple and modern system

```
[Unit]
Description=Service
[Service]
Type=simple
User=root
ExecStart=/usr/local/sbin/script.sh
[Install]
WantedBy=multi-user.target
```

&]



&[slide

## 5.3. Package manager

&.br1

Programs are just executable files, usually placed in a specific folder, like `/usr/bin`.

- MLL comes with ~100 executables (and ~230 after my script)
- my Debian server has ~1000 installed packages
- my current desktop Gentoo setup has ~1500 installed packages
- my old Fedora setups have reached ~3000 packages on multiple occasions

Updating all of them means replacing all of the binaries, means tracking where each one came from, installing a new one might require installing multiple others and so on.

On modern systems, managing binaries is automated by an application called a package manager.

&]



&[middle35

Additional benefits of package management automation include:

- checking for package validity (and tampering)
- utilising multiple server mirrors
- automatic dependency management and installation
- version caveats
- (indirectly) improved stability because of maintainer compatibility checks

&.br1

```
apt install PKG
apt remove PKG
apt upgrade
```

&]



&[slide

## 5.4. Window systems

A widow system is your graphical shell: the graphical way to interact with your computer (the kernel).
It's main components are:

&.br1

- **Window manager**: the overall system with which you create, remove, resize and work with windows. They are generally separated into two types:
  + *Stacked*: this is what you know, windows are just rectangles floating about, and can be put one over each other
  + *Tiled*: windows take up a predefined (but changeable) portion of the screen
- **Compositor**: adds an unseen window buffer, allowing additional processing, before a window is shown. This powers transparency, animation effects and more.
- **Session manager**: graphical application that manages the window managers (outside WMs), think of it as a login window

&]



&[slide27

# 6. War on distros

A distribution is a complete set of of all applications that you might use.

Overall, there are 4-5 distributions from which 90% of all other distros are based upon:

1. [Slackware](http://www.slackware.com/): as the oldest still maintained distro, it served as inspiration for a lot of other distros. It aims to be as simple and as close to UNIX as possible.
2. [Debian](https://www.debian.org/) (and [Ubuntu](https://ubuntu.com/)): it is the most popular distribution (if we also include Ubuntu). Ubuntu aims to be modern and fancy, while Debian aims to be as stable as possible, which makes it the most popular in server usage.
3. [Gentoo](https://www.gentoo.org/): the distribution which defined non-binary software distribution
4. [Arch Linux](https://archlinux.org/): a very popular and modern distribution with the goal of providing the latest and greatest software.

We haven't touched too much, but Linux distributions aren't limited to desktop or server computers.

1. [AOSP](https://en.wikipedia.org/wiki/Android_%28operating_system%29#AOSP): the base of the whole Android operating system
2. [OpenWRT](https://en.wikipedia.org/wiki/OpenWrt): a router operating system

&]



&[slide35

# 7. Demos - virtual machine and setup from zero

&.br2

Now for the best part: we'll be installing a Linux distribution on a virtual machine and then directly on a laptop, running Windows 10!

I've chosen to install [Linux Mint](https://linuxmint.com/), since it is generally targeted towards newcomers and will feel familiar enough to Windows users.

Currently there won't be an official recording of the process, you should've been here live!

&]



&[title35

# Thank you for your time

&]



&[slide18

# Sources

All of these contain, at most, small paraphrased sentences in the slides.
They served as personal educational and reference tools.

- [wikipedia](https://en.wikipedia.org/) and [stackexchange](https://stackexchange.com/) have been used extensively and linked where appropriate.
- [CS416 - Operating Systems Design](https://people.cs.rutgers.edu/~pxk/416/index.html) by [Paul Krzyzanowski](https://people.cs.rutgers.edu/~pxk/rutgers/) at [Rutgers State University](https://www.cs.rutgers.edu/)
- [Origins and History of UNIX, 1969-1995](https://web.archive.org/web/20220713165958/https://homepage.cs.uri.edu/~thenry/resources/unix_art/ch02s01.html) from [The Art of Unix Programming](https://web.archive.org/web/20220611160552/https://homepage.cs.uri.edu/~thenry/resources/unix_art/index.html) by Eric Steven Raymond
- [Ken Thompson interviewed by Brian Kernighan at VCF East 2019](https://www.youtube.com/watch?v=EY6q5dv_B-o) by [Vintage Computer Federation](https://vcfed.org/)
- [Pseudo Terminals presentation](http://www.cs.uah.edu/~kkeen/CS590/Pseudo%20Terminals.ppt) from [CS590](http://www.cs.uah.edu/~kkeen/CS590/) by [Kevin J. Keen](http://www.cs.uah.edu/~kkeen/kkeen.html) at [University of Alabama in Huntsville](https://www.uah.edu/)
- [Hard links and Unix file system nodes](https://teaching.idallen.com/dat2330/04f/notes/links_and_inodes.html) from [DAT 2330 Introduction to Operating Systems](https://teaching.idallen.com/dat2330/04f/) by [Ian D. Allen](http://www.idallen.com/)
- [Virtual Memory, OS Course Notes](https://www.cs.uic.edu/~jbell/CourseNotes/OperatingSystems/9_VirtualMemory.html) by [Dr. John T. Bell](https://www.cs.uic.edu/~jbell/) at [University of Illinois Chicago](https://www.cs.uic.edu),  
  which is directly based on [Operating System Concepts, 9th Edition](https://archive.org/details/operating-system-concepts-9e) by [Abraham Silberschatz](https://codex.cs.yale.edu/avi/)
- [Chapter 9 Preview - Virtual Memory](http://csapp.cs.cmu.edu/3e/home.html) from [Computer Systems: A Programmer's Perspective](http://csapp.cs.cmu.edu/) by [Randal E. Bryant](http://www.cs.cmu.edu/~bryant) and [David R. O'Hallaron](http://www.cs.cmu.edu/~droh)
- [Memory Management, Lecture Notes](https://www.cse.iitb.ac.in/~mythili/teaching/cs347_autumn2016/notes/07-memory.pdf) by [Mythili Vutukuru](https://www.cse.iitb.ac.in/~mythili/)
- [Paging advantages and disadvantages](http://www.cs.iit.edu/~cs561/cs351/VM/paging%20advantages%20and%20disadvantages.html) by Charles R. Bauer at [Computer Science Departmemnt of Illinois Institute of Technology](http://www.cs.iit.edu/)
- [Bash Reference Manual](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html) by the [GNU project](https://www.gnu.org/) and [ksh manual](https://man.openbsd.org/ksh) by the [OpenBSD project](https://www.openbsd.org/)
- [Manual of the Bourne Shell on Version 7](https://www.in-ulm.de/~mascheck/bourne/v7/), archived by [Sven Mascheck](https://www.in-ulm.de/~mascheck/)
- [/usr hierarchy](https://www.linuxbase.org/betaspecs/fhs/fhs/ch04.html) from [Filesystem Hierarchy Standard](https://www.linuxbase.org/betaspecs/fhs/fhs/index.html) by [Linux Foundation](https://www.linuxfoundation.org/), Daniel Quinlan, Paul 'Rusty' Russell, Christopher Yeoh
- [Filesystem Hierarchy Standard](https://www.pathname.com/fhs/pub/fhs-2.3.pdf) by Rusty Russel, Daniel Quinlan and Christopher Yeoh
- [User & Group Names](https://refspecs.linuxbase.org/LSB_3.1.1/LSB-Core-generic/LSB-Core-generic/usernames.html#FTN.AEN26405) from [Linux Standard Base Core Specification](https://refspecs.linuxbase.org/LSB_3.1.1/LSB-Core-generic/LSB-Core-generic/book1.html) by [Free Standards Group](https://en.wikipedia.org/wiki/Free_Standards_Group)
- [Managing User Accounts](https://docstore.mik.ua/orelly/linux/run/ch05_07.htm) from [Running Linux, Third Edition](https://docstore.mik.ua/orelly/linux/run/index.htm) by Matt Welsh, Matthias Kalle Dalheimer, and Lar Kaufman
- [The BIOS/MBR Boot Process](https://neosmart.net/wiki/mbr-boot-process/) by [NeoSmart's Knowledgebase](https://wiki.neosmart.net/)

# Licensing

Images on slides "2.1. shell" and "2.2. UNIX-style file system" are made by me and [licensed as content](/license.html).  
The script [mll-set.sh](http://syndamia.com/talks/linuxgraphy-by-strabo/mll-set.sh) is licensed under MIT (though I don't see it as substatial enough to be licensed under anything).

&]
