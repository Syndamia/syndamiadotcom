# Slides

These are the different presentations I've made and possibly presented.

<br>

#### *2023*

&.gb 19 Jan &emsp; [Linuxgraphy by Strabo](./linuxgraphy-by-strabo) &emsp; *Last presented: 22.01.2023*
: A presentation talking about some of the history of UNIX and how Linux works

#### *2022*

&.gb 27 Sep &emsp; [Demo presentation](./demo) &emsp; *Won't be presented*
: Showcase of my presentation system
