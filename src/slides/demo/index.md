# Demo presentation

This is a small presentation, explaining how my custom system for making presentations works.

&:important This page is [due for rework](/rework.html), I've switched to MEL from ftags

&.presentation

&[title35

# My presentation system
&(font18 *Syndamia AT [syndamia.com](https://syndamia.com)* &)

Before we go any further, click the **icon in the middle** right above this rectangle, to the left.

Now the presentation should fill out the entire page. On the bottom right are your controls.

Press the *right arrow* in the controls to go to the next slide.

&]



&[slide27

# Key bindings

&.br3

|Action              |Keyboard keys                                    |
|--------------------|-------------------------------------------------|
|Go to previous slide| Backspace, Page up, Left arrow, Up arrow        |
|Go to next slide    | Space, Enter, Page down, Right arrow, Down arrow|
|Toggle presenting   | P, T                                            |
|Stop presenting     | Esc                                             |
|Go to first slide   | Home                                            |
|Go to last slide    | End                                             |

&]



&[slide

# Introduction

&.br1

I created a simple system for making presentations with slides in them.
The gist of it is: different [Markdown](https://en.wikipedia.org/wiki/Markdown) paragraphs are put into a rectangle, each rectangle representing a slide, with some added options for layout and format.

## ftags

An ftag (["format tag"](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/awk-edits#ftags)) is what I call a short string of characters, that starts with two ampersands (&&).
Each ftag is a "marker" that gets replaced by some other text (usually [HTML tags](https://en.wikipedia.org/wiki/HTML)).

An easy example is the image of the Great Britain flag (part of [Flags.awk](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/awk-edits/Flags.awk), not the presentation system!): typing out &&GB (in lowecase), gets replaced by the actual image - &&gb.

&]



&[slide

## Why?

- Programs like Microsoft PowerPoint and LibreOffice Impress have very poor compatibility, between programs, versions, languages and even operating systems.
  A web-based solution is significantly more consistent and flexible.

- Ideally, the system would be made well enough that creating a presentation would be significantly faster than with one of those.

- Making my own system means I can tailor it to my needs and preferences.
  Looking around other [HTML-based presentation tools](https://gist.github.com/vasilisvg/1611562):
  + [reveal.js](https://revealjs.com/) is way too heavy and fancy for my needs (animations are nice, but I'm fine going without them)
  + [DZSlides](https://paulrouget.com/dzslides/) and [S5](https://meyerweb.com/eric/tools/s5/) are really nice, however both are HTML-centric, and I want my slides to be written in Markdown.
    Creating ftags that are transformed to DZSlide HTML is possible, but at that point I should just make my own system.
  + and pretty much everything else falls into either category

&]



&[slide

# Formatting

Every (presentation-specific) ftag is listed and explained in [Presentation.awk](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/awk-edits/Presentation.awk).
To summarize, each slide is wrapped in `&&slide` and `&&/slide` (or `&&clide` and `&&/clide`), you can change font size for specific portions, you can add basic (without formatting) tables for fancy layout, you can align text and elements and you can add line breaks.

And they can work together in most different combination.

&.br1

Though, remember that everything gets translated to HTML, so don't do (even if it works)

```
&&slide &&font20 &&/slide &&/font
```

Enough talking, here are examples!

&]



&[slide

&(font18 Font-size 18px &)
&(font20 Font-size 20px &) &(font22 Font-size 22px &) &(font25 Font-size 25px &)
Font-size 30px (default)
&(font35 Font-size 35px &)

&.br5

&(fright
This text is floated to the right
&)

&.br2

&(centered While this one is centered &)

&.br2

&(fright Floated right<br>Floated right<br>Floated right &)

But you may realize that text floated to the right will make other text wrap around it!
Maybe this should be changed?

&]



&[slide18

The source Markdown for the predvious slide (without the pipes to the left) is:

```
&&slide

&&font18 Font-size 18px &&/font
&&font20 Font-size 20px &&/font &&font22 Font-size 22px &&/font &&font25 Font-size 25px &&/font
Font-size 30px (default)
&&font35 Font-size 35px &&/font

&&lineb5

&&fright
This text is floated to the right
&&/fright

&&lineb2

&&centered While this one is centered &&/centered

&&lineb2

&&fright Floated right<br>Floated right<br>Floated right &&/fright

But you may realize that text floated to the right will make other text wrap around it!
Maybe this should be changed?

&&/slide
```

&]



&[middle &{row

You can also easily tabularize

&{column &(fright

- List Element 1
- List Element 2

&) &}

&.newrow

**any** sort of content

&}

&.br1

&{sqrow

So you can very easily put images next to your text

&(centered ![](/tutorials/fix-no-frame-buffer-plan9/img/rio-default-login.png =120x) &)

&} &]



&[slide18

The source Markdown for the predvious slide (without the pipes to the left) is:

```
&&clide &&table

You can also easily tabularize

&&cont &&fright

- List Element 1
- List Element 2

&&/fright &&/cont

&&tarow

**any** sort of content

&&/table

&&lineb1

&&ftable

So you can very easily put images next to your text

&&centered ![](/tutorials/fix-no-frame-buffer-plan9/img/rio-default-login.png =120x) &&/centered

&&/table &&/clide
```

&]



&[slide27

# Other features

As you've obviously noticed, the presentation "system" also adds a navigation bar, with which to present and navigate slides.
You can also print very easily the whole presentation on A4 sheets of paper, since that's the exact size of each slide.

Pretty much all other website functionalities, like themes and click-to-zoom images, aren't inhibited.
The slides are easily scrollable/browseable and if you don't have JavaScript enabled, everything will be shown as a normal(-ish) article, without any *major* styling issues.

## Setup

Everything is contained in 3 files: [Presentation.awk](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/awk-edits/Presentation.awk), [presentation.js](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/src/scripts/presentation.js) and [presentation.css](https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/src/presentation.css), which pretty much work on your "compiled" HTML.

`Presentation.awk` handles replacement of ftags (only in `/talks` pages!).
`presentation.js` is inserted into the HTML by `&&presentation`, and handles the navigation bar as well as insertion of `presentation.css` (the overall presentation-specific styling).

&]



&[title

# Thats All. Thank you for your time!

&(fright &(font18
*P.S. Press the same button (next to the print button) with which you started presenting, to stop presenting ;)*
&) &)

&]
