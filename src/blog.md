# Blog

Articles detailing personal experiences or exploration of certain topics.

<br>

&.bg 18 Nov &emsp; [Суичът изгоря!](/blog/switchut-izgorq)
: `Shallow dive` | `Story` | `Networking` `Switch` `NetworkPartner`

&.gb 06 Oct &emsp; [The physics behind a black hole shader](/blog/the-physics-behind-a-black-hole-shader)
: `Deep dive` | `Physics` | `General theory of relativity` `Celestial mechanics` `Gravity` `Black Hole` `Algorithm`

&.gb 20 Feb &emsp; [Microsoft and FOSS: a fire to be extinguished](/blog/microsoft-and-foss-extinguish)
: `Shallow dive` | `Opinion` | `Microsoft` `GitHub` `Linux` `FOSS`

#### *2023*

&.gb 30 Aug &emsp; [ThinkPad X270 - my favorite machine](/blog/thinkpad-x270-my-favorite-machine)
: `Medium dive` | `Review` | `Lenovo` `ThinkPad` `Yoga`

&.gb 19 July &emsp; [Icon fonts aren't evil, you just have to care](/blog/icon-fonts-arent-evil-you-just-have-to-care)
: `Shallow dive` | `Web development` | `Icon font` `SVG` `Accessibility` `FontForge`

&.bg 14 July &emsp; [ФМИ и висшето образование в програмистката индустрия](/blog/fmi-i-vissheto-obrazovanie-v-programistkata-industriq)
: `Deep dive` | `University` | `FMI` `Education`

&.gb 07 June &emsp; [Friendship ended with Funtoo, now Gentoo is my best friend](/blog/friendship-ended-with-funtoo-now-gentoo-is-my-best-friend)
: `Shallow dive` | `Linux` | `Funtoo` `Gentoo` `portage` `packages` `emerge` `ebuild`

&.gb 01 May &emsp; [BASH is weird](/blog/bash-is-weird)
: `Shallow dive` | `bash` | `Linux` `scripting`

&.gb 18 Apr &emsp; [Sane explanation of compilers: Part 1](/blog/sane-explanation-of-compilers/part-1.html)
: `Deep dive` | `Compilers` | `C` `yacc` `bison` `math` `Trees` `Regex` `Grammar` `Languages`

&.gb 13 Jan &emsp; [Plan 9 in Linux: Mouse menus](/blog/plan9-in-linux-mouse-menus)
: `Medium dive` | `Linux` | `Plan 9` `mouse` `bash` `script` `X11` `openbox` `jgmenu` `plumber`

#### *2022*

&.gb 23 Dec &emsp; [2.6 years of web design](/blog/2.6-years-of-web-design)
: `Shallow dive` | `Web design`

&.gb 13 Dec &emsp; [When a language forces simpler thinking](/blog/when-a-language-forces-simpler-thinking)
: `Shallow dive` | `Common Lisp` | `Advent of Code` 

&.gb 04 Dec &emsp; [Taming The Fun-too](/blog/taming-the-fun-too)
: `Story` | `Funtoo` | `Linux` `Gentoo` `packages`

&.gb 10 Sep &emsp; [Encrypted email](/blog/encrypted-email)
: `Medium dive` | `Email` | `Encryption` `Security` `PGP` `S/MIME` `Protonmail` `Tutanota`

#### *2020*

&.gb 20 May &emsp; [Mundus: My first shot at game development](/blog/mundus-first-shot-at-gamedev)
: `Deep dive` | `Gamedev` | `2D` `Sandbox` `Crafting` `Building` `Fighting` `GtkSharp 2.0` `C#`
