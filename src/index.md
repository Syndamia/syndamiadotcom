# Welcome

My name is Kamen and this is my personal website.
Here I share/store certain parts of my work and write about whatever I find interesting in the computing world.
Have a look around and enjoy your stay!

<br>

---

**Shortcuts:**
[Source repository](https://gitlab.com/Syndamia/syndamiadotcom) | [Contact information](/about) | Resume in [English](/files/resume-en.pdf) &amp; [Български](/files/resume-bg.pdf)

---

<br>

<a class="borderless" href="https://www.vim.org/"><img class="oldschool-badge" src="./img/created.with.vim.gif" alt="Created With VIM"></a>
<a class="borderless" href="https://www.anybrowser.org/campaign/"><img class="oldschool-badge" src="./img/abasat.gif" alt="Viewable With Any Browser"></a>
<a class="borderless" href="https://spyware.neocities.org/"><img class="oldschool-badge" src="./img/watchdog.png" alt="Spyware Watchdog"></a>
<a class="borderless" href="https://observatory.mozilla.org/analyze/syndamia.com"><img class="borderless" src="/img/mozilla-observatory.png" alt="Mozilla Observatory Score"></a>
<a class="borderless" href="https://securityheaders.com/?q=syndamia.com&followRedirects=on"><img class="borderless" src="/img/security-headers.png" alt="Security Headers Score"></a>
<a class="borderless" href="https://www.ssllabs.com/ssltest/analyze.html?d=syndamia.com"><img class="borderless" src="/img/ssl-labs.png" alt="SSL Report"></a>

### Keyboard navigation (requires javascript)

|Key|Available|Action|
|---|---------|------|
|C  |Everywhere|Toggles the theme between light and dark|
|W  |Everywhere|Toggles the page width|
|E  |Everywhere|Expands/shrinks the sidebar|
|K, Backspace, PageUp, Left arrow, Up arrow|Presentations|Go to previous slide|
|J, Spacebar, Enter key, PageDown, Right arrow, Down arrow|Presentations|Go to next slide|
|P, T|Presentations|Start or stop (toggle) presenting|
|Escape key|Presentations|Stop presenting|
|Home|Presentations|Go to first slide|
|End|Presentations|Go to last slide|
