# Tutorials

Educational articles with the sole goal of explaining technologies, processes or anything in between.

<br>

#### *2023*

&.gb 16 Dec &emsp; [Fix "qc timeout" kernel log error](/tutorials/fix-qc-timeout)
: `Linux` | `kernel` | `libata` `qc` `logs` `DMA`


&.gb 28 Jun &emsp; [Fix networked Brother printers not working in Linux](/tutorials/fix-network-brother-printers)
: `Linux` | `CUPS` | `Printers` `Brother` `Network` `avahi`

&.gb 14 Jun &emsp; [Fix "exec /gns3/init.sh: no such file or directory"](/tutorials/gns3-initsh-no-such-file)
: `Linux` | `GNS3` | `Docker` `busybox`

&.gb 26 Apr &emsp; [Install drivers for Epson scanners](/tutorials/install-drivers-for-epson-scanners)
: `Linux` | `SANE` | `Scanners` `Epson` `epkowa` `iscan`

&.gb 25 Apr &emsp; [Managing Android apps with ADB (and without root)](/tutorials/managing-apps-with-adb-without-root)
: `Android` | `ADB` | `Apps` `Linux` `Windows`

&.gb 01 Jan &emsp; [virt-manager freezing up when clicking Browse in any window](/tutorials/virt-manager-freeze-on-browse)
: `Linux` | `virt-manager` | `QEMU/KVM` `libvirt` `Linux` `GTK`

#### *2022*

&.gb 16 Sep &emsp; [How to encrypt email](/tutorials/how-to-encrypt-email)
: `All` | `Email` | `PGP` `S/MIME` `Email Client` `Gmail` `Outlook` `Protonmail`

&.gb 30 Jul &emsp; [Fix "No frame buffer" error in <span class="no-wrap">Plan 9</span>](/tutorials/fix-no-frame-buffer-plan9)
: `Plan 9` | `Configuration` | `QEMU/KVM` `Linux` `monitor`

&.gb 30 Jul &emsp; [Install <span class="no-wrap">Plan 9</span> with virt-manager](/tutorials/install-plan9-with-virt-manager)
: `Plan 9` | `Installation` | `Setup` `QEMU/KVM` `virt-manager` `Linux`

#### *2021*

&.gb 01 Nov &emsp; [How to turn off notifications on Windows](/tutorials/turn-off-notifications-windows)
: `Windows 10` | `Settings` | `Notifications`

&.gb 01 Nov &emsp; [How to disable Sticky Keys prompt](/tutorials/disable-sticky-keys-popup)
: `Windows` | `Settings` | `Sticky Keys`

&.gb 26 Oct &emsp; [How to remove Windows Weather Widget](/tutorials/remove-weather-widget)
: `Windows 10` | `Settings` | `Taskbar` `Weather Widget`

&.gb 04 Oct &emsp; [Windows Run dialog](/tutorials/windows-run-dialog)
: `Windows` | `Commands` | `Run` `Settings`

&.gb 03 Oct &emsp; [How to export Skype data](/tutorials/export-skype)
: `Skype` | `Export` | `Archive` `skype-parser`

#### *2020*

&.gb 24 Jul &emsp; [How to use Jira](/tutorials/how-to-use-jira)
: `Project Management` | `Jira` | `Agile` `Issue tracking` `Bug tracking` `Kanban` `Atlassian`

&.gb 21 May &emsp; [Connect to Mysql in .NET and Entity Framework](/tutorials/connect-to-mysql-in-dotnet-ef)
: `ASP.Net` | `MySQL` | `Entity Framework` `Pomelo`

