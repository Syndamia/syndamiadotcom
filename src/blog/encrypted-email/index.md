# Encrypted email

## Contents

1. [Encryption and signature](#encryption-and-signature)
2. [OpenPGP](#openpgp)
   1. [PGP/Inline](#pgpinline)
   2. [PGP/MIME](#pgpmime)
3. [S/MIME](#smime)
4. [reop](#reop)
5. [Post-email systems](#post-email-systems)
6. [Centralized non-email systems](#centralized-non-email-systems)
   1. [Protonmail](#protonmail)
   2. [Tutanota](#tutanota)
7. [Conclusion](#conclusion)

When you try and look around for articles, regarding encrypted emails, all you'll find is people complaining about it's difficulties and shortcomings. But, no one actually explains what to do if you, nevertheless, want to encrypt your emails! At best most articles will just [shove some product up your face](https://www.virtru.com/blog/arent-people-using-email-encryption).

That's why, after a fairly lengthy research process, I've decided to do just that. These are the more common ways to encrypt your email with their strengths and weaknesses. I also wrote a (somewhat rough) [tutorial on how to setup](/tutorials/how-to-encrypt-email) [OpenPGP](#openpgp) or [S/MIME](#smime) in practice.

*Note:* What I'm referring to is end-to-end encryption, where you send an encrypted email, and the recipient decrypts it. Encryption between you and a mail server is practically a given and is setup by the mail server administrator.

## Encryption and signature

Unless stated otherwise, all of the listed encryption (and signature) schemes work by utilizing [public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography).

In brief, if you are sending an encrypted email, the **recipient** needs to have generated a public and private key pair. Then you need to have acquired **their public key**. Finally, if you want to send them an encrypted email:

1. You encrypt **your** email with **their public key**
2. The recipient decrypts **your** message with **their private key**

And if someone wants to send **you** an encrypted email, **you** need to generate a public and private key pair, share **your public key** with them, and when you receive the message, decrypt it with **your private key**.

Every email you send can be [digitally signed](https://en.wikipedia.org/wiki/Digital_signature) (not to be confused with [electronic signatures](https://en.wikipedia.org/wiki/Digital_signature)), meaning the message is (cryptographically) "signed", and with this signature, the recipient can check if the message was changed during transit. Digital signatures also use public-key cryptography, but the other way around:

1. You sign **your** email with **your private key**
2. Then the recipient verifies **your** message with **your public key**.

This might sound a bit complicated, but all you need to worry about is you and the recipient generating these public-private key pairs, exchanging public keys, and your email client should handle the rest. And if you use a tool like [pEp](https://www.pep.security/en/), the creation and exchange of keys can be easily automated.

## OpenPGP

By far, the most popular way to encrypt your emails is with [OpenPGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy). PGP is the original program which implemented the encryption scheme, while OpenPGP is the standard/encryption scheme, which was created from PGP (so when I say OpenPGP, I mean a program that implements OpenPGP).

By design, it relies on a [web of trust](https://en.wikipedia.org/wiki/Web_of_trust), the idea being, different people sign each others public keys (sometimes at [key signing parties](https://en.wikipedia.org/wiki/Key_signing_party)), and you know that a specific key corresponds to a specific person, only when a large amount of people have signed it.

OpenPGP divides the security community quite a lot, some [abolish it](https://restoreprivacy.com/let-pgp-die/) while other still [defend it](https://arstechnica.com/information-technology/2016/12/signal-does-not-replace-pgp/), but at the end of the day, it's very widespread and if someone encrypts their email, chances are it's with OpenPGP (unless they're in a business, where [S/MIME](#smime) rules).

There are two main ways to encrypt an email with OpenPGP:

### PGP/Inline

The first is called "PGP/Inline", with which you encrypt only the message itself. That's it. And there are [other limitations](https://josefsson.org/inline-openpgp-considered-harmful.html) too, to name a few:

1. You can't encrypt attachments, period.
2. Your message can only be plain ASCII text characters. So any Unicode character (probably) won't work, and HTML messages are out of the question.

Though, it's not all bad. If you can survive only sending text to other people, PGP/Inline might be a better option:

1. It's much easier to work with, you can directly copy the encrypted PGP message and decrypt it however you like
2. The other option, PGP/MIME, is much harder to implement properly and there have been cases where [it's implementation is flawed](https://web.archive.org/web/20180625054201/https://www.whonix.org/blog/pgp-inline-vs-mime)

### PGP/MIME

"PGP/MIME" is the second option. As it's name suggests, you can encrypt [MIME](https://en.wikipedia.org/wiki/MIME) data with PGP, which by definition means it supports any content type. This includes:

1. Support for Unicode characters and HTML messages
2. Attachments of any type
3. Delivery status report

Basically, any [multipart subtype](https://en.wikipedia.org/wiki/MIME#Multipart_subtypes). Also, I should mention, the message structure and all metadata is hidden by design.

PGP/MIME is capable of a lot more, and is [generally preferred](https://www.mailpile.is/blog/2014-11-21_To_PGP_MIME_Or_Not.html) over PGP/Inline. Nonetheless, it has it's shortcomings:

1. As already mentioned, it is hard to implement and [flawed implementations](https://web.archive.org/web/20180625054201/https://www.whonix.org/blog/pgp-inline-vs-mime) have been made
2. Everything is encrypted together, so you'll need to download the whole email, including all attachments, to decrypt it
3. If your client doesn't support it, manual decryption takes a bit more work

## S/MIME

[S/MIME](https://en.wikipedia.org/wiki/S/MIME), roughly said, combines and extends the [Cryptograhic Message Syntax](https://en.wikipedia.org/wiki/Cryptographic_Message_Syntax) encryption scheme and [MIME](https://en.wikipedia.org/wiki/MIME) standard to create a singular, email-oriented standard.

Unlike OpenPGP it doesn't use a web of trust, but a [Public key infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure). This means that, rather than people exchanging certificates between themselves, a [Certificate Authroity](https://en.wikipedia.org/wiki/Certificate_authority) connects public keys with entities (people or organizations for example).

So, already, it has a huge advantage, you don't need to exchange keys, since the email client will handle it. S/MIME is most often used by corporations/organizations, so support in commercial mail clients like [Google Workspace Enterprise Edition](https://support.google.com/a/answer/6374496?hl=en#zippy=) and [Outlook Enterprise/Business](https://support.microsoft.com/en-us/office/encrypt-messages-by-using-s-mime-in-outlook-on-the-web-878c79fc-7088-4b39-966f-14512658f480) ([Exchange Online has to be configured](https://docs.microsoft.com/en-us/exchange/security-and-compliance/smime-exo/configure-smime-exo?view=o365-worldwide) by an administrator and can be used only with [paid clients](https://docs.microsoft.com/en-us/deployoffice/endofsupport/microsoft-365-services-connectivity)) is available.

On the other hand, you can browse [Google's](https://support.google.com/a/answer/7448393?hl=en&ref_topic=9061730) and [Microsoft's](https://docs.microsoft.com/en-us/security/trusted-root/participants-list) trusted (root) CAs all you want, but you'll find that there is only one place where you could register a certificate for free: [Actalis](https://www.actalis.com/s-mime-certificates.aspx). A good tutorial on the procedure can be found [here](https://www.dannyguo.com/blog/how-to-get-a-free-s-mime-certificate/), but beware, *they* generate the private key.

Otherwise, it's email encryption capabilities are the same as those outlined in [PGP/MIME](#pgpmime).

## reop

[reop](https://flak.tedunangst.com/post/reop) is a very new, simple and "completed" system. The thing is, it's only a single command line utility that uses some encryption schemes (picked by the [nacl](http://nacl.cr.yp.to/index.html) library) for it's own unique system. Currently, there doesn't seem to be any email clients that support it.

It is also fairly limited:

> no support for key revocation, or long chains of certificates, or partial trust, or key servers

However, as explained in it's [article](https://flak.tedunangst.com/post/reop), that might not be that big of a deal. Finally, there are very very few people that use it. From experience, I have only seen [Haelwenn Monnier](https://hacktivis.me/about) use it.

The only reason for using reop is if you want a very small system with a very simple source code behind it. Nevertheless, it's still a viable alternative.

## Post-email systems

There is a somewhat old [document](https://github.com/OpenTechFund/secure-email), created by the [OpenTechFunc](https://www.opentech.fund/), which outlines very thoroughly all "next generation secure email or email-like communication" technologies.

In it, they define the term "[post-mail](https://github.com/OpenTechFund/secure-email#post-email-alternatives)" as:

> projects to create alternatives to email that are more secure yet still email-like

And outline zero trust and fingerprinting as the main advantages of these systems. Also most of these systems are [peer-to-peer](https://en.wikipedia.org/wiki/Peer-to-peer).

Given that these projects aren't really email, I won't dive much deeper into it. But I will mention, almost all listed projects seem to have been abandoned. Those still under active development are [goldbug](http://goldbug.sourceforge.net/) and [retroshare](https://retroshare.cc/). Until 2021, zeronet *was* updated, but the [author](https://github.com/shortcutme) seems to have [disappeared](https://github.com/HelloZeroNet/ZeroNet/issues/2808#issuecomment-1132820787).

## Centralized non-email systems

"Centralized non-email systems" is another term, which as far as I can tell, is also coined by [this document](https://github.com/OpenTechFund/secure-email) from [OpenTechFunc](https://www.opentech.fund/). They define it as "centralized email-like messaging platform" and further divide it into:

> 1. Closed system: you can only send to other users on the same provider.
> 2. Semi-closed system: you can communicate outside the system, but it is troublesome to do so.
>    - If you send an email to someone else using the same system, it is end-to-end encrypted.
>    - If you send an email outside the system, then the recipient gets a URL they can use to view the message. Often, the message is symmetrically encrypted using a shared secret.
> 3. Semi-closed system, with no encryption: just like above, but emails outside the system have no encryption.

Out of the listed options, Enlocked doesn't work anymore and was [a bad system](https://adammonsen.com/post/853/) to begin with, ShazzleMail is pretty much [dead](https://www.reddit.com/r/privacytoolsIO/comments/nv6gzu/comment/hf338kq/?utm_source=share&utm_medium=web2x&context=3) and Walnut also isn't up.

That leaves us with the two last-standing providers:

### Protonmail

Protonmail, or [proton.me](https://proton.me/) as they are called now, which uses good old [OpenPGP](#openpgp). That means they have end-to-end encryption and zero-access encryption by default for all protonmail accounts. They also [support encrypted conversations with non-protonmail users](https://proton.me/support/how-to-use-pgp).

Funnily enough, Protonmail doesn't really fit in any of the listed categories, since encrypted emails outside the system will work as any [OpenPGP](#openpgp) encrypted one.

Protonmail's services have a free tier, which should be enough for lighter usage, but restricts **heavily** storage, label and folder count, amount of sent messages per day, etc. At the time of writing, their second tier pretty much lifts all restrictions for 5 euro per month (without annual contracts). Check out their pricing [here](https://proton.me/pricing).

### Tutanota

[Tutanota](https://tutanota.com/) uses it's own special system for security. In brief, all of your emails are (zero-access) stored and end-to-end encrypted with (mostly) [AES 128](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard), and while they generate and hold your public and private keys, your password is needed to decrypt your private key (do check out their [security page](https://tutanota.com/security/) for more details).

However, it's crucial to note that they are a type *2. Semi-closed system*. If you send an encrypted email to a non-tutanota recipient, the recipient will get a URL from which they will be able to reply, through Tutanota's web client.

Finally, pricing. In general, they have a lot less features than [Protonmail](#protonmail), but their free tier is a lot more capable than [Protonmail](#protonmail)'s. Their pricing is extremely flexible, being able to buy specific features by themselves, like storage or mail aliases. Check it out [here](https://tutanota.com/pricing/).

## Conclusion

In brief, almost all email encryption systems use [public-key cryptography](#encryption-and-signature). The most popular of these systems for personal use is [OpenPGP](#openpgp), but in the business space, [S/MIME](#smime) is quite a bit more prominent. [reop](#reop) is a viable, but limited, very new and lightweight systems for encrypting your email messages.

[Protonmail](#protonmail) is an email provider, which has [OpenPGP](#openpgp) built-in, enabling and supporting encryption by default, but their free tier leaves what to be desired. [Tutanota](#tutanota) is another provider, with their own entirely different system, where encrypted emails to non-tutanota users force the recipients to use their web client.
