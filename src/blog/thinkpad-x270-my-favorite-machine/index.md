# ThinkPad X270 - my favorite machine

## Contents

1. [Context](#context)
2. [What I Think](#what-i-think)
   1. [Display](#display)
   2. [CPU and RAM](#cpu-and-ram)
   3. [Hardware support and chassis](#hardware-support-and-chassis)
   4. [Ports](#ports)
   5. [Batteries](#batteries)
   6. [Keyboard and power button](#keyboard-and-power-button)
   7. [Trackpoint and mouse pad](#trackpoint-and-mouse-pad)
   8. [Repairability](#repairability)
   9. [Cooling](#cooling)
   10. [Pricing](#pricing)
3. [Conclusion](#conclusion)

ThinkPad laptops are generally well liked, a lot of people I know use them and every one that I've touched has been a pleasure to work with.
Suffice to say, I've wanted to get one for quite a while now, so back in the middle of May I bit the bullet and spontaneously bought a second hand [Thinkpad X270](https://www.thinkwiki.org/wiki/X270_Detailed_Hardware_Information).

I've used it for the past 3 months, and in all honesty, it's not without it's drawback, but the X270 has easily become my favorite computer.

*Note: this whole review was written on the aforementioned Thinkpad X270, without external devices (like keyboard or monitor) attached.*

## Context

My main computer is a pre built (without only major upgrades) desktop tower with an [i3-4160](https://www.intel.com/content/www/us/en/products/sku/77488/intel-core-i34160-processor-3m-cache-3-60-ghz/specifications.html), scoring [3510 on CPU mark](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i3-4160+%40+3.60GHz&id=2317), paired with 16 gigs (for the past 9 months, otherwise I had 8 GB) of 1600 MHz memory, and a 1080p IPS monitor.
I also own a laptop, the [Lenovo Yoga S730](https://www.lenovo.com/gb/en/p/laptops/yoga/yoga-slim-series/yoga-s730/88ygs701077) with an [i7-8565U](https://ark.intel.com/content/www/us/en/ark/products/149091/intel-core-i78565u-processor-8m-cache-up-to-4-60-ghz.html), scoring [6152](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-8565U+%40+1.80GHz&id=3308), 16 GB of 2133 MHz RAM and 1080p IPS monitor.
Understanding my relationship with those machines is relevant to how I view the X270.

I got the desktop tower in 2015, and even at the time, it's specs were in the middle range.
Long-term I think that was a net-positive, since it pushed me to optimize my Linux configuration and improve my Linux skills.
Anyway, in all honesty, the only time when I wish for more power is in video games, but I've found myself liking programming much more, so I've not gamed in quite a while.
Summarising, 99% of the time it's hardware is more than sufficient.

The Yoga S730 was an "investment" into my future, since at the time I was going to programming lessons (which gave me a lot of the skills I have today) with my dad's laptop.
With this new machine I could go to the lectures and write code freely on a **very** fast (considering I was doing C# programming in Visual Studio on Windows 10, speed was always needed) and physically light machine, without being slowed down by the cheap, 4 years old, 16 inch behemoth.
From this point on, I was hooked on the 13 inch size, which as we all know is [the perfect size](https://youtu.be/Za_Ul08dtj8?t=1208) and [they should even make laptops that are any bigger](https://youtu.be/Q0h8wWlBxY0?t=1672).

However, very quickly I found the ultrabook fashion of only including USB-C ports, very annoying to deal with, especially if you don't pay for an all-in-one dongle (which I didn't do for years).
Additionally, the screen had *no* anti-gloss coating, so most of the time I was looking at my own reflection, rather than the actual screen contents.
Final thought, in the beginning battery life wasn't that much of a problem, but nowadays the battery barely holds for 3-4 hours, and I think a cell inside is failing.

## What I Think

My dream laptop was a "fat" Yoga S730 - 13 inches diagonally (an absolute **MUST**) with USB type A ports, a non-glossy display and better battery life.
Let's now consider the Thinkpad in question:

- 13 inches in size diagonally,
- *two* USB type A ports and more
- display with an anti-gloss coating
- both an external and internal battery (we'll talk about them later), allegedly being able to last 6-8 hours

Absolutely perfect... well, almost.

### Display

By default, the laptop comes with a **horrible** 720p TFT display, with **horrid** color contrast, brightness and viewing angles.
Surprisingly enough, online there were 1080p IPS drop-in replacements, and they weren't too expensive: the [one I got from amazon.de cost me 115 euro](https://www.amazon.de/dp/B07RZQ5LPW/ref=pe_27091401_487024491_TE_item) (including the 25 euro international shipping).
The laptop itself did cost me ~170 euro, so the screen itself was 2/3 of the laptop price, but at 285 euro total, the deal was still a steal, at least considering where I live at.

[Replacing the display itself was dead easy](https://www.youtube.com/watch?v=hzI3U3up5xY): pop-open the bezel, disconnect the old, reconnect the new, put the bezel back on, boom!
The only negative is that, since it's 1080p, my battery life decreased by 30-60 minutes.

Oddly enough though, if I set the refresh rate at 58.86 (or something else that's lower than 60.00, but this is the lowest I was able to go) the display seemed to use less power and I got that battery life back.
Here are the exact Linux commands I used (make sure you don't have any external displays connected):

```bash
# find your display name, mine was eDP-1; I got this from StackExchange somewhere I think
displayname="$(xrandr | grep connected | grep -v disconnected | cut -f 1 -d ' ')"
# get the exact needed display parameters for 1920x1080 resolution at 58.90 hertz refresh rate
displayparams=$(cvt 1920 1080 58.90 | tail -n 1 | cut -f 2- -d ' ')
modename=$(echo $displayparams | cut -f 1 -d ' ')

# create and "select" the new display mode for our screen
xrandr --newmode $displayparams
xrandr --addmode $displayname $modename
xrandr --output $displayname --mode $modename
```

From now on, my opinion will refer to the X270 with the upgraded display.

### CPU and RAM

The processor is underwhelming to say the least, my machine came with the [Intel Core i5-6300U](https://www.intel.com/content/www/us/en/products/sku/88190/intel-core-i56300u-processor-3m-cache-up-to-3-00-ghz/specifications.html), which has two cores at a base frequency of 2.50 GHz.
It's [CPU Mark is about 3240](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-6300U+%40+2.40GHz&id=2609), but if you were following, it scores a little bit worse than the [CPU on my desktop](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i3-4160+%40+3.60GHz&id=2317), so I'm used to it.
But I can understand that being a deal breaker for a lot of people.

The RAM wasn't too bad actually, I got 16 GB of DDR4 memory at 2133 MHz.
My desktop runs 16 GB of DDR3 memory at 1600 MHz, so in practice, even though the CPU is slightly slower, the overall machine always performs noticeably better than my desktop.

I must mention, on some devices there seems to be an issue, where the RAM module would very slightly move in it's slot when the laptop is held in a certain way, which will cause the computer to freeze.
[The solution is to put something beneath it, as a standoff](https://old.reddit.com/r/thinkpad/comments/jq53yt/freezing_x270x260x250_official_fix_a_bit_lmao/), but I haven't had to do this yet.

### Hardware support and chassis

ThinkPad laptops generally have [Linux drivers for just about anything](https://www.thinkwiki.org/wiki/Drivers), you can freely control the different lights on the laptop or the precise fan speed.
Since this was my first machine in the line, I found this out after getting it, which is a huge advantage to be sure.

Also, ThinkPad laptops are usually made with a thick high quality plastic and the X270 is no exception.
It feels really comfortable on the wrists, compared to the S730's metal casing, and really strong, it won't break easily for sure.
Another slight advantage to the plastic is how it doesn't always feel cold when it isn't doing much or really hot when the laptop is under high usage.

### Ports

As already mentioned, I have two USB type A ports, just like I wanted, but additionally there is an Ethernet, HDMI, USB-C and SD card ports.
And the Ethernet port isn't one of those "folding" ports, it's a plain old 8P8C (RJ-45) plug.
Depending on the model, you may also get a SIM card antenna, smart card reader and a fingerprint sensor, but mine didn't have those.

The only thing I don't entirely like is that the USB-C port isn't the main power connector (though it can be used for charging), it's so convenient when your phone and laptop can share the power supply.
But this is in no way a deal-breaker, and the connector is much beefier than normal USB-C, so I would say the trade-off is robustness.

### Batteries

Something absolutely amazing is that the X270 comes with two, count them, **TWO** batteries - one internal and one (removable) external.
The last time I saw a removable battery was on the old family laptop from 2011, and this one has an additional internal one.
Do I even need to explain why this is awesome?

However, the great news don't end there, both the internal and normal external batteries hold 20 Wh of energy, but you can buy, today, brand new external batteries with capacity of 52 Wh!
As you might imagine, I bought one (though not through amazon.de, since they don't ship batteries), which cost me 51 euro without shipping (we're now at 340 euro total cost).

With my setup and normal usage, 20 Wh usually last me around 3-4 hours, so with the increased external battery and the internal one we are looking at >12 hour battery life, absolutely incredible!
If you also carry around and properly charge the smaller external battery, you could easily last >16 hours without having to charge anything!
The trade-off is that the 52 Wh battery is much thicker, so it's not flush with the laptop, but I've found it improves the angle of the keyboard and airflow.

### Keyboard and power button

Speaking of the devil, unfortunately, the X270 doesn't have the fantastic old keyboards on older ThinkPad models (though, if you are determined, [you could probably put an old one in the laptop](https://www.youtube.com/watch?v=Fzmm87oVQ6c)).
However, comparing to other laptop keyboards, I would say this one isn't too bad, it's not anything amazing, but it better than a good amount of others.
Compared to the S730, it's has a much longer key travel and the keys generally feel a lot clicker.

On the aforementioned Yoga, the Home, End, Page Up and Page Down are accessible only with a Fn combination, and next to the F row you have Print Screen, Insert and then Delete.
Using the S730's keyboard is kinda weird.
The X270 is much more normal, though the square brackets and pipe keys are half-width.
The Fn key is to the left of the Ctrl key, but in the BIOS you can swap them around, so that's not a problem.
My only issue is with the spacebar, because the actual switch isn't in the center of the key, but to the left of it, and I mostly press the spacebar with my right hand, so when I don't absolutely slam it, the key press could just barely not register.

The only reason I'm talking about the power button at all is that, baked in the firmware, to register a press you have to [hold it down for 1-3 seconds](https://bbs.archlinux.org/viewtopic.php?pid=1193137#p1193137), at least when shutting down.
This isn't a bug, it's a feature, and it has come quite in handy, I've had friends casually press the power button on my S730 and shut it off without me being able to react.

### Trackpoint and mouse pad

Actually, the keyboard is amazing in one aspect: it has a track point!
Now that I haven't seen since an old Dell Latitude from the 90s.
It's actually pretty useful, since you don't have to move your wrists to move the cursor around, and with the mouse buttons right below the keyboard, it's pretty comfortable and fast to move the cursor with your index finger on the trackpoint and with the thumb on the buttons.

Oh, yeah, there are physical moues buttons for left, right and middle click!
Gestures are nice, but having a physical button is just so wonderful, do I even need to explain it?

On the other hand, the trackpad itself is nothing to write home about.
It's on the small side and the surface is a little bit more grip-y than on my Yoga S730, so after a lot of use the tips on my fingers feel "tired".

### Repairability

It's very easy to disassemble and reassemble the laptop, all screws are standard Phillips screws, the ones on the bottom cover have retainers and nothing is really glued in place.
The RAM isn't soldered on (though there is only one slot), so it's always upgradable and replaceable.

Laptops usually have pretty stiff hinges, and in the long term this wears out on the plastic until it breaks.
I've seen people complain about the cheap plastic and design which causes such issues, and that isn't incorrect, but I firmly believe that the stiffness is the biggest factor.
A laptop hinge should be loose enough so you could open it with a single finger and the screen won't "fall" down when it's close to it's most opened state.
If you need both hands to open the laptop, or the laptop lifts a bit as you open it, I would say it's way too stiff and you might have problems in a couple of years.

Depending on the laptop, changing the stiffness might prove difficult (as is the case for my S730), but fortunately, on the X270, you'll just need a very small wrench (or pliers) and you can rotate the nuts at the ends of the hinges pretty easily, until the stiffness is just right.
Just remove the bottom cover, change the tightness and then test how it feels, do so until you can open it with a pinky and it stays upright when it's close to the most "opened" position.
It may be a little tricky to make sure both hinges have the same stiffness, but just tweak until it feels and looks right.

### Cooling

The X270 has plenty of ventilation grills on the bottom and the left side, so you'll always be nice and cool.
Better yet, another advantage of the 52 Wh battery is that by lifting the laptop up, there is more air on the bottom from which to suck cold air in.

My only issue is that the fan curve prioritizes silence a little too much on lower CPU temperatures.
It's by no means outrageous, the fan kicks in at a little bit above 45-50 degrees Celsius, which is fine, but when the CPU is close to or at those temperatures, the chassis starts getting a little warm.
During the summer my left hand stars sweating a little bit more, which can be quite uncomfortable.

Of course, as already mentioned, we have drivers for everything, so I installed [thinkfan](https://github.com/vmatare/thinkfan).
With it, I configured the fan to spin up on those lower temperatures and left the automatic configuration intact when we start getting warmer:

```
sensors:
  - tpacpi: /proc/acpi/ibm/thermal
    indices: [0]

fans:
  - tpacpi: /proc/acpi/ibm/fan

levels:
  - [0,  0, 35]
  - [1, 35, 38]
  - [3, 38, 53]
  - ["level auto", 53, 255]
```

This may shorten the fan lifespan, since it will be running more, but for me it's worth it.

### Pricing

As I already said, I got my X270 second hand from [subincho.com](https://subincho.com/index.php), with only a few cosmetic issues; it's practically brand new.
ThinkPads are quite often favorites for businesses, so now is the time when those 6 year old machines get thrown out on the second hand market.
I'll work with the final price of 340 euro, which includes the monitor upgrade and 52 Wh external battery, since that's when the machine goes from good to fantastic.

I live in Bulgaria, so depending on where you live, there might be better offers, but even with a doubled price, you won't find anything brand new that is as good.
Let's look at the listings in [JAR computers](https://www.jarcomputers.com/), since I've found their pricing and availability to be quite normal for online shops (and the physical shops almost always offer fewer devices with a much higher price).

So, 340 * 2 = 640 euro, or roughly 1300 Bulgarian Lev.
At the time of writing, [searching for a ~13 inch laptop at around that price range](https://www.jarcomputers.com/Laptopi_cat_2.html?ref=all&fprop[4413][48266]=48266&fprop[4413][48256]=48256&order=-1&&fprice[price-filter-other]=p_from-1100-p_to-1600) yields only two results: a [Microsoft Surface Laptop Go 2](https://www.jarcomputers.com/microsoft-surface-laptop-go-2-8qc-00024-prod-nbmicrosoft8qc00024.html) and [Microsoft Surface Laptop Go](https://www.jarcomputers.com/ms-surface-laptop-go-tnv-00009-prod-nbmicrosofttnv00009.html), both of which are on sale (with the second's original price being ~2300 lev, around ~1100 euro, three times the price).
Both have soldered on RAM with no slots, so it cannot be upgraded, both have 1536x1024 displays with no anti-glare, that are glued to the screen bezel, both miss the Ethernet and HDMI ports, and since they aren't (older) ThinkPads, you won't get the TrackPoint or mouse buttons or Linux driver support or removable battery.
Of course, they have their own positives, the display is a touchscreen and they score [2.3](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-1035G1+%40+1.00GHz&id=3558) and [3.0](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-1135G7+%40+2.40GHz&id=3830) times higher on CPU mark respectively, but still, you get so much more with the ThinkPad for half the price.

Now, in all fairness, this is comparing a second hand laptop to a brand new one, for double the price you could get a lot more in a second hand machine.
Looking at the same place from which I bought my X270, they currently have [a ThinkPad L13](https://subincho.com/laptopi?product_id=15309) for ~510 euro, which only misses out on the Ethernet port and external battery, but instead provides [double the CPU mark](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i7-10510U+%40+1.80GHz&id=3549).
And that's only one site, if you look around enough you could probably find even better deals, but the 340 euro is still a really good deal.

## Conclusion

The Lenovo ThinkPad X270, with the upgraded monitor and 52 Wh external battery, is an absolutely fantastic machine.
At 13 inches diagonally, it takes just the right amount of space, it has all the ports you might need and the battery life is larger than your work day.
Let's also not forget - it's a ThinkPad!
This means it's chassis is composed of a very robust and nice feeling plastic, you get a trackpoint and physical mouse buttons, as well as some of the best hardware support on Linux.
Did I mention it's easily customizable and modable?

It's main drawback is the weak CPU, but if you're like me, that won't be a deal-breaker, and for the max price of 340 euro, you couldn't even dream of a more feature-rich and comfortable laptop.
Depending on where you live, even doubling the price won't get you (in a new laptop) everything that this machine offers.

Strongly recommend!
