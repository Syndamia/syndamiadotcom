# Taming the Fun-too

Let's set the scene: it's October of last year and I just began my first year as a university student. For the first time in my life, I was able to meet people who were significantly ~~nerdier~~ cooler than me.
One particular dude I met introduced me to [Funtoo](https://funtoo.org), a boutique fork of [Gentoo](https://gentoo.org).

Now, I'm not a very big fan of distro hopping, it takes too much time and you often find yourself not liking the finished product, resulting in further distro hopping.
However, at that time I was a bit softened by my growing pains with Fedora, the sweat promises of improved efficiency and usability, and the fact that fun is in the name! *What could go wrong* :)

So, about a month later I bit the bullet, and installed it on my main computer. It has now been a tad more than a year later and I can comfortably say: *a lot could go wrong...*

<br>

In the same way that a distro is made to solve a certain problem, every distro introduces a fundamental flaw in it's design. Ubuntu solves the "it's too complicated" problem, but introduces issues with performance. Arch solves "my packages are too old and what I need isn't there" issue, but introduces "my packages are too new and something just broke".

Then there's Funtoo. You might reason, since it's a fork of Gentoo, it's big flaw is the fact that you need to compile every package, with the prize being unparalleled efficiency. So did I, but as it turns out there is another, much larger flaw: **packages**.
The distribution is a lot smaller than it's big brother so a lot of stuff, understandably, cannot be tested. However, the real kicker is that package ebuilds[^ebuilds] are generated in a different way from Gentoo.
This often means that using a Gentoo ebuild will make the problem even worse, but sometimes it does work out.

<br>

The first such problem child was QtWebEngine. As the name suggests, it gives the ability for graphical applications, made with the Qt framework, to render webpages.
I don't *need* it for my setup, but there are a couple of useful programs that required it.

What would happen is, after about 3 hours of compiling, the compilation would just stop, without any errors, anywhere. To add insult to injury, because I hadn't set up [ccache](https://ccache.dev/) at the time, everything would have to be compiled *all over again*.
A fairly deep dive into different forums led me to the wondrous conclusion that I needed to remove a certain build flag, which wasn't enabled in the first place...

After *much* tweaking and trying, I found out there just wasn't enough memory, the package needed at least 32 GiB (including swap) of memory for a successful compilation... D'oh!
And here comes the really bad part: I had setup a swapfile of adequate size! However, as it turned out, in the configuration I tried loading it before loading the drive it was stored in... **Double D'OH!!**

<br>

Then came the big boss: python. Now, I wasn't planning on doing any software development with it, but as it turned out, other people had, so certain packages had building issues.
And what was the exact issue you might ask?
Well, to put it simply, Funtoo had official ebuilds for python 3.7, but a lot of packages needed at least version 3.8. This time however, I had a *brilliant* idea: I would copy Gentoo's python 3.8 ebuilds!

What I didn't realize, was that I had unknowingly opened Pandora's box.
Over time I needed to install more and more python packages from Gentoo, until one day I created an *unsolvable* circular dependency[^circular-dependency].
Usually there are solutions to such situations, but I kid you not, I had tried **everything** and **nothing worked**.

This time, there isn't even a solution, I just made sure to never upgrade these packages again.
Which proved problematic, since these packages were important and over time other stuff broke, but that wouldn't be relevant for long.

<br>

Not too soon after, armageddon struck: glibc, GNU's C library. All I wanted to do was try out Ungoogled Chromium, but not as a flatpak.
The only thing standing in the way was the requirement of "glibc 3.4.29", while all I had was 2.70.0.
Nevertheless, I dove straight in, not learning from my past mistakes, I installed the latest and greatest from the Gentoo repositories.

That didn't work and there was another compiling issue (that I can't remember) which just tipped me over the edge. I decided to throw in the towel and downgrade to the normal Funtoo version, until

```
Sanity check to keep you from breaking your system:
Downgrading glibc is not supported and a sure way to destruction.
Aborting to save your system.
```
"Umm what?" was my reaction too. For reasons not completely clear to me, this turns out to be yet another roadblock. Did I mention a couple applications also stopped working after the upgrade?
Unfortunately my believes and ego were firmly set in stone, "Yeah, but I know what I'm doing, I'm downgrading from a non-Funtoo glibc, it will be fine!"
A sanity check override and a package downgrade later, *it wasn't...*

Yes, my system was broken. Very broken. So broken in fact, portage wasn't working, so un-downgrading was not an option. And, of course, booting successfully my machine up ever again wasn't an option either...
Thank goodness for [Timeshift](https://github.com/linuxmint/timeshift)'s system restore functionality or I would've had to rebuild my whole system.

<br>

There are **a lot** of other situations in which Funtoo's package difficulties and my sheer incompetence have led to wasted time and broken stuff, but these are the main ones that stand out in my mind (or more accurately, the ones that I still remember). Nowadays I use the distro in a much smarter fashion, with a much tighter grip.

Although I still need Gentoo packages, I install them from Funtoo's portage Gentoo kit.  
Although I still have issues with packages not being able to be built, I just mask[^mask] them until I have adequate time/energy (or until someone fixes it).  
Although everything that has happened, for now, I'm still sticking with Funtoo.


[^ebuilds]: Files with instructions and settings for the package manager (portage) on how to build a specific package

[^mask]: All ebuilds that are "masked", will not be used (therefore, corresponding package will not be compiled and installed/upgraded)

[^circular-dependency]: To build package A you'll need to have package B installed. But to build package B, you'll need package A installed.
