# Friendship ended with Funtoo, now Gentoo is my best friend

## Contents

1. [Terminology](#terminology)
2. [Public Enemy No 1: Packages](#public-enemy-no-1-packages)
3. [I didn't follow the wolf pack philosophy](#i-didnt-follow-the-wolf-pack-philosophy)
4. [Why did I choose Funtoo in the first place?](#why-did-i-choose-funtoo-in-the-first-place)
   1. [Debunking benefits](#debunking-benefits)
   2. [Personal advantages over Gentoo](#personal-advantages-over-gentoo)

Around 6 months ago I wrote [Taming The Fun-too](/blog/taming-the-fun-too), a blog post sharing my personal experiences with the Funtoo linux distribution.
What you don't know is that less than a month later, at the end of December, I installed Gentoo on my desktop computer, and an additional 3 months later my laptop followed.
Here is what happened.

## Terminology

Before we begin, I want to take a second and explain some Gentoo/Funtoo terminology:

- `emerge` (as a verb): what does "install" mean? I would argue it means copying a program and the file it depends on somewhere on a system, with the goal of being able to run this program.  
  Now, in Gentoo and Funtoo, most programs are compiled from source code, so before you install you download it's source and compile.
  For this reason, on such systems you would usually say a certain program/package is *being emerged, not installed* (and the words comes from the [package-manager command-line utility of the same name](https://wiki.gentoo.org/wiki/Emerge)).
- `ebuild` (file): the exact steps needed to emerge a package differ from one project to another, but how do you manage packages then?
  The solution is that when you try to emerge something, a special file with detailed instructions is looked up: an ebuild file.
- repository tree: the collection of all ebuilds
- `use` flags: every program can be built with different flags (aka options), which enable or disable certain features from being available in the result package.
  For example, the text editor [vim](https://packages.gentoo.org/packages/app-editors/vim) supports sound, but not everyone would want to have that feature.

## Public Enemy No 1: Packages

Most packages and their dependencies on Funtoo go unmaintained and untested for years, which significantly limits your abilities to install and build software.

Python was stuck at version 3.7, while a lot of packages like barrier, libplacebo, mpv and pulseeffects[^packages-in-repo-tree] required[^required-3.8] at least 3.8.
So, with a lot of effort and poking around, I got python 3.8 to work, just barely.
What's even better is that eventually I reached an **unfixable** circular python dependency (between dev-python/packaging and dev-python/setuptools).

Another problem child was glibc, it is *currently* at [version 2.33-r2](https://github.com/funtoo/core-kit/tree/1.4-release/sys-libs/glibc), which under Gentoo [is officially masked and discouraged for usage](https://gitweb.gentoo.org/repo/gentoo.git/tree/profiles/package.mask#n542)[^glibc] and wouldn't you know it, also prevents some packages from being compiled (mainly I remember ungoogled-chromium).

There were a lot more problematic packages, but I've forgotten most details.
What I do remember is that the day I decided to hop to Gentoo, I tried to update my system and yet again I got some sort of issue, either a package couldn't compile properly or there was some dependency conflict.
That was preceded by my unsuccessful attempts at getting the kicad package to work a couple of days prior.

At that point, this had become completely normal.
**But** Funtoo shouldn't be blamed for it!

## I didn't follow the wolf pack philosophy

On the [Welcome](https://www.funtoo.org/Welcome) page of Funtoo's website, in the second paragraph, we are linked the [Wolf Pack Philosophy](https://www.funtoo.org/Wolf_Pack_Philosophy) page.
This is essentially a document describing how a member of the community should behave, and I didn't follow it.
Any member of the pack is supposed to:

- **Be Mindful of the Pack** by tackling challenges with the community  
  but as I found (package) challenges, I tackled them on my own, not trying to consult anyone or ask for help
- **Be Interconnected** by supporting and being supported by the community  
  but I didn't connect with anyone on my issues
- **Howl** by expressing their needs and sharing their "adventures"  
  but I didn't express them and didn't share the work I've done

Or more simply put, Funtoo is a small project, and you need to communicate with others to get things done.

Another way to look at it is that in a project of this scale, what gets supported is what the community uses, so in a way, I was trying to use Funtoo in the *wrong* way.
This may sound ridiculous, but then again, they aren't making a project which "just works" for everyone (like Ubuntu or Linux Mint), this is a project by the community for the community.
You can't expect them to support everything that a big-scale distro does, like Gentoo, with a fraction of the manpower, so if nobody supports a package (which exists in the repository tree) that doesn't work and you don't at least speak up, it will probably stay broken[^kicad] (this kind of relates to the **Is Territorial** point in the philosophy).

However, in my defense, I didn't open bug reports or create forum posts because my issues weren't "directly" related to Funtoo, or a normal Funtoo installation.
Most of the time I would have an issue with a package, then get it's Gentoo counterpart, try to get all dependencies working one by one, and it will fail with some unsearchable error.
At that point, when I was truly ready to throw in the towel, I had done so much, and tweaked so many things, I couldn't even explain what I had done to my system, let alone ask for help on my setup.
Additionally, the information on my initial issue would be lost, and undoing my changes would require too much work.
After enough time, so many stuff like that accumulated, I'm pretty sure I started getting problems for packages which normally weren't problematic.

## Why did I choose Funtoo in the first place?

The primary reason I switched was because of a fellow student in university convinced me of all the cool features, general stability and resilience of Funtoo.
Looking back at it, the last two parts could be applied to a lot of distros, but I was using Fedora at the time, wanted to switch, and without any clear preference, I went with what he offered.

### Debunking benefits

But we did also talk about Gentoo, and he pointed out some reasons why Funtoo is better.
Looking at these with the benefit of hindsight:

> git > rsync

By default, on Gentoo synchronization of ebuild files is done via rsync, while on Funtoo it's done with git.
Gentoo has actually supported git synchronization for quite a while, and [the setup is easy](https://wiki.gentoo.org/wiki/Portage_with_Git), so in the modern day it's really a matter of preference.
Though, [rsync is probably better with bad internet connections](https://lwn.net/Articles/759578/).

> profiles (flavors and mix-ins)

Funtoo has a system called [profiles](https://www.funtoo.org/Funtoo_Profiles).
The idea is that you can group a whole set of packages and use flags into one name with the ability to turn them off and on again.
So, if you want to install gnome on your system, you don't have to worry about flags and potential dependency problems, just use the gnome mix-in and everything will be ready to go for emerging.

This is pretty cool but it's also somewhat unnecessary.
You would save time on your very first install ever, but once you've set your flags on one machine, you'll just need to copy some files over (make.conf and package.use) and be done with it.
There is a case to be made for people who often emerge and unmerge packages, but they are very much a minority (which I'm not a part of).

> Funtoo has [stage 3 tarballs](https://wiki.gentoo.org/wiki/Stage_tarball) with already setup DEs

This means that on a Funtoo system, you could directly unpack the tarball (the initial prebuilt binaries), and without emerging anything, have a ready-to-go desktop environment.
The huge benefit is that you can very quickly reboot into your "install" (after setting up the booting of course), and use your computer with the comfort of a graphical environment while everything else is being emerged.

Again, this is really cool, but at the end of the day, a normal "text-only" installation isn't very difficult, the [Gentoo handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation) is very detailed and well written, you install a system once so in the long term it wouldn't matter and I personally use a custom desktop environment setup, so those DEs would be bloat.

> fixed release model

[Funtoo releases fixed version of their distro](https://forums.funtoo.org/topic/1945-funtoo-release-model/#comment-9321), which means, in theory, packages will be way more stable and tested than normally.
I've never found problems with packages being "too new", and this is more relevant to how the packages are managed by the maintainers, so it doesn't make that much of a difference to the end user, in my opinion.

> [autogens](https://www.funtoo.org/Autogen)

Rather than making ebuilds manually, on Funtoo you can automatically generate them with simple Python code.
This system probably reduces management time quite a lot, especially for a smaller team of maintainers, but you would still need to understand ebuild files.
And I'm not a maintainer, so I don't really care.

### Personal advantages over Gentoo

Though I've "disproved" all of them, that's not to say there aren't any advantages:

> ccache

Imagine you are compiling a program with source code of over 20000 files and in the latest update, only 1000 of them were changed.
It will be significantly faster, if you recompile only those 1000 files and use your work from before for the others.
That is the idea of ccache, and on Funtoo it works great, I had it enabled for all packages.
Unfortunately, for Gentoo it's the opposite effect, I couldn't compile most packages with it enabled.

> package.use > make.conf

There is a big emphasis on writing your use flags in a separate `/etc/portage/package.use` file (or make it a folder and add use flags in different subfiles), and to try and change `/etc/portage/make.conf` as little as possible.
I generally believe this is a significantly better system than stuffing everything into `make.conf`, and it isn't pushed that hard on Gentoo.

That said, Gentoo still supports it, so I'm not missing out, but I probably wouldn't have used it.

> better ebuilds

Though I can't empirically prove this, I found that the packages that worked on Funtoo compiled faster than their Gentoo counterparts and generally the ebuilds were simpler and more manageable.

> Slightly better documentation

I'm not saying Gentoo's documentation is bad, but Funtoo's is often written a bit more simply.

<br>

Funtoo has it's audience, if you're ready to be a part of the wolf pack, it has to potential to be even better than Gentoo.
That said, if you need something which always just works, your choice should be Gentoo, as it is for me.

[^packages-in-repo-tree]: Not all of those packages are in the repository tree, but that doesn't matter, if you want to compile a program, you should be able to do so.
[^required-3.8]: Required might be a strong word, I don't remember if I tried (though I'm sure I did) just downgrading the Python version in the ebuild. Nevertheless, there were for sure useful packages which really did need it.
[^glibc]: This point may not be completely fair, since Funtoo has a system for [ebuild generation](https://web.archive.org/web/20230528115518/https://projects.funtoo.org/metatools/docs/autogen.html), which means that they might've made configuration changes and/or applied patches which "fix" that version, but still, it really isn't a good look.
[^kicad]: On that aforementioned kicad package, drobbins, the benevolent dictator for life of Funtoo, categorized it as a [package which people aren't interested in](https://bugs.funtoo.org/browse/FL-8435?focusedId=52907&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-52907).
