# Microsoft and FOSS: a fire to be extinguished

## Contents

1. [GitHub's embrace and extensions](#githubs-embrace-and-extensions)
2. [Blowtorch to the penguin](#blowtorch-to-the-penguin)
3. [But these are good things for the user](#but-these-are-good-things-for-the-user)
   1. [The practical answer](#the-practical-answer)
   2. [The moral answer](#the-moral-answer)

A week ago [Microsoft announced they'll be adding "sudo" to Windows](https://devblogs.microsoft.com/commandline/introducing-sudo-for-windows/).
I'm not really knowledgeable enough to be certain what I'm saying is true, so I could be wrong, but upon hearing these news, my mind immediately jumped to the [EEE: Embrace, Extend, Extinguish](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish).

As it is explained on Wikipedia, this is a very old and successful tactic, where Microsoft would enter new markets by:

1. Embracing public standards and implementing all features the current product landscape offers, then
2. Extending their offering with proprietary features, which will be pushed to customers and finally
3. Extinguishing their competition, since the vast majority of consumers would be depending on the proprietary features of Microsoft's product

## GitHub's embrace and extensions

Thinking about the development of GitHub under Microsoft's rule, this pattern seems to show up.
Open-source by itself is primarily about publishing the source code of your project, but GitHub isn't just about that.
Now we have:

- [GitHub Octoverse](https://octoverse.github.com/), yearly reports on open-source globally, but quite often through the lens of GitHub
- [GitHub Insider](https://resources.github.com/newsletter/), a monthly newsletter
- [GitHub Copilot](https://github.com/features/copilot/), an AI developer tool
- [GitHub Enterprise](https://github.com/enterprise), an AI developer platform
- [GitHub blog](https://github.blog/), mostly a set of tutorials and informational articles regarding GitHub

and the list just goes on.

GitHub isn't just a public place to view open-source software anymore, if you are a novice developer or you just don't care that much, it will be very easy to associate the FOSS community with the GitHub community.
Once you've started using all of those services and tools, there is no coming back, because other repository platforms focus only on repositories.

I mean, [GitLab](https://gitlab.com/) doesn't offer a developer forum like [GitHub Discussions](https://docs.github.com/en/discussions/collaborating-with-your-community-using-discussions/about-discussions), you won't find a [sourcehut](https://sourcehut.org/)-branded FOSS newsletter, [SourceForge](https://sourceforge.net/) won't allow you to add [animations, graphics, games, etc. on your profile](https://github.com/abhisheknaiidu/awesome-github-profile-readme), there isn't a [codeberg](https://codeberg.org/) [command-line repository management app](https://cli.github.com/), so *obviously GitHub is where it's at for FOSS*.

And you'll be introduced to GitHub before any of the others, thanks to the [GitHub Student Developer Pack](https://education.github.com/pack), where students can get free access to a variety of services, or to [GitHub classroom](https://docs.github.com/en/education/manage-coursework-with-github-classroom/get-started-with-github-classroom/about-github-classroom), where newer developers will probably publish their first pieces of code.

Why learn to use the git terminal command when [GitHub Desktop](https://desktop.github.com/) abstract everything away in a beautiful clicky interface (as a "bonus", this will make you associate git with GitHub too), why follow [Hacker News](https://news.ycombinator.com/) or multiple different FOSS websites, when Microsoft provides.

Why learn anything, when Microsoft's GitHub serves it to you on a silver platter?
Well, for an increasing number of developers, I fear, the answer is: "there is no point".

## Blowtorch to the penguin

Now let's talk about Microsoft's other plaything: Linux.
Here, I feel, the tactic is even more obvious: thanks to [WSL](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux), Linux applications look and run like any other Windows app.
Thanks to [Windows Terminal](https://en.wikipedia.org/wiki/Windows_Terminal), Windows can't be seen as a lesser OS for developers, all benefits of having a proper, fully-fledged, UNIX-like command-line can now be found on Windows.

I mean, they've added the sudo command for crying out loud, there is even an official [Windows package manager](https://en.wikipedia.org/wiki/Windows_Package_Manager)!

For a novice or someone uninterested, you wouldn't ever have to learn how to use Linux.
You'll use Windows as you've known for your entire life, and if you need something even vaguely related to Linux, you'll just easily get it on another silver platter.
I'm certain over the coming decade, the true Linux desktop market share, as in machines running natively Linux, will shrink, but since WSL users will contribute to that number, we'll see the opposite.


## But these are good things for the user

It could be argued that Microsoft is creating something good for the user, that GitHub's extensions will pull more people into FOSS, it will make developers stronger and more connected than ever.
You could argue that integrating Linux into Windows just gives you the best of both worlds, that for a user you don't have to chose anymore, but reap the benefits of both OSs.
Why should any of this be seen as a problem?

### The practical answer

The whole point of FOSS is to oppose business, to oppose the idea that everything is a product which must be sold, which must be unchangeable and fully under the control of the company who made it.
A product which can be changed and regulated to feed the bottomless greed of businessmen.
If everyone thinks Microsoft equals FOSS, then they can start changing that definition and people will follow.

I can easily imagine a future where, all of a sudden, you can view GitHub repositories only if you have a GitHub account, when it will be on a subscription, when to access Microsoft's open-source projects you need to go through some enlisting process (similar to that for Unreal Engine), when Microsoft adds some sort of authentication mechanism, where you cannot run modified versions of certain FOSS projects (so even if you have the source code, and can modify and compile it, you cannot run the executable, forcing you to use the official version only).

If everyone thinks Linux is found in Windows, then developers will focus on integrating it better with Windows, until most new Linux software is somehow available only through WSL, but doesn't work on distributions.
It's tempting to imagine Linux software or Linux projects turn into Windows software and Windows projects, when less and less people try to make something new, something good, something for everyone, and more and more create proprietary garbage.

### The moral answer

[We do things, not because they are easy, but because they are hard](https://en.wikipedia.org/wiki/We_choose_to_go_to_the_Moon).
Knowledge is the most important resource we have, everything humanity has achieved has been with the creation, expansion and distribution of knowledge.
Developers are the craftsmen of computer software and to create you need knowledge, the more you have the more you can do.

Tools we make must be used to make our job easier, but we must be able, or at least understand, how to do the job without the tool.
Walking down this path, at a certain point, entire concepts can become forgotten to history, which makes developers worse at their craft, and software worse to use.
