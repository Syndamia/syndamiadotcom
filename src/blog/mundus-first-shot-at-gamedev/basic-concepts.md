# Basic Concepts - Mundus: My first shot at game development

## [*Due for rework*](/rework.html)

[`<<`](./) || [`< User Interface`](./user-interface.html)

<br>

## Contents

1. [Tiles](#tiles)
2. [Layers and superlayers](#layers-and-superlayers)
3. [Inventory](#inventory)
4. [Crafting](#crafting)
5. [Event logs](#event-logs)

Even though the game is quite unfinished, there are still a fair bit of mechanics I've developed.

## Tiles

The in game world (map) is composed of squares, called tiles. Everything is a type of tile. There are two main categories of tiles: items and mobs. Mobs (mobile object for short) are the creatures in the game, while items are everything else, from the tools, to the trees.

There is only one type of mob tile and that is `MobTiles`. All mobs, with the exception of the player, have the same functionality and properties:

- health - if it drops to or below 0, they die

- defence - reduces or completely negates certain amounts of damage

- inventory

- random movement qualifier - used for randomized movement around the world (does not apply to the player)

- item that is dropped upon death

- the superlayer they are currently in

Items, however, are divided into:

- Material tiles: primary used for crafting. Some of them are "consumable", meaning they can be consumed by the mob, which removes them from the inventory and changes a property of the mob. In your inventory they are surrounded in orange.

- Gear tiles: something you equip (in your accessories and gear inventories) that modifies properties of the mob. In your inventory they are surrounded in purple. None are implemented.

- Ground tiles: the tiles on the superlayer ground, like the grass tile. Mobs and structure tiles reside on top of them. They also separate the superlayers.

- Structure tiles: tiles that can be placed in the world, like a tree. Some of them can be picked up, like a ladder, but most drop a different item each time they take damage. In your inventory they are surrounded in red.

- Tool tiles: used for interacting (changing) the environment of the world. In your inventory they are surrounded in blue. There are four types, depending on their capabilities:

  - Shovels: you dig up (remove) ground tiles from ground layer

  - Axes: used to break (remove) wooden structures (trees, ladders, ...)

  - Pickaxes: used for breaking (removing) structures made from hard materials (rocks, boulders, ...)

  - Swords: used for damaging (killing) mobs

## Layers and superlayers

The in game world (map) is composed of superlayers. Superlayers represent each elevation level in the world. Conceptually, there are in total 5 superlayers: Space, Sky, Land, Underground and ???, although only Sky, Land and Underground are implemented.

Each superlayer is composed of 3 layers: Structure, Mob and Ground. Each one of them contains a specific type of tiles, meaning structure tiles are only stored in "structure" layer, ground tiles only in "ground" layer and mob tiles only in "mob" layer. Even though, they are different layers, there are some rules for the interaction between them:

- **Mobs cannot be on top of structures**, except if the structure is marked as "walkable"

- **Structures cannot be placed on top of mobs**. "Walkable" structures can be placed below a mob.

- **Structures cannot be placed on top of holes** (empty spots in the ground layer), **except if they are "climable"**, in which case they will be placed in the structure layer on the superlayer below. Mobs also cannot reside on top of holes (they fall to the superlayer below)

## Inventory

Each mob can store certain amount of item tiles. That is done in it's inventory. The inventory is comprised of 4 parts:

- Hotbar: a place for having access to a small amount of items at all times. It can contain all item tiles, except groundtiles

- Items: used for bulk storage, it can hold any type of item tile and is the largest part of the inventory

- Accessories and Gear: used only for storing gear tiles

## Crafting

Creating new item tiles is done through crafting. Each craftable item requires one to five different materials in different quantities. The item tiles must be inside the "items" part of your inventory.

## Event logs

Certain actions you do in the game are stored in memory as messages (logs). They are also used to give you information that you couldn't get otherwise, for example, there is a log when you can't climb up a superlayer because a mob is blocking the way, or when you damage/kill a structure or mob.

