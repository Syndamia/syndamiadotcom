# User Interface - Mundus: My first shot at game development

## [*Due for rework*](/rework.html)

[`<<`](./) || [`< Timeline`](./timeline.html) || [`Basic Concepts >`](./basic-concepts.html)

<br>

## Contents

1. [Main screen](#main-screen)
2. [Create new game](#create-new-game)
3. [Game Window](#game-window)
4. [Map menu](#map-menu)
5. [Inventory menu](#inventory-menu)
6. [Crafting menu](#crafting-menu)

The user interface shows how unfinished the whole thing is, you can find many unimplemented options and features. Even then, there is a lot you can do.

A lot of the topics covered are explained in more detail in [Basic Concepts](./basic-concepts.html).

**Note:** all screenshots are made in Linux Mint Cinnamon with Mint-Y theme.

## Main screen

![](./img/main-screen.png)

We start off with the main screen.

Only the `Create a new game` button is implemented, although the `Settings` one does open a blank window.

## Create new game

![](./img/create-new-game.png)

Creating a game gives you a couple of options:

`Game Mode` - Survival is the only implemented option, where you start with almost no items and have to gather food and materials. Creative was supposed to be the opposite, giving you access to all items and making you invincible.

`Difficulty` - Higher the difficulty, the more your energy gets depleted and the less trees, boulders and mobs there are.

`Screen & Inventory size` - The screen is where you navigate the in game world. The way the user interface works, both are linked.

`Map size` - Map size (in survival mode) is determined by the Screen & Inventory size, the higher the size, the smaller the map.

**Note:** all game window screenshots are made with the `Small Game Window`.

## Game Window

![](./img/small-game-window.png)

After the world is generated, the game window opens.

At the top you have your (game) screen. Here you interact with the in-game world. Navigation is done by clicking on a place you want to go to. If you want to alter the environment, first click on an item, that can modify the world and click the thing you want to alter.

Below that is your energy bar. Each mob, including the player, has an energy bar. Each action you do takes energy. If your energy gets depleted, you start taking damage for everything you do. Eating meat from killing mobs is the only way to replenish it.

Then you have your health. It represents how much life you have. If it drops to zero, you die, although no end screen is developed.

Underneath that, there is your hotbar. It is a part of your inventory.

After that, you have the `Event log` button and some of the most recent events. Clicking the button opens a window from which you can checkout everything that has happened. On the sides, you can find the `Map` and `Inv` toggle buttons and the `Pause` and `Music` menu buttons. The `Music` menu can only play and stop WAV files.

## Map menu

![](./img/map-menu.png)

The Map menu is an extension (to the left) of the normal game window. It cannot be opened at the same time as the inventory menu.

At the top, you can see the ground layer tiles. Below it you can find the name of the superlayer you're currently in and your coordinates. At the bottom there are the structure layer tiles and `There is a hole above player` field, where you can check whether you are able to ascent a superlayer.

## Inventory menu

![](./img/inventory-menu.png)

The Inventory menu is an extension (to the right) of the normal game window. From here you can store your items, organize them, craft and see information about a given item.

## Crafting window

![](./img/crafting-window.png)

Every time you open the crafting menu, it automatically finds all items that you can craft using the items in your "Items inventory".

At the top it shows you what items and how much of them you need to craft the currently selected item. Below that it shows the item that you can craft and the `Prev` and `Next` buttons for navigating available items to craft. Finally, there is the `Craft` button. Clicking it removes the required items and adds the crafted item to your inventory.

