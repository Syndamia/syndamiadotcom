# Introduction - Mundus: My first shot at game development

[`Timeline >`](./timeline.html)

<br>

## Contents

[*Due for rework*](/rework.html)

1. [Timeline](./timeline.html)
2. [User Interface](./user-interface.html)
    1. [Main screen](./user-interface.html#main-screen)
    2. [Create new game](./user-interface.html#create-new-game)
    3. [Game Window](./user-interface.html#game-window)
    4. [Map menu](./user-interface.html#map-menu)
    5. [Inventory menu](./user-interface.html#inventory-menu)
    6. [Crafting menu](./user-interface.html#crafting-menu)
3. [Basic Concepts](./basic-concepts.html)
   1. [Tiles](./basic-concepts.html#tiles)
   2. [Layers and superlayers](./basic-concepts.html#layers-and-superlayers)
   3. [Inventory](./basic-concepts.html#inventory)
   4. [Crafting](./basic-concepts.html#crafting)
   5. [Event logs](./basic-concepts.html#event-logs)

I have been toying with the idea of making a game ever since my first exposure to programming. The [earliest piece of code](https://gitlab.com/Syndamia/Shower/-/tree/master/ARK%20(2015)) I ever wrote was about replicating a computer boot up sequence from the video game [SOMA](https://store.steampowered.com/app/282140/SOMA/). I developed it in 2015, solid 3 years before my first proper introduction to software development.

Five years later, in early 2020, I had to make a project for an exam. I (initially) had about a month to complete it and the only requirements were technical: utilize a database with at least x tables, employ a specific design pattern, etc. Fairly quickly I decided upon making a "minecraftian" game: 2D sandbox where you could fight, craft, build, eat and explore.

I called it [Mundus](https://gitlab.com/Syndamia/Mundus) and it took me around 3 months to complete and consisted of more than 2000 lines of code. Although these metrics aren't very impressive in the real world, they were a monumental achievement for me. In the beginning I planned on developing it into a fully fledged game, but that never happened.

Nevertheless, this journey was very important to me, hence writing this multi-page article.

**Link to the source code**: [GitLab](https://gitlab.com/Syndamia/Mundus)

## Screenshots

<div style="display: flex; overflow-x: scroll; align-items: center">
	<img src="./img/small-game-window.png" style="height: 100%; flex: 1; margin-right: 0.5em">
	<img src="./img/map-menu.png" style="height: 100%; flex: 1; margin-right: 0.5em">
	<img src="./img/inventory-menu.png" style="height: 100%; flex: 1;">
</div>

*Screenshots are made on Linux Mint Cinnamon with the Mint-Y theme*
