# Timeline - Mundus: My first shot at game development

[`< Introduction`](./) || [`User Interface >`](./user-interface.html)

<br>

As mentioned, I had big plans for Mundus, so I wanted to make sure it would be well planned. But at the same time, the project took too much energy and I often had holes of no development time. This is an overview of the development "cycle" I went through.

> ***08.02.2020***&emsp;Day 0, Hour 0

At this point of time, I was still studying software development in a national program. On this exact day, we had the first lecture of a new module, where it was announced we would have to make a project for the exam. Up until this point of time, our exams constituted of predefined programming exercises for which we would be given a couple of hours to complete and would be checked by an online platform.

I immediately began brainstorming and by the end of the day, I had decided upon the project I would be developing.

> ***12.02.2020***&emsp;Day 4

Before Mundus I had experimented with making games, but mostly in the command line. This time was going to be different. Up until this day, I was only researching and trying out different technologies to make my game with.

Initially I spent a lot of time with Unity, but the original time constraints and my lack of knowledge with the game engine discouraged me from continuing with it. Subsequently, I got discouraged from using any game engine and settled for `GtkSharp 2.0`. Now, this is widget toolkit, not even close to a game engine, but hey, it was much better than the command line, it was simple and shipped with the IDE I was using at the time.

This decision proved to have been a mistake down the road, but the clock was ticking and I had no more time to waste. Besides, I also believed that in the end, moving it to something like Unity wouldn't pose too much trouble.

Finally, it was time to plan the actual game.

> ***27.02.2020***&emsp;Day 19

So far I have only done preparation but on this day, I started actual programming. The UI and most of the mechanics were completely sketched.

Around a week later I also drew the first proper textures for the game world: a [tile of grass](https://gitlab.com/Syndamia/IT-kariera/-/blob/a100f71372496ba89e26fedc520edcefd2442848/Year%202/Software%20development/Mundus/Mundus/Icons/Land/Ground/grass.png) <img src="./img/grass-tile.png" class="pixelated-render"> and a [tile of grass with a boulder on top](https://gitlab.com/Syndamia/IT-kariera/-/blob/a100f71372496ba89e26fedc520edcefd2442848/Year%202/Software%20development/Mundus/Mundus/Icons/Land/Ground/boulder.png) <img src="./img/boulder-tile.png" class="pixelated-render">. These replaced the [grass](https://gitlab.com/Syndamia/IT-kariera/-/blob/7b2c8cbc1d6adb874ca6a41f2871ea1950866d70/Year%202/Software%20development/Project/Mundus/Mundus/Views/Icons/Land/Ground/grass.jpg) <img src="./img/placeholder-grass-tile.jpg" class="pixelated-render"> and [rock](https://gitlab.com/Syndamia/IT-kariera/-/blob/7b2c8cbc1d6adb874ca6a41f2871ea1950866d70/Year%202/Software%20development/Project/Mundus/Mundus/Views/Icons/Land/Ground/rock.jpg) <img src="./img/placeholder-rock-tile.jpg" class="pixelated-render"> placeholder textures I made on the 29th.

Concerning the temporary textures, I just drew them in half an hour in GIMP, so I could test out some of the code.

The proper textures, I believe, turned out fairly well, given that I've never been good at drawing and this is my very first time trying to draw pixel art. There is no magic behind the process, after scouring the internet for pixel art that looked kinda close to what I imagined, I tried to utilize and mix whatever I found fitting and then just messed around for hours. As you can imagine, this wasn't a very efficient process, and it took me around 1-2 days for both textures.

> ***28.03.2020***&emsp;Day 49

Slowly and steadily I had been making fine progress, but a lot of issues started to surface. As it turns out, I hadn't thought everything completely through, so I mostly went back to the drawing board.

But at the very least, I had some important systems in place, like breaking and placing stuff, proper movement through the map and using items. Also, I had drawn my favorite texture from the whole projects: [the tree](https://gitlab.com/Syndamia/Mundus/-/blob/c47a34847e52ef76361394dba2c4f524a56d1b92/Mundus/Icons/Land/Items/tree.png) <img src="./img/tree-tile.png" class="pixelated-render">.

> ***06.04.2020***&emsp;Day 58

Things started to take a turn for the better, I had planned everything I needed to plan and spring break had began. A solid week of no high-school made me more productive than ever. Some days I coded for 8 or even 10 hours straight, from morning to evening. Tools, multiple layers and climbing between them, new items and many bug improvements.

And then, they extended the break for one more week, the productivity boost could've continued even further, but, alas, I was burned out. The productivity boost, as good as it was, it tired me out very fast. There were still a couple of days with progress, but this time I actually took a break.

<br>

However, remember how they extended the spring break by a week? Well, this was caused by the emergence of the COVID-19 pandemic in Europe and Bulgaria. Subsequently, after the break, we didn't go back to school. Due to the emergent situation, development on Mundus became increasingly sporadic and progress slowed down drastically. At least the exam got pushed back by a month/month and a half.

<br>

> ***23.05.2020***&emsp;Day 105

Finally, after so much time, and relative amount of hiccups, Mundus was finished. Or at least, finished enough so I could present it on the exam. And this happened just in the nick of time too, the exam date was in 8 days, on the 31st.

I had done a lot, it had a randomly-ish generated world, it had animals, it had items, you could chop down trees and get branches and craft tools. But, well, you could do everything in about 3 minutes. The systems were in place, and mostly what it lacked was content: much more animals, much more layers, much more items.

About the exam itself, I got an Excellent 6 grade, the highest possible grade. Even with it's drawbacks, Mundus was jam packed with features and extendability, unlike the other projects.
