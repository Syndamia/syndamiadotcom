# How to disable Sticky Keys prompt

We've all been there: you're playing a game and after pressing `Shift` multiple times, you get a popup about "Sticky Keys", similar to this one:

![](./img/windows-sticky-keys-prompt.png)

This is how to stop the popup from showing up ever again:

1. Press **Windows key + R**, type in `control access.cpl`, and then press **OK**

   ![](./img/windows-run-control-access.png)
2. Inside the opened up window, press **Make the keyboard easier to use**

   ![](./img/windows-make-keyboard-highlighted.png)
3. After that, press **Set up Sticky Keys**

   ![](./img/windows-sticky-keys-highlighted.png)
4. Uncheck the **Turn on Sticky Keys when SHIFT is pressed five times**

   ![](./img/windows-sticky-keys-disable.png)
5. Finally, press **OK** and close the window
