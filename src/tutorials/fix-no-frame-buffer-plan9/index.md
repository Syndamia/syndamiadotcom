# Fix "No frame buffer" error in Plan 9

Let's say you installed [Plan 9 by Bell Labs](https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs) on a QEMU/KVM virtual machine by just following the prompts and entering defaults. In certain cases, you might find your install booting to a shell with the errors:

```
aux/vga: inportl(0xc100): permission denied
rio: can't open display: initdisplay: /dev/draw/new: no frame buffer
init: rc exit status: rio 31: display open
```

![](./img/boot-prompt-no-frame-buffer-error.png)

For some reason, on certain configurations, the `xga` (`vga`) monitor type does not work when booting into Plan 9, even though it works fine during install. The fix is to adjust monitor settings to `vesa`:

1. In the shell prompt enter

   ```
   aux/vga -m vesa -l
   ```

   You should get a cursor and a cleared prompt.

   ![](./img/manual-prompt.png)

2. In this "new" shell prompt, enter

   ```
   rio
   ```

   You should now get a grey background with the cursor in it.

   ![](./img/manual-rio.png)

   - You are now in a graphical environment, so technically you can stop here. The other steps are for configuring it so you wont ever have that problem again.

3. Create a new window (refer to the bullet point for instructions). Enter

   ```
   9fat:
   ```

   and afterwards

   ```
   sam /n/9fat/plan9.ini
   ```

   this is the configuration file we will be changing.

   ![](./img/rio-9fat-commands.png)

   - How to make a window: right click, hold, hover over "New" and release the right click. Now right click somewhere in the top left, hold, drag to somewhere in bottom right, release.

4. Now we will be editing the config file with sam, a line editor. Click the green-backgrounded box, the cursor should appear.
   First select our file:

   ```
   b /n/9fat/plan9.ini
   ```

   and click the green-backgrounded box.
   Then replace `xga` with `vesa`:

   ```
   , x/xga/ s/xga/vesa
   ```

   and click the green-backgrounded box again. We're almost done here, enter `w`, wait a bit, and finally enter `q`.

   - If you want to check whether the file was properly saved, feel free to run

     ```
     cat /n/9fat/plan9.ini
     ```

     and check that monitor is equal to vesa (you can scroll with the arrow keys, or with left and right mouse buttons, acting as up and down).

     ![](./img/rio-plan9ini-cat.png)

   - The first command opens the `plan9.ini` file for editing, and the second command, finds every line that contains `xga` (there should probably be only one) and on each matching line replaces `xga` with `vesa`. Refer to the [docs](https://p9f.org/sys/doc/sam/sam.html) for more information.

5. That's it, all we have to do is halt our file system with `fshalt` (or type out the command in the [Qemu installation guide](https://9p.io/wiki/plan9/Installing_Plan_9_on_Qemu/)) and Force Reset the virtual machine.

The next time you boot into Plan 9 and into your user, the graphical environment should load fine. Enjoy Plan 9!

![](./img/rio-default-login.png)
