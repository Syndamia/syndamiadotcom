# Pages - How to use Jira

[`<<`](./) || [`< Releases`](./releases.html) || [`Components >`](./components.html)

<br>

**Pages are a place for documents for your team**. The goal is to have a centralised location where everyone in your team can share knowledge.

![](./img/test-page.png)

Pages are powered by another Atlasian product, named `Confluence`. It is a **subscription-based software**, but it does also offer a free 30-day trial.

**All documents** (pages) that you write are **organised into `Spaces`**. When creating a page you can choose one of many templates or just start off with a blank one.

**Confluence won't be covered in this tutorial**. You can read more about it [here](https://www.atlassian.com/software/confluence/guides/get-started/set-up).

