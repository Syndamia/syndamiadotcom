# Kanban template - How to use Jira 

[`<<`](./) || [`< Projects`](./projects.html) || [`Scrum template >`](./scrum.html)

<br>

## Contents

1. [Create board](#create-board)
2. [Backlog](#backlog)

Kanban is a **way for visually organising work items** (issues in our case). Kanban consists of **columns** and **cards**, where cards are the work items and are **moved from one column to another** depending on the situation. This should sound **familiar**, because that is how [Boards](./projects.md) work.

In a projects that uses the kanban template, boards can access directly all of the available issues.

This tutorial focuses on the basics, but you can find a lot of information in [these sets of articles](https://www.atlassian.com/agile/kanban) from Atlassian.

## Create board

This template can be chosen when **creating a project**

![](./img/choose-classic-template.png)

and **from the `Create board` screen**.

![](./img/scrum-kanban-board.png)

## Backlog

In kanban, **the backlog is just another column in your board(s)**. It can be removed entirely.

![](./img/backlog-kanban.png)

