# Cards - How to use Jira

[`<<`](./) || [`< Filters`](./filters.html) || [`Projects >`](./projects.html)

<br>

## Contents

1. [Properties](#properties)
2. [Creating](#creating)
3. [Viewing](#viewing)
4. [How to use](#how-to-use)
   1. [Right side](#right-side)
   2. [Work log](#work-log)
   3. [Issue flagging](#issue-flagging)
   4. [Basic issue information](#basic-issue-information)

**Each issue is represented as an object with properties**. More specifically, these objects are called 'cards'.

![](./img/kanban-cards.png)

## Properties

Each card has the following properties:

![](./img/fields-cards.png)

- **Project** - the [project](./projects.html) this card is stored in

  ![](./img/project-create-card.png)

- **Issue type** - the basic purpose of an issue (Improvement, Bug, ...)

  ![](./img/issue-types.png)

  - ![Story](./img/story-type.png) - functionality or feature expressed as a goal, something like "As a user, I want to be able to pay for my order via PayPal"

  - ![Bug](./img/bug-type.png) - a problem/error

  - ![Epic](./img/epic-type.png) - a collection of issues, issues are linked through 'Epic link' (see below)
  
  - ![Task](./img/task-type.png) - a small piece of work

  - ![Sub-task](./img/sub-task-type.png) - an issue that is directly connected to another issue. This is used for when a given issue needs to be broken down into separate even smaller issues.

- **Summary** - a (required) brief explanation of the issue

  ![](./img/summary-create-card.png)

- **Description** - detailed explanation of the issue

  ![](./img/description-create-card.png)

- **Reporter** - person who created the issue

  ![](./img/reporter-create-card.png)

- **Assignee** - person who will work on the issue

  ![](./img/assignee-create-card.png)

- **Labels** - tags with which you can organise even more the issue

  ![](./img/label-create-card.png)

- **Epic link** - link of issue with type Epic

  ![](./img/epic-link-create-card.png)

## Creating

To create a card yourself, you need to **press the `Create` button on the top bar**.

![](./img/create-header.png)

## Viewing

**All** cards (issues) can be viewed from a [project](./projects.html)'s side menu, under 'Issues'.

![](./img/issues-sidebar.png)

Of course, they can also be viewed through **[filters](./filters.html)** and a **[project](./projects.html)'s board**.

## How to use

By default, opening a card presents, at the top, it's **key** (think of it as an ID),

![](./img/card-key.png)

it's **summary** below it,

![](./img/card-summary.png)

options for adding **attachments**, **subtasks**, **linking issues** and **adding more apps** (think of apps like extensions),

![](./img/card-attach-subtask.png)

following are the **description**, **actual attachments**, **subtasks**, **linked issues**, etc.,

![](./img/desc-att-sub-link.png)

and at the bottom you have the **activity**, consisting of **comments** on the issue,

![](./img/activity-comments.png)

**history** of things that have happened to that issue,

![](./img/activity-history.png)

and **work logs** (a bit later we talk about them in more detail).

![](./img/activity-work-log.png)

### Right side

To the right side, you have all of the different **properties** (some fields without a value will be hidden).

![](./img/card-right-side.png)

On the top right you can change **watch options** ([watching an issue](https://www.idalko.com/jira-watch-issues/) means you get notified by all changes done to it), you can **share** the card and do **actions** (the three dots).

![](./img/card-top-right.png)

**Actions** are things you can do to the whole card, like moving or deleting it.

![](./img/card-actions.png)

The most notable options are [`Log work`](https://confluence.atlassian.com/jirasoftwarecloud/logging-work-on-issues-902499028.html) and `Add flag`.

### Work log

In Jira, you can **estimate how long an issue will take for it to be resolved**. This is done through **logging work**.

In the `Log work` screen, you'll be met with a `Time spent` and `Time remaining` fields, that you'll need to fill with the appropriate information.

![](./img/time-tracking.png)

The time remaining takes a **default** value from the **previous time log**, so you don't need to specify it every time.

### Issue flagging

A **flagged issue** is an issue that is very important. Flagged issues are displayed in yellow to signify their needed attention.

![](./img/flagged-card.png)

### Basic issue information

On the bottom right you can see **when the issue was created** and **when it was last updated**. You can also access the field configuration menu from the `Configure` button. It won't be covered in this tutorial, you can read about it [here](https://confluence.atlassian.com/adminjiracloud/changing-a-field-configuration-844500798.html).

![](./img/time-and-configure.png)

