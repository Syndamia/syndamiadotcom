# Dashboards - How to use Jira

[`<<`](./) || [`< Components`](./components.html)

<br>

## Contents

1. [Adding a dashboard](#adding-a-dashboard)
2. [Adding gadgets](#adding-gadgets)

Dashboards are a **place where you can view a lot of information**.

![](./img/default-dashboard.png)

Dashboards are composed of **gadgets**. 

![](./img/gadgets.png)

Gadgets have functionality for **displaying certain information**, like `Average Age Chart` or `Issues in progress`. Each gadget can be setup to **take information from one or more projects and/or saved filters**.

## Adding a dashboard

Dashboard can be added from the top bar, under `Create dashboard` in the `Dashboards` menu

![](./img/create-dashboard-top.png)

or, if you are already in a dashboard, from `Create dashboard` in the three dots menu.

![](./img/create-dashboard-dots.png)

## Adding gadgets

**Note**: you cannot add gadgets to the default dashboard.

You can add gadgets either **through the `Add gadget` button in the top right**

![](./img/add-gadget-top.png)

or, **if you don't have any gadets**, **from the `add a new gadget` message(s)**.

![](./img/add-a-gadget.png)

After that, you are greeted by a window that **shows all of the avalable ones** (you might need to **press `Load all gadgets`**).

![](./img/load-all.png)

![](./img/add-gadget.png)

Adding a gadget is as simple as clicking the button `Add gadget` next to the name of the gadget you want to add.

![](./img/introduction-add.png)

After it is added, you'll **need to configure it**, usually a gadget **requires atleast a filter/project**.

![](./img/bubble.png)

Options for different gadgets can be found on **their top right corner, in the three dots menu**.

![](./img/bubble-dots.png)

