# Bug tracking template - How to use Jira 

[`<<`](./) || [`< Scrum template`](./scrum.html) || [`Releases >`](./releases.html)

<br>

The Bug Tracking template is completely different from the [Scrum](./scrum.html) and [Kanban](./kanban.html) templates, because it **offers only issue tracking**.

**There are no boards, no cards, no backlog**. All issues are accessed from the `Issues` page from the sidebar.

![](./img/issues-sidebar.png)

That's why this template is **available only when creating a project**.

Of course, **you can add boards at any time**, but this project template doesn't add any by itself.

