# Scrum template - How to use Jira 

[`<<`](./) || [`< Kanban template`](./kanban.html) || [`Bug tracking template >`](./bug.html)

<br>

## Contents

1. [Backlog](#backlog)
2. [Boards and Issues](#boards-and-issues)
3. [Create board](#create-board)

The scrum template is very similar to the kanban template, with one big difference: **sprints**.

**All issues** are put into the **backlog** (which is now a **separate page**). From there, they are organised into **sprints**. 

In general [agile software development](https://www.atlassian.com/agile), a **sprint is a small duration of time** (one to four weeks) where the team focuses on a **set of tasks** and at the end everyone **evaluates the results** and plans the next sprint.

Your **boards only work on the issues of the currently active sprint**.

## Backlog

As we've discussed, the **backlog** is used for organising **all issues** into **sprints**.

![](./img/sample-scrum-backlog.png)

From here you can also organise **Versions** (refer to [Releases](./releases.html)) and **Epics** (refer to [Cards](./cards.html)). An important note is that Epics in this context don't need to have their linked issues in only one sprint.

![](./img/versions-and-epics.png)

Creating a sprint is done from the conveniently named `Create sprint` button. It shows a box on top where you can either move issues from the backlog to the sprint or create entirely new issues.

![](./img/sample-sprint.png)

## Boards and Issues

As previously mentioned, boards have access only to the issues in the active sprint. This is also reflected in the user interface. After selecting a board from the sidebar, you view it from the `Active sprints` page.

![](./img/active-sprints.png)

## Create board

This template can be chosen when **creating a project** 

![](./img/choose-classic-template.png)

and **from the `Create board` screen**.

![](./img/scrum-kanban-board.png)

