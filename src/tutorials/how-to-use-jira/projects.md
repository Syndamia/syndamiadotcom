# Projects - How to use Jira

[`<<`](./) || [`< Cards`](./cards.html) || [`Kanban template >`](./kanban.html)

<br>

## Contents

1. [Creating a project](#creating-a-project)
2. [Boards](#boards)
   1. [Columns](#columns)
   2. [Adding more boards to a project](#adding-more-boards-to-a-project)
3. [Backlog](#backlog)
4. [Add item](#add-item)

In short, **projects are a collection of issues** with added functionality for **organising them, getting statistics, etc**.

![](./img/projects.png)

## Creating a project

To create a project, either **click on `Create project` from the menu `Projects` in the top bar**,

![](./img/create-project-top.png)

or **click on the `Create project` button in the `View all projects` page**.

![](./img/view-all-projects-top.png)

![](./img/create-project.png)

**Note**: when you **have created your account** the first thing you'll see is the `Create project` page.

You'll be met by the `Choose project type` screen. From here choose `Classic project`, in this tutorial the `Next-gen project` types **won't be covered**.

![](./img/choose-project-type.png)

After that you'll need to put a **name** for your project, set a **key** (used as an ID for issues ; it is automatically generated, but you can change it if you want) and select a **template**.

![](./img/create-project-new.png)

This tutorial covers **only** the three classic software templates: [**Kanban**](./kanban.html), [**Scrum**](./scrum.html) and [**Bug tracking**](./bug.html).

![](./img/choose-classic-template.png)

In a nutshell, **templates** govern how **boards and issues should work together**. Later in the tutorial we cover the differences between these templates.

Projects also have [Releases](./releases.html), [Pages](./pages.html) and [Components](./components.html), that are also explored.

## Boards

Each project contains, amongst many other things, boards. **Boards are a way to better organise results from [filters](./filters.html)**. By default every project gets a board with a filter for all of the project's issues.

### Columns

**Boards are divided into columns** that contain the cards (issues). A card can move **freely** from one column to another.

![](./img/SOD.png)

To illustrate how this could be useful, let's say we have three columns: `TO DO`, `DOING` and `DONE`.

![](./img/todo-doing-done.png)

When you create an issue you initially put it in the `TO DO` column. After someone has started working on it, they put the issue in the `DOING` column. Finally, when the issue is resolved, it is put in the `DONE` column.

Of course, the name, quantity and purpose of different columns **depend on the project's organisation**.

Adding and removing columns can be done from the `Columns` tab in the `Board settings` menu (three dots on the top right of a board).

![](./img/board-settings.png)

![](./img/columns-edit.png)

### Adding more boards to a project

You can also add to a project as many [**Scrum**](./scrum.html) and [**Kanban**](./kanban.html) templated boards as you want.

This can be done, either while in a board, from **clicking the three dots and choosing `Create board`**

![](./img/create-board.png)

or **in the sidebar, from the `Board` menu**.

![](./img/sidebar-create-board.png)

After that, you are met with the `Create a board` window, where you can choose your template.

![](./img/scrum-kanban-board.png)

Once you've made your choice, you will be met with three options:

![](./img/board-from.png)

- **Board created with new Software project** - creates a new project with a new board (has the name of the project).

  ![](./img/npb.png)

- **Board from existing project** - creates a board that gets the filter from another project

  ![](./img/ntb.png)

- **Board from an existing Saved Filter** - as it says, you add to the project (indicated in the `Location` field) a board based on a saved filter

  ![](./img/ntb-filter.png)

## Backlog

**The backlog is a place for issues that outline a general task/feature**.

Depending on your project template, the backlog will either be a **separate window ([Scrum](./scrum.html))**,

![](./img/scrum-backlog.png)

**just a column in a board ([Kanban](./kanban.html))**,

![](./img/backlog-kanban.png)

or **nonexistent ([bug tracking](./bug.md))**.

The purpose of the backlog depends on the template and it is described in the page of each template.

## Add item

**You can also add some resources to the sidebar of your project** for everyone to use. That is done through the `Add Item` menu.

![](./img/add-item-kanban.png)

From here you can **add a website shortcut**, simply by putting the web address and giving it a name.

![](./img/add-item-to-project.png)

![](./img/add-shortcut.png)

The name is used for showing the shortcut on the sidebar.

![](./img/added-site.png)

You can also add a `Bitbucket` or `GitHub` repository which you can **leave** only as a **shortcut**, or you can **integrate** it so that **issues are transferred to Jira**.

![](./img/test-repo.png)

![](./img/test-repo-sidebar.png)

**Integration won't be covered in this tutorial**, but I can recommend [this](https://www.idalko.com/jira-github-integration/) article for `GitHub` integration and [this](https://www.idalko.com/jira-bitbucket-integration/) one for `Bitbucket` integration.

From the `Add Item` menu you can also **toggle the shortcut to [Pages](./pages.html)**.

