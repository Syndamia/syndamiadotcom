# Releases - How to use Jira

[`<<`](./) || [`< Bug tracking template`](./bug.html) || [`Pages >`](./pages.html)

<br>

The `Releases` page in a project shows **versions**, the ones in development and the ones that are completed.

**Versions are just a big collection of issues** and the **goal of a released version** is for **all of it's issues to be resolved**. It serves as a point in time for the development of a project.

## Adding issues to a version

When creating an issue, you can specify a version in the `Fix versions` property.

![](./img/fix-versions.png)

