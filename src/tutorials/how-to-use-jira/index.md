# Introduction - How to use Jira

[`Filters >`](./filters.html)

<br>

## Contents

1. [Filters](./filters.html)
   1. [Criteria](./filters.html#criteria)
   2. [JQL](./filters.html#jql)
2. [Cards](./cards.html)
   1. [Properties](./cards.html#properties)
   2. [Creating](./cards.html#creating)
   3. [Viewing](./cards.html#viewing)
   4. [How to use](./cards.html#how-to-use)
      1. [Right side](./cards.html#right-side)
      2. [Work log](./cards.html#work-log)
      3. [Issue flagging](./cards.html#issue-flagging)
      4. [Basic issue information](./cards.html#basic-issue-information)
3. [Projects](./projects.html)
   1. [Creating a project](./projects.html#creating-a-project)
   2. [Boards](./projects.html#boards)
      1. [Columns](./projects.html#columns)
      2. [Adding more boards to a project](./projects.html#adding-more-boards-to-a-project)
   3. [Backlog](./projects.html#backlog)
   4. [Add item](./projects.html#add-item)
4. [Kanban template](./kanban.html)
   1. [Create board](./kanban.html#create-board)
   2. [Backlog](./kanban.html#backlog)
5. [Scrum template](./scrum.html)
   1. [Backlog](./scrum.html#backlog)
   2. [Boards and Issues](./scrum.html#boards-and-issues)
   3. [Create board](./scrum.html#create-board)
6. [Bug tracking template](./bug.html)
7. [Releases](./releases.html)
8. [Pages](./pages.html)
9. [Components](./components.html)
   1. [Creating and editing](./components.html#creating-and-editing)
   2. [Adding issues](./components.html#adding-issues)
10. [Dashboards](./dashboards.html)
    1. [Adding a dashboard](./dashboards.html#adding-a-dashboard)
    2. [Adding gadgets](./dashboards.html#adding-gadgets)

[Jira](https://www.atlassian.com/software/jira) is one of the most used project management and issue tracking products out there.

It has a lot of features that can be quite overwhelming for new comers, especially these who are new to project management software. That's why I'm writing this tutorial.

**Note**: certain actions can be done only from a project administrator

