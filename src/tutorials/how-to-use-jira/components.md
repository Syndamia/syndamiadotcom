# Components - How to use Jira

[`<<`](./) || [`< Pages`](./pages.html) || [`Dashboards >`](./dashboards.html)

<br>

## Contents

1. [Creating and editing](#creating-and-editing)
2. [Adding issues](#adding-issues)

Components are **yet another way to group issues together**. They don't have any specific meaning like versions, their usage depends on the project's organisation.

## Creating and editing

Creating and editing them is done from the **`Components` menu in the sidebar**.

![](./img/components-sidebar.png)

Creating one is done through the button on the top right (in the center if you don't have any components) by the name of `Create component`.

![](./img/create-component-empty.png)

![](./img/create-component-full.png)

Clicking on it presents you with a screen where you input it's information.

![](./img/create-component-page.png)

**A name is the only requirement**. You can also add a **description**, **a lead(er) for the component** and **person who gets assigned to an issue by default**.

## Adding issues

Adding an issue to a component can be done either **when creating it** or **when viewing it**.

**Right under the summary**, there is a place for indicating a component, named `Components`.

![](./img/components-create-card.png)

When viewing an issue, you can add one from the `Components` field to the right (you might need to first click on `Show more fields`).

![](./img/show-fields.png)

![](./img/components-field.png)

