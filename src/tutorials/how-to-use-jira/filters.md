# Filters - How to use Jira

[`< Introduction`](./) || [`Cards >`](./cards.html)

<br>

## Contents

1. [Criteria](#Criteria)
2. [JQL](#JQL)

**Filters work like an advanced issue search that you can save**. With filters you filter issues from chosen project(s) by their properties (status, type, start date, etc.).

Making filters is done through the `Advanced issue search` page, from the `Filters` menu.

![](./img/filters-advanced-issue-search.png)

## Criteria

From here you can see some criteria: `Project`, `Type`, `Status`, `Assignee`.

![](./img/criteria-advanced-issue-search.png)

Clicking on the criteria will reveal what options you can choose for it. Access to more criteria can be obtained via the `+ More` menu.

![](./img/plus-more-advanced-issue-search.png)

After configuring your desired options, you can **save the filter** and use it later. To do so, simply click on `Save as` and enter a name for it.

![](./img/save-as-advanced-issue-search.png)
![](./img/save-filter-filters-advanced-issue-search.png)

You can view all of your filters from the conveniently named `View all filters` option in the `Filters` menu.

![](./img/view-all-filters-advanced-issue-search.png)

## JQL

You can do **more advanced search** through `JQL` (Jira Query Language), but we won't be covering this topic. I can recommend [this article](https://www.atlassian.com/blog/jira-software/jql-the-most-flexible-way-to-search-jira-14) on the subject.

