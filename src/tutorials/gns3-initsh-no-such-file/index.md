# Fix "exec /gns3/init.sh: no such file or directory"

This problem can easily occur on new installations and can be hard to figure out.
In a [GitHub issue](https://github.com/GNS3/gns3-server/issues/2069#issuecomment-1296741195), the solution that fixed it for me was using Debian's busybox:

0. Get into a temporary folder

   ```bash
   cd /tmp
   ```

1. Get and extract the Debian binary

   ```bash
   wget -q http://launchpadlibrarian.net/471755664/busybox-static_1.30.1-4ubuntu6_amd64.deb
   ar x busybox-static_1.30.1-4ubuntu6_amd64.deb
   tar -xf data.tar.xz
   ```

2. Move it to the proper location, your path may differ slightly

   ```bash
   sudo mv ./bin/busybox /usr/lib/python3.11/site-packages/gns3server/compute/docker/resources/bin/busybox
   ```
