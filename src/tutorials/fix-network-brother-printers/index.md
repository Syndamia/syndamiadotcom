# Fix networked Brother printers not working in Linux

Before we start, make sure you have CUPS and avahi installed and running.

## Make sure CUPS recognises it

Go to [localhost:631/printers](https://localhost:631/printers/) and make sure your printer is listed there.
If **not**, go to `Administration/Add Printer` and select your printer.
If it's **not** listed even there, then you have some other network connectivity issue.

Otherwise [try to print a test page](#try-to-print-a-test-page).

### Check if it **is** recognised on the network.

Execute in the command line:

```bash
avahi-browse --all -t -r
```

There will be a lot of output, but scroll down until you find something *like*:

```
= enp0s25 IPv4 Brother HL-L2350DW series                     _pdl-datastream._tcp local
hostname = [BRW9C305B9EC83D.local]
address = [192.168.0.110]
```

The exact values aren't important, just make sure the part after IPv4 is the model of your printer.
`address` is the local printer address, ping it.
In this example, the command would be:

```bash
ping 192.168.0.110
```

- If the ping is successful, you might also want to put that IP address in your browser.
  With the example, you would open the link `http://192.168.0.110`.
  Your printer **may** or **may not** support Web Based Management, which would allow you to edit it's setting from the browser.
  If the page isn't available, don't worry about it.

If output is **not** something like `64 bytes from ...` (so, if it is `Destination Host Unreachable` or ping just hangs, without output), then it's an issue with your network or printer network configuration.
Make sure the printer is properly connected to the network.  

Otherwise, this shows that your computer has network access to the printer, and all problems are local.

### Check if your printer is supported by Linux.

Go to [www.openprinting.org/printers](https://www.openprinting.org/printers) and enter your model.
If it isn't listed then it doesn't work for sure.
Alternatively, if it is and the rating [isn't "Perfectly"](https://wiki.linuxfoundation.org/openprinting/database/databaseintro#about_the_data), then read the whole page, in case networking is listed as nonfunctional.
If it **is** supported, then:

### Check if there are drivers available

Run in the command line:

```bash
lpinfo -v
```

If you only get entries like `network WORD`, without anything which looks like a URL (URI), then you may not have a working driver.
Make sure you have installed any or tried all of the drivers listed in the printer's [www.openprinting.org](https://www.openprinting.org/printers) page.

## Try to print a test page

If it is recognised, click on it on the "Queue Name" column, then press "Maintenance" and select "Print test page".
Go back and you should find an entry under "Active jobs listed in processing order", at the bottom of the page.
After a little while, the "State" should change from "pending" to "processing" (make sure to refresh the page).

### Unable to locate printer "SOMETHING.local"

If you also have the error "Unable to locate printer "SOMETHING.local"." (obviously SOMETHING is the exact value you get), then that means the printer is accessed via the domain name "SOMETHING.local".
`.local` domains require a special DNS server to be resolved, so inside `/etc/nsswitch.conf`, on the line that starts with `hosts`, add to it's end the value `mdns_minimal`.
It should look something like:

```
hosts:    files dns mdns_minimal
```

After saving your changes, your printer should pretty quickly start printing the test page, no need to restart anything.

- if your printer supports Web Based Management, then you should be able to go open the page `http://SOMETHING.local` in your browser and edit your printer settings from there.
