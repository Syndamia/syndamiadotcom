# Windows Run dialog

## Contents

1. [Opening the Run dialog](#opening-the-run-dialog)
2. [Commands](#commands)
   1. [Folders](#folders)
   2. [Programs](#programs)
   3. [Windows settings](#windows-settings)
3. [For more](#for-more)

On Windows, most often you run an application by finding and double-clicking it's `.exe` file. What a lot of people don't realise is that you can launch any application or open any file you want by typing a single command. That can be achieved inside the **Run dialog**.

## Opening the Run dialog

To open the Run dialog, you can either:

*A)* Press `Win + R` key combination

or

*B)* Right click on the Windows menu, and then press `Run`

![](./img/open-run-windows-menu.png)

## Commands

![](./img/run-dialog.png)

### Folders

You can open any folder by typing in it's **full path**. So, if you have a folder named `Work` inside your `Documents` folder, the full path will be something like `C:\Users\John\Documents\Work\`.

If your desired folder is directly in your account folder, so `Desktop`, `Documents`, `Pictures`, etc. you can just type in the name of the folder. For example, if you want to open your `Pictures` folder, just type in `pictures` and it will be opened.

There are, of course, some more special location, some of which are:

|Command|Location|
|---|---|
|`%ProgramFiles%`|Program Files Folder|
|`%AppData%`|User's App Data Folder|

**Bonus**: `%appdata%\Microsoft\Windows\Start Menu` will open the Start Menu folder, from which you can edit what goes inside your start menu.

### Programs

You can always run any application by typing in it's full path. So, to launch something like FireFox, you'll need to type in `C:\Program Files\Mozilla Firefox\firefox.exe`.

But, whats more important is that you can more easily launch some Windows apps, like Calculator or Control Panel. Here is a list of commands and the application they launch:

|Command|Application|
|---|---|
|`calc`|Calculator|
|`mspaint`|Microsoft Paint|
|`snippingtool`|Snipping Tool|
|`notepad`|Notepad|
|`sndvol`|Volume Mixer|
|`soundrecroder`|Sound Recorder|
|`wuapp`|Windows Updates|
|`shutdown`\*|Shutdown or restart computer|
|||
|`cmd`|Command Prompt|
|`powershell`|Windows PowerShell|
|`control`|Control Panel|
|`taskmgr`|Task Manager|
|`msinfo32`|System Information|
|`winver`|Windows Version Window|
|`cleanmgr`|Disk Cleanup|
|`dfrgui`|Disk Cleanup (defragmentation)|
|`mstsc`|Remote Desktop Connection|
|||
|`regedit`|Registry Editor|
|`msconfig`|System Configuration Settings|
|`dcomcnfg`|Component Services|
|`sigverif`|File Signature Verification Tool|

\* The `shutdown` command [requires parameters](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/shutdown):

- `/s` will shutdown in one minute
- `/r` will restart in one minute
- `/t` specifies after what amount of time to shutdown or restart (a number needs to be entered afterwards, so `/t 0` for immediate execution)
- `/f` forces running applications to close (might result in data loss!)

### Windows settings

There are also many "shortcuts" to different Windows settings windows. Those include:

|Command|Setting|
|---|---|
|`mmsys.cpl`|Sound Properties|
|`powercfg.cpl`|Power Options|
|`timedate.cpl`|Time and Date Options|
|`ncpa.cpl`|Network Connections|
|||
|`inetcpl.cpl`|Internet Properties|
|`compmgmt.msc`|Computer Manager|
|`devmgmt.msc`|Device Manager|
|`diskmgmt.msc`|Disk Management|
|`firewall.cpl`|Windows Firewall|
|||
|`sysdm.cpl`|System Properties|
|`services.msc`|Services|
|`comexp.msc`|Component Services|
|`gpedit.msc`|Group Policy Editor|
|`lusrmgr.msc`|Local Users and Groups|
|`certmgr.msc`|Certificate Manager|
|`taskschd.msc`|Task Scheduler|
|`fsmgmt.msc`|Shared Folders|

**Note**: `cpl` stands for *Control Panel Item* and `msc` stands for *Microsoft System Configuration*.

## For more

For more commands, check out [windowsruncommands.com](https://windowsruncommands.com/). They have a much bigger list of commands and shortcuts you can use.
