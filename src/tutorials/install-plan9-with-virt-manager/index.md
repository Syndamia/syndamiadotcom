# Install Plan 9 with virt-manager

## Contents

1. [Creating the virtual machine](#creating-the-virtual-machine)
2. [Installing Plan 9 itself](#installing-plan-9-itself)

Installing [Plan 9 by Bell Labs](https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs) with `qemu` itself isn't difficult and there are [official instructions](https://9p.io/wiki/plan9/Installing_Plan_9_on_Qemu/). However, when using `virt-manager`, there are certain uncertainties that might worry a first-timer and certain issues that arise.

## Creating the virtual machine

We'll be installing Plan 9 with a CD (ISO) image. You can download the latest version (compiled every night) from [here](https://9p.io/plan9/download.html). For a compressed version and checksums, refer to [this page](https://9p.io/wiki/plan9/Download/index.html).

1. Press the *"Create a new virtual machine"* and select *"Local install media (ISO image or CDROM)"*

2. For the install media select the extracted `.iso` CD image of Plan 9 and for operating system chose *"Generic or unknown OS (generic)"*

3. For memory select 512 MiB (the default 1024 MiB is also fine) and 1 CPU
   - Plan 9 is stupid lightweight and according to [the installation instructions](https://9p.io/wiki/plan9/Installation_instructions/index.html) you need 16-32 MB of memory, but I still prefer to give it a little more, just in case I decide to do something extreme.
   - You can also increase these values as much as you like, Plan 9 should be able to handle the more resources

4. If you plan to only test the OS out, choose *"Create a disk image for the virtual machine"* and enter 2 GiB of size. If you plan more serious use, you might want to follow [the qemu instructions](https://9p.io/wiki/plan9/Installing_Plan_9_on_Qemu/) and create a larger disk in the `.raw` format.

5. Finally click finish.

## Installing Plan 9 itself

If everything worked correctly, on boot you should see the screen in step 1.
From here on out, it's dead simple, and I even recommend just going through [Hatty Hacker's "Basic Plan 9 Installation" YouTube video](https://www.youtube.com/watch?v=NNWFTq0ZwLE).
I'll show a more abridged version of what you need to do.

1. On the boot screen press 1 and enter

   ![](./img/boot-screen.png)

2. On the questions "use DMA for ide drive" and "mouseport is" press enter

3. On "vgasize" press enter, or set up a higher (standard) resolution with 24 colors. Examples: `1920x1080x24`, `1280x720x24`
   - A very incomplete list can be found [here](https://9p.io/wiki/plan9/setting_the_right_monitor_size/index.html)

4. **Very important!** On "monitor" enter `vesa`. `xga` (or `vga`) will work during installation but seem to break when booting the installed OS.
   - If you mistakenly leave it at `xga`, refer to [this tutorial](/tutorials/fix-no-frame-buffer-plan9).

From here on out, you press enter at most of the prompts. The following list enumerates all cases where you need to do something else than press enter.

1. On "Disk partiton" question, enter `sdC0` (the option that says Hard Disk on it)

   ![](./img/partdisk-partition.png)

2. On "Install mbr" question, enter `y`

   ![](./img/partdisk-mbr.png)

3. When you get ">>>" before the cursor, press w, then enter, then q and finally enter. This should happen twice, when in `disk/fdisk` and `disk/prep`

   ![](./img/partdisk-fdisk.png)
   ![](./img/prepdisk-prep.png)

4. On "Distribution disk" type out the one that ends with `data` (and potentially has `cdrom` besides it). This is most commonly `/dev/sdC1/data`

   ![](./img/mountdist-distribution-disk.png)

5. When you get "/%" before cursor (during mountdisk task) just enter `exit`

   ![](./img/mountdist-browse.png)

6. On "Enable boot method" enter `plan9`

   ![](./img/bootsetup-boot-method.png)

After finishing the installation and seeing the message "Feel free to shutdown your computer", Force Off the virtual machine and then power it on again.

On boot, on the first prompt press enter, and on the second enter `glenda`. Enjoy Plan 9!

