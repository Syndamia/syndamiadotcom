# How to export Skype data

## Contents

1. [Downloading files](#downloading-files) 
2. [Viewing your export - Official Tool](#viewing-your-export---official-tool)
3. [Viewing your export - ArtemVeremienko Fork](#viewing-your-export---artemveremienko-fork)

Skype has always allowed to export your chat history (and files), but it's not as straight forward as it should. Continue reading for the required steps on doing it.

## Downloading files

1. Open [secure.skype.com/en/data-export](https://secure.skype.com/en/data-export)
   - You could be asked to sign in. After signing in you'll need to *open the link again*, since you would be redirected to the wrong place.
2. After opening the link you should be greeted by something like this. Check the two checkboxes next to **Conversations** and **Files** *(pointed to by the red arrows)*.

   ![](./img/initial-screen.png)
3. Press **Submit request**. After being greeted by this screen, press **Continue**.

   ![](./img/export-prepared.png)

   - You should now see an **Available exports** section near the top of the page. The status will be **Pending**.

     ![](./img/available-exports.png)
5. Reload the page after a bit of time. When your export is ready, you'll find a **Download** button next to it, click it.

   ![](./img/download-button.png)

Your export will be downloaded as a `.tar` archive file. You'll need an archiver utility to extract the files, something like [WinRAR](https://www.rarlab.com/download.htm) or [7-Zip](https://www.7-zip.org/) *(I personally recommend 7-Zip, it is completely free (no need to worry about your [infinite trial](https://youtu.be/fTgZRVVr3_Y)) and works faster)*.

## Viewing your export - Official Tool

1. Extract your archive
2. Open the [Skype Parser Download](https://go.skype.com/skype-parser), which will prompt you to save `skype-parser.zip`
3. Extract `skype-parser.zip`
4. Inside the `skype-parser` folder, open `index.html` (it might be named just `index`). You'll be greeted by this page in your browser:

   ![](./img/default-skype-parser-index.png)
5. Click the `Choose File` option and select the `messages.json` (it might be named just `messages`) file from your export.

   ![](./img/choose-file.png)
6. Press the `Load` button right below, and you're done. On the left side you have your conversations, and when opening one, your messages will be displayed to the right (just like in Skype itself)

   ![](./img/archived-conversations-skype-default.png)

There is only one issue with this method - the official tool is somewhat broken. Images and links are shown as formatting codes, but you can't actually look at them as you would in Skype itself.

The fix is using an alternative to the official tool.

## Viewing your export - ArtemVeremienko Fork

[ArtemVeremienko](https://github.com/ArtemVeremienko) is a GitHub user that took the official parser and fixed it. Their source code can be found over at [github.com/ArtemVeremienko/skype-parser](https://github.com/ArtemVeremienko/skype-parser).

1. Go to the [latest release of his skype-parser](https://github.com/ArtemVeremienko/skype-parser/releases/latest) and download `skype-parser.zip`

   ![](./img/github-skype-parser-download.png)
2. Extract all of the files. They'll be in the same folder as the `skype-parser.zip` file, so find the `index.html` file and open it.
3. Continue from step 5 onward of the [Viewing your export - Official Tool](#viewing-your-export---official-tool) section
