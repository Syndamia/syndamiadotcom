# Install drivers for Epson scanners

Sometimes your Epson scanner might work out of the box, and sometimes it might not.
First, make sure you have [SANE](http://sane-project.org/) installed on your machine[^gentoo-sane].
Second look through [the website](http://www.sane-project.org/sane-mfgs.html#Z-EPSON), most scanners are supported out of the box, but about 15% of them either aren't supported or are untested.

## Complete or Good

If your scanner falls into the first category, labeled "Complete" or "Good", you probably have an issue with SANE.
Make sure the `saned` service is running without errors[^openrc-sane]:

```bash
systemctl status saned.service
```

and make sure the [sane-epson2](http://www.sane-project.org/man/sane-epson2.5.html) (or [sane-epson](http://www.sane-project.org/man/sane-epson.5.html)) back-end is enabled, you should have `epson2` (or `epson`) without a leading `#` inside `/etc/sane.d/dll.conf`.

## epkowa plus non-free interpreter

If it's unsupported and there is a note

> supported by the epkowa backend plus non-free interpreter

you can still make it work.
You'll have to get the **non-free** (free as in [publicly available source code](https://en.wikipedia.org/wiki/Free_software_movement), you don't have to pay for anything) Epson Scan 2 software.

It can be downloaded from [the support site](https://support.epson.net/linux/en/epsonscan2.php) as a `deb` or `rpm` package.
If your system doesn't natively work with any of those (i.e. it isn't listed under "Distribution" and isn't some sort of derivative of those), you can still try the `rpm` package.

1. Install `rpm` with your package manager
2. Download the 32 or 64 bit **[ rpm ]** package
3. Extract it

   ```bash
   tar -xf epsonscan2-bundle-SOMETHING.rpm.tar.gz
   ```

4. Install the core application and non-free plugins with rpm

   ```bash
   sudo rpm -Uvh --nodeps ./epsonscan2-bundle-SOMETHING.rpm/core/epsonscan2-SOMETHING.rpm ./epsonscan2-bundle-SOMETHING.rpm/plugins/epsonscan2-non-free-plugin-SOMETHING.rpm
   ```

Alternatively, you can install the `deb` package with `dpkg` or build and install it from [source](https://download.ebz.epson.net/man/linux/epsonscan2_e.html#sec5-1-4) (source file is available at the bottom of the [support site](https://support.epson.net/linux/en/epsonscan2.php)).

You will need[^gentoo-epkowa] to install `iscan`, which usually provides the aforementioned `epkowa` backend.
Also make sure you have `epkowa` inside `/etc/sane.d/dll.conf` without a leading `#`.

Finally, only for the very first time after connecting your powered-on scanner, start "Epson Scan 2", select it and press "Next".
The drivers should automatically be installed and you can close the window and use your own scanning software of choice afterwards.

[^gentoo-sane]: On Gentoo the package is `sane-backends`, as per the [wiki](https://wiki.gentoo.org/wiki/SANE).
[^openrc-sane]: For OpenRC: `rc-service saned status`
[^gentoo-epkowa]: Surprisingly enough, on my Gentoo installation everything works fine without the package. Either something else is helping me or [something is in the "more magic" position](http://www.catb.org/jargon/html/magic-story.html)...
