# Fix "qc timeout" kernel log error

Depending on your harddrive, you may get a 15 second delay on boot.
Looking at the kernel logs (with `dmesg`), you may find the following errors:

```
kernel: ata1.00: qc timeout after 15000 msecs (cmd 0x47)
kernel: ata1.00: Read log 0x00 page 0x00 failed, Emask 0x40
kernel: ata1.00: NCQ Send/Recv Log not supported
kernel: ata1.00: Read log 0x00 page 0x00 failed, Emask 0x40
kernel: ata1.00: ATA Identify Device Log not supported
```

## Solution

Add a kernel parameter which tells Linux that the drive doesn't support DMA logs.  
Meaning, add the following parameter:

```
libata.force=1.00:nodmalog
```

where `1.00` is the thing between "ata" and ":".

So, if your logs were

```
kernel: ata3.01: qc timeout after 15000 msecs (cmd 0x47)
...
```

then the parameter would be:

```
libata.force=3.01:nodmalog
```

## Explanation

&:important The following is how I understand the issue, but I did not find definitive proof thats exactly what happens.  
            Take this section with a grain of salt.

A harddrive can support [dma transfers](https://en.wikipedia.org/wiki/Direct_memory_access), normally the CPU would handle data transfer from the harddrive to the system memory (RAM), meaning it asks for a bit of data, waits, when the data comes, puts it in memory, then asks for next bit and so on.
With DMA, the CPU could ask the drive for everythng it wants (not a tiny bit), and then the DMA controller (DMAC) on the drive will handle the actual data transfer from the drive to the memory (when the transfer is done, the drive will tell the CPU).
The benefit is that the CPU can do other work while the transfer is going on, greatly improving efficiency, but the downside is that the harddrive has to support it (have a DMAC chip on it).

Normally it would be useful for the DMAC to keep logs in a special place on the drive.
This is where the issue comes from: your harddrive supports DMA but does not support logs for it.
However, the Linux kernel has no good way of knowing that, there is no "property" on the drive which tells us this.
The only way is to try and read from that log: if it succeeds, then there is support, if it fails, then there isn't.

But what does it mean to fail?
The simplest definition would be for the drive to return an error singal, but in our case, the drive would just not return anything, ignoring our request.
In that case, how would we know if the drive just doesn't know how to respond (meaning it doesn't support the logs) or if it's just very slow?
Well, we can't, so we put a time limit, if it doesn't respond in 15 seconds, then it probably doesn't support it.

That's what the error is saying: `qc` is short for ["queued command"](https://docs.kernel.org/driver-api/libata.html#origins-of-commands), it's used to represent a (S)ATA command (hence the `cmd` at the end, telling us which command it's referring to).
So, the kernel is telling us "The hard drive (ATA) command to read (DMA) logs timed out after 15 seconds" (it first says a command timed out, then it says which one).

By adding that kernel parameter, we're telling Linux that the drive doesn't support DMA logs, therefore the kernel doesn't need to do this "wait 15 seconds for response if support exists" routine.
