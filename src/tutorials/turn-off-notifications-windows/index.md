# How to turn off notifications on Windows

What if you need to concentrate on some work, and you don't want to have your notifications show up while doing so? Or, what if some apps just put out notifications too often? Well, this is how to disable notifications:

1. In the *Search bar* type in **Notification**. The **Notification & actions settings** option should show up, click it

   ![](./img/windows-search-notifications.png)
2. If you want to turn off all notifications, switch the **Get notifications from apps and other senders** to the *Off* position

   ![](./img/windows-settings-notifications-off.png)

   *Note: "Suggest best ways", "Welcome Experience" and "Tips and tricks" will still show up, you'll have to uncheck those individually*

   If you want to turn off notifications for specific apps, scroll down to the **Get notifications from these senders**, find the applications you want to turn off notifications for and flick the switch

   ![](./img/windows-settings-notifications-application.png)

3. Finally, you can simply close the window
