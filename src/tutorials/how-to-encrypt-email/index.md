# How to encrypt email

## Contents

1. [Getting keys](#getting-keys)
   1. [PGP](#pgp)
      1. [pgptool.org](#pgptool.org)
      2. [GNU Privacy Guard](#gnu-privacy-guard)
   2. [S/MIME](#smime)
2. [Exchanging keys](#exchanging-keys)
3. [Configuring clients](#configuring-clients)

In [this](/blog/encrypted-email) blog post I shared the most common technologies for email encryption and a bit of how they work. PGP and S/MIME are the most popular schemes, with support from a lot of clients.

These are all the steps required to go from having no encryption, to encrypting all of your future emails.

## Getting keys

PGP and S/MIME are both built upon [public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) so you and all of your recipients need to have a pair of public and private keys.

### PGP

As [discussed](/blog/encrypted-email/#pgp), PGP relies on a [web of trust](https://en.wikipedia.org/wiki/Web_of_trust), meaning you can generate your keys by yourself.

#### pgptool.org

By far, the easiest way to do this is via the website [pgptool.org](https://pgptool.org/). Just open the link, fill out your details, select the recommended values (select never as your expiration time) and wait a couple minutes. After they are done, you can click the two red buttons to download your public and private keys.

#### GNU Privacy Guard

As an alternative, you can use a desktop program. The most popular one is [GNU Privacy Guard](https://www.gnupg.org/) (GPG for short). It is [available](https://gnupg.org/download/#binary) on most desktop operating systems, including [Windows](https://gpg4win.org/download.html) and [MacOS](https://gpgtools.org/).

The *Windows* version comes with a graphical user interface, so it's easiest to use that:

1. Open the application "Kleopatra"
2. Click on `File` -> `New Certificate`
3. Input your details and press `OK`
   - Feel free to click on `Advanced Settings` and look if if the value for `RSA` is `4096 bit`, the value for `+ RSA` is enabled and is `4096 bits`, and finally both `Signing` and `Authentication` are selected
4. On the next screen, press `Create`
5. You'll be asked for a password. Make one up, and enter it, but do not forget it!
   - The password is used for your private key. If anyone gets ahold if it, they **will** be able to impersonate you. A password makes sure that doesn't happen, at least until you tell everyone that your key was leaked.
6. On the final screen, click `Finish`
7. To help with the next chapter, we'll extract your public key. In "Kleopatra" you should see a line, containing your name and email. Right-click on it, and select `Export Certificate`.

On *Linux* and *MacOS*, you can use the command line:

1. Enter this command in your favorite terminal

   ```bash
   gpg --full-generate-key
   ```
2. Follow the prompt on the screen (sample pictures available in [encryptionconsulting.com](https://www.encryptionconsulting.com/how-to-generate-pgp-key-pair/))
3. Export your public key (of course, change me@email.com and the location to the one you want, but don't forget the .asc extension)

   ```bash
   gpg --armor --export me@email.com > ~/Documents/me-email.com-publickey.asc
   ```

### S/MIME

Contrary to PGP, S/MIME relies on a certificate authority, meaning you'll have to buy your keys from someone. Any provider from [Google](https://support.google.com/a/answer/7448393?hl=en&ref_topic=9061730)'s list of trusted CAs will work.

Currently, the only one that provides free certificates is [Actalis](https://www.actalis.com/s-mime-certificates.aspx). Their process is pretty straight forward, just follow the instructions (shown in [this](https://www.dannyguo.com/blog/how-to-get-a-free-s-mime-certificate/) article).

## Exchanging keys

You should now have a text file with your public key in it. When you want to send an encrypted email to someone, ask them for their public key, and meanwhile send them your public key. It doesn't matter how you do that, even if it's with unencrypted emails.

Remember, **never** send your **private** key to anyone!

## Configuring clients

Now we need to configure your email application to automatically encrypt and decrypt messages. There are a lot of email clients, so I have compiled the most used ones and linked resources on how to set them up.

When writing a message, don't forget to check if encryption is turned on for this email! On most clients, in the compose window, you'll have to select an option to enable encryption, but others do that automatically.

*Freemium means it has a free and a paid tier. Premium means you have to pay for it. FOSS means it's free and open source. FreemiumOSS means it is free and open source, but also has paid tiers.*

|Client     |Type       |Platform             |PGP/Inline |PGP/MIME   |S/MIME     |
|-----------|-----------|---------------------|-----------|-----------|-----------|
|**Web**    |           |                     |           |           |           |
|Gmail      |Freemium   |Web                  |[Yes](https://flowcrypt.com/)^[1](#1)|[Yes](https://flowcrypt.com/)^[1](#1)|[Yes](https://support.google.com/a/answer/6374496?hl=en&ref_topic=9061730)^[2](#2)|
|Outlook    |Freemium   |Web                  |No         |No         |[Yes](https://support.microsoft.com/en-us/office/encrypt-email-messages-373339cb-bf1a-4509-b296-802a39d801dc#bkmk_encryptwithsmime)^[2](#2)|
|iCloud mail|Freemium   |Web                  |[Yes](https://support.apple.com/en-us/HT201214)|[Yes](https://support.apple.com/en-us/HT201214)|[Yes](https://support.apple.com/en-us/HT202345)|
|Yahoo!     |Freemium   |Web                  |No         |No         |No         |
|Protonmail |Freemium   |Web                  |[Yes](https://proton.me/support/how-to-use-pgp)|[Yes](https://proton.me/support/how-to-use-pgp)|No|
|Roundcube  |Self-hosted|Web                  |[Yes](https://github.com/roundcube/roundcubemail/tree/master/plugins/enigma)^[1](#1)|[Yes](https://github.com/roundcube/roundcubemail/tree/master/plugins/enigma)^[1](#1)|[No](https://github.com/roundcube/roundcubemail/issues/4977)|
|Rainloop   |Self-hosted|Web                  |Yes        |[No](https://github.com/RainLoop/rainloop-webmail/issues/1798)|No|
|**Desktop**|
|Thunderbird|FOSS       |Windows, MacOS, Linux|[Yes](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq#w_i-have-never-used-openpgp-with-thunderbird-before-how-do-i-setup-openpgp)|[Yes](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq#w_i-have-never-used-openpgp-with-thunderbird-before-how-do-i-setup-openpgp)|[Yes](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq)|
|Outlook    |Premium    |Windows, MacOS       |No         |No         |[Yes](https://support.microsoft.com/en-us/office/encrypt-email-messages-373339cb-bf1a-4509-b296-802a39d801dc#bkmk_encryptwithsmime)        |
|Mailbird   |Premium    |Windows              |[No](https://mailbird.featureupvote.com/suggestions/74494/pgp-encryption-support)|[No](https://mailbird.featureupvote.com/suggestions/74494/pgp-encryption-support)|[No](https://mailbird.featureupvote.com/suggestions/160861/smime-support)
|Evolution  |FOSS       |Linux                |[Yes](https://help.gnome.org/users/evolution/stable/mail-encryption-gpg-set-up.html.en)        |[Yes](https://help.gnome.org/users/evolution/stable/mail-encryption-gpg-set-up.html.en)        |[Yes](https://help.gnome.org/users/evolution/stable/mail-encryption-s-mime-manage.html.en)        |
|Sylpheed   |FOSS       |Windows, MacOS, Linux|[Yes](https://sylpheed.sraoss.jp/doc/manual/en/sylpheed-8.html#ss8.2)        |[Yes](https://sylpheed.sraoss.jp/doc/manual/en/sylpheed-8.html#ss8.2)        |[No](https://www.sraoss.jp/pipermail/sylpheed/2011-May/004551.html)|
|Claws-mail |FOSS       |Windows, Linux       |[Yes](https://claws-mail.org/plugin.php?plugin=gpg)^[1](#1)|[Yes](https://claws-mail.org/plugin.php?plugin=gpg)^[1](#1)|[Yes](https://claws-mail.org/plugins.php)^[1](#1)|
|**Mobile** |
|Gmail      |Free       |Android, iOS         |No         |No         |Yes^[3](#3)|
|Outlook    |Free       |Android, iOS         |No         |No         |Yes^[3](#3)|
|Apple Mail |Free       |iOS                  |Yes        |Yes        |Yes        |
|Yahoo!     |Free       |Android, iOS         |No         |No         |No         |
|Edison     |Free       |Android, iOS         |No         |No         |No         |
|Protonmail |Free       |Android, iOS         |[Yes](https://proton.me/support/how-to-use-pgp)|[Yes](https://proton.me/support/how-to-use-pgp)|No|
|K-9 Mail   |FOSS       |Android              |[Yes](https://docs.k9mail.app/en/current/security/pgp/)|[Yes](https://docs.k9mail.app/en/current/security/pgp/)|[No](https://docs.k9mail.app/en/current/security/overview/#end-to-end-encryption)|
|FairEmail  |FreemiumOSS|Android              |[Yes](https://github.com/M66B/FairEmail/blob/master/FAQ.md)|[Yes](https://github.com/M66B/FairEmail/blob/master/FAQ.md)|[Yes](https://github.com/M66B/FairEmail/blob/master/FAQ.md)^[\*](#*)

<sup id="1">1</sup> Available with an extension/plugin  
<sup id="2">2</sup> Available in a paid version of the software (assuming the software is Freemium; this is true for all Premium software)  
<sup id="3">3</sup> Available with a Premium account (even if the software is Free)  
<sup id="*">\*</sup> You can read S/MIME messages, but signing and encrypting is a pro feature
