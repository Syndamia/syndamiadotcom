# Managing apps with ADB (and without root)

## Contents

1. [Installing](#installing)
2. [Opening a shell and connecting to your device](#opening-a-shell-and-connecting-to-your-device)
3. [Managing applications](#managing-applications)
4. [What to remove](#what-to-remove)

Often new phones, especially Samsung ones, come with a whole lot of bloatware, which usually cannot be uninstalled from within the phone.
This could be circumvated by rooting your phone, essentially gaining full administrative control over the device, however this process can be somewhat involved and could [cause issues](https://www.xda-developers.com/samsung-galaxy-z-fold-3-unlock-bootloader-broken-camera/).
A better solution is using the Android Debug Bridge (ADB), a developer tool with which you can more fluently control your device.
Of course, you still won't get full control, but it is as close as it gets, without requiring any permanent and involved configurations.

If you're having trouble, [XDA's article](https://www.xda-developers.com/install-adb-windows-macos-linux/), as well as [these](https://www.maketecheasier.com/uninstall-system-apps-without-root-android/) [maketecheasier](https://www.maketecheasier.com/get-started-with-android-debug-bridge-adb/) [articles](https://www.maketecheasier.com/enable-developer-options-in-android-42/) go into more detail and show pictures.

## Installing

Windows:

1. Download the official platform tools from [here](https://dl.google.com/android/repository/platform-tools-latest-windows.zip)
2. Extract the files in a folder and navigate to it

Linux (or MacOS):

- With your package manager, it should be called `android-tools`, `android-tools-adb` or `android-platform-tools`
- Worst case scenario, download and extract it from [here](https://dl.google.com/android/repository/platform-tools-latest-linux.zip) for Linux, and [here](https://dl.google.com/android/repository/platform-tools-latest-darwin.zip) for MacOS.

## Opening a shell and connecting to your device

ADB is very powerfull, so by default phones don't allow you to connect to the device with it.
First, we'll allow such a connection:

1. Go to `Settings/About phone` (depending on your Android skin, this might be located in a slightly different place)
2. Tap "Build number" 7 times, until a message says you're done
3. Navigate to the Developer options, which is often in `Settings/Developer options`, but could also be inside the `Settings/System` menu
4. Enable the "USB debugging option" (if a popup appears, click OK)

ADB is a command-line only tool, so first open a terminal inside the platform-tools directory:

- On Window, Right-click in the directory and press "Open PowerShell window here" or "Open command window here" or "Open in Terminal" (different OS versions show a different option)
- On Linux (and MacOS), open a terminal window and `cd` to the platform-tools directory

Connect your phone to the computer with a USB cable, change the transfer mode to "File transfer" and in the terminal enter:

```
adb devices
```

On your phone you should get a poppup asking "Allow USB debugging?", press OK.
You should get a "List of devices attached", and it should have one entry (a line below it, with some number and letters and then the word "device").

Now, we'll open a shell[^interactive-shell], a program with which to directly enter commands that the device understands:

```
adb shell
```

## Managing applications

There are 3 general commands that you would want to use:

```
pm list packages
```

Which prints the internal names of all applications that are installed on the device.
All of the other commands use those names (without the leading "package:", if it exists) for referring to applications.

```
pm uninstall -k --user 0 PACKAGE
```

Where PACKAGE is an aforementioned application name.
This command just uninstalls the app for the normal user, meaning it will be brought back on factory reset.

```
pm disable --user 0 PACKAGE
```

Disabled PACKAGE, so it wouldn't work but you wouldn't need to reinstall it (just enable it).
Beware that, if it shows some sort of error (anything that isn't "Success"), you'll probably need to use `disable-user`, rather than just `disable`.

After you're done, you can pretty much just unplug your device, but remember to **turn back off the "USB debugging" option!**

## What to remove

This is a lost of commands, which remove a whole lot of arguably unnecessary apps.
Use **AT YOUR OWN RISK!**
Although they shouldn't, they might cause problems **FOR WHICH I'M NOT RESPONSIBLE!**

- Facebook

```
pm uninstall -k --user 0 package:com.facebook.services
pm uninstall -k --user 0 package:com.facebook.katana
pm uninstall -k --user 0 package:com.facebook.system
pm uninstall -k --user 0 package:com.facebook.appmanager
```

- Netflix

```
pm uninstall -k --user 0 com.netflix.partner.activation
pm uninstall -k --user 0 com.netflix.mediaclient
```

- Microsoft apps

```
pm uninstall -k --user 0 com.microsoft.skydrive
pm uninstall -k --user 0 com.skype.raider
pm uninstall -k --user 0 com.microsoft.office.excel
pm uninstall -k --user 0 com.microsoft.office.word
pm uninstall -k --user 0 com.microsoft.office.powerpoint
```

- Google apps

```
pm uninstall -k --user 0 com.google.android.googlequicksearchbox
pm uninstall -k --user 0 com.google.android.gm
pm uninstall -k --user 0 com.google.android.apps.tachyon
pm uninstall -k --user 0 com.google.android.music
pm uninstall -k --user 0 com.google.android.apps.docs
pm uninstall -k --user 0 com.google.android.apps.maps
pm uninstall -k --user 0 com.android.chrome
pm uninstall -k --user 0 com.google.android.apps.photos
pm uninstall -k --user 0 com.google.vr.vrcore
pm uninstall -k --user 0 com.google.android.youtube
pm uninstall -k --user 0 com.google.android.videos
pm uninstall -k --user 0 com.google.android.talk
pm uninstall -k --user 0 com.google.android.apps.books
pm uninstall -k --user 0 com.google.android.apps.magazines
pm uninstall -k --user 0 com.google.android.apps.plus
```

- Windows 10 Phone comanion

```
pm uninstall -k --user 0 com.microsoft.appmanager
```

- AR Emoji

```
pm uninstall -k --user 0 com.samsung.android.aremoji
```

- Bixby

```
pm uninstall -k --user 0 com.samsung.android.bixby.agent
pm uninstall -k --user 0 com.samsung.android.bixby.es.globalaction
pm uninstall -k --user 0 com.samsung.android.bixbyvision.framework
pm uninstall -k --user 0 com.samsung.android.bixby.wakeup
pm uninstall -k --user 0 com.samsung.android.bixby.plmsync
pm uninstall -k --user 0 com.samsung.android.bixby.voiceinput
pm uninstall -k --user 0 com.samsung.systemui.bixby
pm uninstall -k --user 0 com.samsung.android.bixby.agent.dummy
pm uninstall -k --user 0 com.samsung.android.app.settings.bixby
pm uninstall -k --user 0 com.samsung.systemui.bixby2
pm uninstall -k --user 0 com.samsung.android.bixby.service
pm uninstall -k --user 0 com.samsung.android.app.routines
pm uninstall -k --user 0 com.samsung.android.visionintelligence
pm uninstall -k --user 0 com.samsung.android.app.spage
```

- Samsung Pass/Pay

```
pm uninstall -k --user 0 com.samsung.android.samsungpassautofill
pm uninstall -k --user 0 com.samsung.android.authfw
pm uninstall -k --user 0 com.samsung.android.samsungpass
pm uninstall -k --user 0 com.samsung.android.spay
pm uninstall -k --user 0 com.samsung.android.spayfw
```

- Samsung Kids

```
pm uninstall -k --user 0 com.samsung.android.kidsinstaller
pm uninstall -k --user 0 com.samsung.android.app.camera.sticker.facearavatar.preload
```

- Samsung Game Launcher

```
pm uninstall -k --user 0 com.samsung.android.game.gamehome
pm uninstall -k --user 0 com.enhance.gameservice
pm uninstall -k --user 0 com.samsung.android.game.gametools
pm uninstall -k --user 0 com.samsung.android.game.gos
```

- Galaxy Friends

```
pm uninstall -k --user 0 com.samsung.android.mateagent
```

- Samsung Smartthings

```
pm uninstall -k --user 0 com.samsung.android.easysetup
pm uninstall -k --user 0 com.samsung.android.beaconmanager
pm uninstall -k --user 0 com.samsung.android.oneconnect
```

- Carmode

```
pm uninstall -k --user 0 com.samsung.android.drivelink.stub
```

- Stickers

```
pm uninstall -k --user 0 com.sec.android.mimage.avatarstickers
```

- Samsung Global Goals

```
pm uninstall -k --user 0 com.samsung.sree
```

- Samsung Members

```
pm uninstall -k --user 0 com.samsung.android.voc
```

- Samsung LED cover

```
pm uninstall -k --user 0 com.samsung.android.app.ledbackcover
pm uninstall -k --user 0 com.sec.android.cover.ledcover
```

- Samsung TV

```
pm uninstall -k --user 0 com.sec.android.app.withtv
```

- Knox

```
pm uninstall -k --user 0 com.samsung.knox.securefolder
pm uninstall -k --user 0 com.knox.vpn.proxyhandler
pm uninstall -k --user 0 com.samsung.knox.keychain
pm uninstall -k --user 0 com.sec.enterprise.knox.cloudmdm.smdms
pm uninstall -k --user 0 com.samsung.android.knox.analytics.uploader
pm uninstall -k --user 0 com.sec.enterprise.knox.attestation
pm uninstall -k --user 0 com.samsung.android.knox.containercore
```

- Samsung DEX

```
pm uninstall -k --user 0 com.sec.android.desktopmode.uiservice
pm uninstall -k --user 0 com.samsung.desktopsystemui
pm uninstall -k --user 0 com.sec.android.app.desktoplauncher
```

- Samsung Browser

```
pm uninstall -k --user 0 com.sec.android.app.sbrowser
pm uninstall -k --user 0 com.samsung.android.app.sbrowseredge
```

- Samsung Email

```
pm uninstall -k --user 0 com.samsung.android.email.provider
pm uninstall -k --user 0 com.wsomacp
```

- Samsung Smartthings

```
pm uninstall -k --user 0 com.samsung.android.easysetup
pm uninstall -k --user 0 com.samsung.android.beaconmanager
pm uninstall -k --user 0 com.samsung.android.oneconnect 
```

- Gear VR

```
pm uninstall -k --user 0 com.samsung.android.hmt.vrsvc
pm uninstall -k --user 0 com.samsung.android.app.vrsetupwizardstub
pm uninstall -k --user 0 com.samsung.android.hmt.vrshell
pm uninstall -k --user 0 com.google.vr.vrcore
```

- Printings

```
pm uninstall -k --user 0 com.android.bips
pm uninstall -k --user 0 com.google.android.printservice.recommendation
pm uninstall -k --user 0 com.android.printspooler
```

- Upday news

```
pm uninstall -k --user 0 de.axelspringer.yana.zeropage
```

- Verizon

```
pm uninstall -k --user 0 com.vcast.mediamanager
pm uninstall -k --user 0 com.samsung.vmmhux
pm uninstall -k --user 0 com.vzw.hss.myverizon
pm uninstall -k --user 0 com.asurion.android.verizon.vms
pm uninstall -k --user 0 com.motricity.verizon.ssodownloadable
pm uninstall -k --user 0 com.vzw.hs.android.modlite
pm uninstall -k --user 0 com.samsung.vvm
pm uninstall -k --user 0 com.vznavigator.[You_Model_Here]
```

- T-mobile

```
pm uninstall -k --user 0 com.mobitv.client.tmobiletvhd
pm uninstall -k --user 0 us.com.dt.iq.appsource.tmobile
```

<br>

More information on available package commands can be shown with `pm help`.
You can learn more about the adb tool from [developer.android.com](https://developer.android.com/studio/command-line/adb).

[^interactive-shell]: You could just run every command like `adb shell COMMAND`, which wouldn't give you an interactive shell, so it is less convinient
