# virt-manager freezing up when clicking Browse in any window

The title explains what the problem is.
You might've also noticed that it spikes a CPU core's utilization to 100%.
Furthermore everything works if you enter a file location manually in the corresponding input field, it's just that darn button.

This issue is caused by some sort of problem with the storage pools[^storage-pools] when it tries to load them.
I was able to fix it by pretty much deleting that folder, but you really should just rename it:

```
sudo mv /etc/libvirt/storage /etc/libvirt/storage-broken
```

libvirt automagically regenerates the original folder, so you might need to restart it:

```
sudo systemctl restart libvirtd
```

[^storage-pools]: The storage pool is just a folder with files that store what locations can be used by virtual machines (more info [here](https://libvirt.org/storage.html)). In the case of virt-manager, pretty much every time you select any folder or file, a folder entry gets added there.
