# How to remove Windows Weather Widget

From June 2021 Microsoft introduced the `Weather Widget` on Windows 10. Given that it takes up space on the taskbar [and is a huge mess](https://www.howtogeek.com/734461/windows-10s-weather-widget-is-a-mess.-is-windows-11-next/), you might want to remove it.

This is how:

1. Right-click on an empty spot in the taskbar

   ![](./img/windows10-taskbar.png)
2. Go up to *News and interests*, and then click *Turn off* in the popped up window

   ![](./img/remove-newsweather-windows10.png)
