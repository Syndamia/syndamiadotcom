# Accessibility

I do personally want to make sure the content on this website is consumable by people with disabilities and/or with poor internet connection and browser speed.
However, at the same time, I want to add any feature that I feel would improve the website experience, for everyone else.
This is how I reach a middle ground between both sides:

### Bare bone website version

At [tiny.syndamia.com](https://tiny.syndamia.com) I offer my website with almost all styling and JavaScript functionality removed.
You'll receive only the webpage text and asset contents, and nothing more.

From my testing, pages usually load two times faster on tiny.syndamia.com.
I do also hope that without the styling and JavaScript, people with impairments will have an easier time either viewing the content or configuring their helping tools.

### Consumable content with everything blocked

By design, all "normal" pages[^normal-pages] can be viewed without any JavaScript, any styling and any custom font.
All of those are meant to make the browsing experience prettier and easier for those that can benefit from it and allow you to change and block certain features without the content being obstructed.
Even my [talks](/talks), where each one is a collection of delicately crafted CSS and JS slides, can be viewed as any normal text article on my site.

### Reasonable icons

First, icons on this website are managed with an icon font.
I've watched [Seren Davies' talk](https://www.youtube.com/watch?v=9xXBYcWgCHA), but I've found working with SVGs noticeably increases workload on my end.

**However**, I've still implemented those icons with accessibility in mind:

- Navigation is done with text entries in the sidebar, not icons.
- Buttons for other functionalities use text to reference their action, rather than (only) an "image", most of the time.
  The only mission-critical one that isn't represented with text is the sidebar folding and expanding button, right next to "Home".
  But it is mapped to the standard up and down pointing triangle characters, so it should still be useable.
- Icons that aren't decorative are "mapped" to their closest standard Unicode symbol, so even without the font, their purpose should be fairly understandable.
  Furthermore, I make sure those Unicode symbols have been standardised since at least 1999.
- My font has only the icons I use, and compared to the SVG equivalent setup, it takes up roughly a third of the size.

[^normal-pages]: Meaning a page that has only text, images and video. If the page contains a JavaScript demo, it obviously cannot function entirely without JS.
