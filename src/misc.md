# Miscellaneous

Other types of content I've made, including documents, images, videos and web demos.

<br>

#### 2022

&.ft-text 19.09 &emsp; [Университетски бележки](/misc/universitetski-belejki)
: Различни материали, които съм правил през следването ми във ФМИ

&.ft-photo 16.07 &emsp; [Layout engines and browsers](/files/misc/Layout%20engines%20and%20browsers.svg)
: A diagram which roughly outlines the main layout engines throughout the web's history
