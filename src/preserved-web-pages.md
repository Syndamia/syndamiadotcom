# Preserved Web pages

Similarly to [preserved media](/preserved-media), these are web pages/sites with endangered archival, **not** made by me, that I've decided to host.

If you are an owner of something listed here, [contact me](/about) and I'll remove it immediately.

<br>

&.ft-browser 2005 &emsp; [Brilliant Button Maker](https://hob.syndamia.com/brilliant-button-maker/) by Luca Zappa &emsp; [source code](https://syndamia.com/files/hob/BrilliantButtonMaker-LucaZappa-updated.zip)
: A nice web button maker from the 2000s, written in PHP. Since it was no longer available, I found and fixed up most of the PHP that archive.org saved.
