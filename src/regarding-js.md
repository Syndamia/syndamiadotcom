# Regarding usage of JavaScript

On 20.02.2022 I decided to lift the "ban" on usage of JavaScript in this website. This gives me the freedom to implement some useful features for those that have it enabled. **Any content that doesn't depend on JavaScript will be perfectly consumable without it!**

Any user data that might be saved isn't, and never will be, used for purposes not listed here. There isn't, and won't ever be, any form of tracking or data collection that isn't required for some feature (listed here).

## Implemented features

|Script|Description|GitLab source|Size|Lines|
|---|---|---|---|---|
|[image-show.js](/scripts/image-show.js)|Click to zoom on images|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/image-show.js)|551B|24|
|[back-to-top.js](/scripts/back-to-top.js)|Automatically appearing button to scroll to top|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/back-to-top.js)|378B|15|
|[styling-preload.js](/scripts/styling-preload.js)|Disable/enable certain stylesheets before page rendering|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/styling-preload.js)|385B|13|
|[theme-changer.js](/scripts/theme-changer.js)|Change theme between light and dark (split in two files)|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/theme-changer.js)|468B|18|
|[page-width.js](/scripts/page-width.js)|Change whether text covers the whole width of the page or only the middle (applicable to wide screens)|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/page-width.js)|640B|23|
|[expand-navbar.js](/scripts/expand-navbar.js)|Button to shrink and expand the sidebar|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/expand-navbar.js)|728B|20|
|[presentation.js](/scripts/presentation.js)|Presentation logic (button capabilities and other) **Loaded only on pages with presentations!**|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/presentation.js)|5.0KB|154|
|[keyboard.js](/scripts/keyboard.js)|Implements keyboard shortcuts|[Link](https://gitlab.com/Syndamia/syndamiadotcom/-/tree/master/src/scripts/keyboard.js)|810B|40|
||Total without presentation.js:||4.0KB|153|
||Total with presentation.js:||9.0KB|307|

## Data in browser storage

<p></p>

### Local storage

|Key|Values|Description|
|---|---|---|
|`theme`|`dark` \| `light`|Saves which theme should be loaded|
|`width`|`norm` \| `wide`|Saves in which width pages should be shown|
|`navbar`|`norm` \| `hidden`|Saves whether the navbar should be expanded or shrunk|

<p></p>
