/* Print button */

presentationStyle.cssRules[presentationStyle.cssRules.length - 1]
	.insertRule("article, body { border: none !important; margin: 0; max-width: none; padding: 0; }");
presentationStyle.cssRules[presentationStyle.cssRules.length - 1]
	.insertRule("nav, article > :not(.slide), #pagenav { display: none !important; }");
presentationStyle.cssRules[presentationStyle.cssRules.length - 1]
	.insertRule(".slide { height: 210mm; width: 297mm; margin: 0; }");

const printBtn = document.getElementById('printBtn');

printBtn.addEventListener('click', function() {
	window.print();
});

/* Next and previous buttons */

const prevSlideBtn = document.getElementById('prevSlideBtn');
const slideIndexShow = document.getElementById('slideIndex');
const nextSlideBtn = document.getElementById('nextSlideBtn');

const slides = document.getElementsByClassName('slide');
var presenting = false;
var slideIndex = parseInt(document.URL.substr(document.URL.indexOf("#slide") + 7));
if (isNaN(slideIndex)) slideIndex = 0;

slideIndexShow.innerText = slideIndex;
slideIndexShow.href = '#slide-' + slideIndex;

  // Shows the "slideIndex + mod" numbered slide
function showSlide(mod) {
	if (!presenting) return;

	slides[slideIndex].classList.remove('absolute-slide');
	slideIndex = (slides.length + slideIndex + mod) % slides.length;
	slides[slideIndex].classList.add('absolute-slide');

	slideIndexShow.innerText = slideIndex;
	slideIndexShow.href = '#slide-' + slideIndex;
}

prevSlideBtn.addEventListener('click', function() {
	showSlide(-1);
});

nextSlideBtn.addEventListener('click', function() {
	showSlide(+1);
});

/* Present button */

const presentCont = document.getElementById('presentCont');
const presentBtn = document.getElementById('presentBtn');
const articleBtn = document.getElementById('articleBtn');

var pageY = 0;
var pageX = 0;

function togglePresentMode() {
	if (presentationStyle.disabled) return;

	// Hide the whole page
	document.body.classList.toggle('invisible');
	document.body.classList.toggle('no-overflow');
	// Fix background color
	document.body.classList.toggle('body-bg-color');

	// Show the slide controller
	presentCont.classList.toggle('absolute-cont');
	for (var i = presentCont.children.length - 1; i >= 0; i--) {
		if (i == 4) continue; // presentBtn
		presentCont.children[i].classList.toggle('nodisplay');
	}
	presentBtn.classList.toggle('setting-btn');
	if (presentBtn.innerText[0] === 'P') {
		presentBtn.innerText = presentBtn.innerText.substr(8);
	} else {
		presentBtn.innerText = 'Present ' + presentBtn.innerText;
	}
	// Show the first slide
	slides[slideIndex].classList.toggle('absolute-slide');

	// Alignment of presentation to screen
	if (presenting) {
		window.scrollTo(pageX, pageY);
	}
	else {
		pageY = window.scrollY; pageX = window.scrollX;
		window.scrollTo(0, 0);
	}
	presenting = !presenting;
	fixAbsoluteSlideScale();
}
presentBtn.addEventListener('click', togglePresentMode);

function fixAbsoluteSlideScale() {
	var height = (presenting) ? window.innerHeight : articleElem.offsetHeight;
	var width  = (presenting) ? window.innerWidth  : articleElem.offsetWidth - 32;
	var scale = Math.min(height / slides[0].offsetHeight, width / slides[0].offsetWidth); 
	// Update .slide
	presentationStyle.cssRules[0].style.transform = 'scale(' + scale + ')';
	presentationStyle.cssRules[0].style.marginBottom = 'calc(1em - ' + slides[0].offsetHeight + 'px * ' + (1 - scale) + ')';
	// Update .absolute-slide
	presentationStyle.cssRules[1].style.transform = 'translate(-50%, -50%) scale(' + scale + ')';
	// Update .clicked-image
	presentationStyle.cssRules[2].style.transform = 'translate(-50%, -50%) scale(' + (1/scale) + ')';
}
window.addEventListener('resize', fixAbsoluteSlideScale);
window.addEventListener('load', fixAbsoluteSlideScale);

/* Show as article button */

function togglePresentationController() {
	printBtn.classList.toggle('nodisplay');
	presentBtn.classList.toggle('nodisplay');

	// Fix message
	if (articleBtn.innerText[0] === 'A')
		articleBtn.innerText = 'Slide' + articleBtn.innerText.substr(7);
	else
		articleBtn.innerText = 'Article' + articleBtn.innerText.substr(5);
}

function toggleArticleMode() {
	// Toggle presentation.css
	presentationStyle.disabled = !presentationStyle.disabled;

	// Add or remove "a" to url when in or not in article mode
	var href = window.location.href;
	var indqa = href.indexOf('?a');

	if (presentationStyle.disabled && indqa == -1) {
		var indquery = href.indexOf('?');
		if (indquery < 0) {
			var indend = Math.max(href.lastIndexOf('/'), href.indexOf('.html') + 4);
			href = href.slice(0, indend + 1) + '?a' + href.slice(indend + 1);
		}
		else {
			href = href.slice(0, indquery + 1) + 'a' + href.slice(indquery + 1);
		}
	}
	else if (!presentationStyle.disabled && indqa > -1) {
		href = href.slice(0, indqa) + href.slice(indqa + 2);
	}

	window.history.replaceState(history.state, document.title, href);

	togglePresentationController();
	fixAbsoluteSlideScale();
}
articleBtn.addEventListener('click', toggleArticleMode);

if (presentationStyle.disabled)
	togglePresentationController();
