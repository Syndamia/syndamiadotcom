/* Theme */
const darkThemeStyle = document.styleSheets[1];
if (localStorage.getItem('theme') === 'light')
	darkThemeStyle.disabled = true;

/* No JS */
const hideJSStylings = document.styleSheets[2];
hideJSStylings.disabled = true;

/* Presentation */
const presentationStyle = document.styleSheets[3];
if (window.location.href.indexOf('?a') > -1)
	presentationStyle.disabled = true;
