const widthBtn = document.getElementById('changeWidthBtn');
const widthLabel = widthBtn.childNodes[0];
const articleElem = document.getElementsByTagName('article')[0];

if (localStorage.getItem('width') === 'wide') {
	widthLabel.innerText = 'Narrow';
	articleElem.classList.add('article-wide');
}

widthBtn.onclick = function() {
	articleElem.classList.toggle('article-wide');

	if (localStorage.getItem('width') !== 'norm') {
		widthLabel.innerText = 'Wide';
		localStorage.setItem('width', 'norm');
	}
	else {
		widthLabel.innerText = 'Narrow';
		localStorage.setItem('width', 'wide');
	}

	fixAbsoluteSlideScale(); // From presentation.js
}
