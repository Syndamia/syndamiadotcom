document.addEventListener('keydown', function (e) {
	if (e.ctrlKey) return;

	switch (e.code) {

		case 'KeyC': // theme-changer.js
			themeBtn.click();
			break;
		case 'KeyW': // page-width.js
			widthBtn.click();
			break;
		case 'KeyE': // expand-navbar.js
			expandBtn.click();
			break;

		/* presentation.js */

		// Previous slide
		case 'KeyK': case 'Backspace': case 'PageUp':
		case 'ArrowLeft': case 'ArrowUp':
			showSlide(-1);
			break;
		// Next slide
		case 'KeyJ': case 'Space': case 'Enter': case 'PageDown':
		case 'ArrowRight': case 'ArrowDown':
			showSlide(+1);
			break;
		// Start (toggle) presenting
		case 'KeyP': case 'KeyT':
			togglePresentMode();
			break;
		// Exit presentation
		case 'Escape':
			if (presenting) togglePresentMode();
			break;
		// Go to first slide
		case 'Home':
			showSlide(-slideIndex);
			break;
		// Go to last slide
		case 'End':
			showSlide(-slideIndex -1);
			break;
	}
});
