const themeBtn = document.getElementById('changeThemeBtn');
const themeLabel = themeBtn.childNodes[0];

if (darkThemeStyle.disabled)
	themeLabel.innerText = 'Dark';

themeBtn.onclick = function() {
	darkThemeStyle.disabled = !darkThemeStyle.disabled;

	if (localStorage.getItem('theme') !== 'light') {
		themeLabel.innerText = 'Dark';
		localStorage.setItem('theme', 'light');
	}
	else {
		themeLabel.innerText = 'Light';
		localStorage.setItem('theme', 'dark');
	}
}
