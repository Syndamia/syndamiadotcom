const expandBtn = document.getElementById('expandNavbarBtn');
const wrapperElem = document.getElementById('content-wrapper');

if (localStorage.getItem('navbar') === 'hidden' || window.innerWidth <= 610 || window.innerHeight <= 610) {
	expandBtn.innerText = '\u25BC';
	wrapperElem.classList.add('content-wrapper-vertical');
}

expandBtn.onclick = function() {
	wrapperElem.classList.toggle('content-wrapper-vertical');

	if (!wrapperElem.classList.contains('content-wrapper-vertical')) {
		expandBtn.innerText = '\u25B2'; // triangle pointing up
		if (window.innerWidth > 610) localStorage.setItem('navbar', 'norm');
	}
	else {
		expandBtn.innerText = '\u25BC'; // triangle pointing down
		if (window.innerWidth > 610) localStorage.setItem('navbar', 'hidden');
	}
}
