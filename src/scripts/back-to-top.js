const scrollBtn = document.getElementById('scrollBtn');

function scrollBtnShow() {
	if (window.scrollY >= 800) {
		scrollBtn.style.display = 'block';
	} else {
		scrollBtn.style.display = 'none';
	}
}
window.addEventListener('scroll', scrollBtnShow);
window.addEventListener('load', scrollBtnShow);

scrollBtn.addEventListener('click', function() {
	window.scrollTo(0, 0);
});
