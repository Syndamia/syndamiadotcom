# Rework

Pages that have the message [**Due for Rework**](./rework.html) are in some form **outdated**. They might have bad formatting, bad wording, contain outdated information or something else is wrong with them.

In any case, **these pages shouldn't be taken at face value, as they will be updated and fixed soon enough**.

