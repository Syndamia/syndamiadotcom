# .*html
#
# == About ==
#
# ssg generates an id for every heading. That's why after every heading there
# can be a hyperlink to itself. However, because of the current styling, such a
# hyperlink is required (except for h1).

/[ \t]*<h[23456]/ {
	if (match($0, /<h2 id="contents"/) == 0) {
		match($0, /<h./)
		gap = "<div class=\"gap" ((substr($0, RSTART + 2, 1) == 2) ? "" : "-small") "\"></div>" ORS
	}
	match($0, /id="[^"]*"/)
	$0 = gap $0 ORS "<a class=\"h-anchor\" href=\"#" substr($0, RSTART + 4, RLENGTH - 5) "\" aria-hidden=\"true\">#</a>"
}
