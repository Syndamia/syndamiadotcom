# .*html
#
# == About
#
# This script forces all images with SVG source, which aren't alone, to have 
# a hard-coded height to them. The idea is to not make SVGs inside text take
# up the whole screen.
# This is mostly done for browsers that do not support CSS.

/<p>.+<img src="[^"]*\.svg"/ || /src="[^"]*\.svg"[^>]*>.+<\/p>/ {
	result = ""
	while(match($0, /src="[^"]*\.svg"/)) {
		result = result substr($0, 1, RSTART + RLENGTH - 1) " height=18";
		$0 = substr($0, RSTART + RLENGTH);
	}
	$0 = result $0;
}
