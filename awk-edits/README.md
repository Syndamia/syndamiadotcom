These AWK scripts are part of the 3rd "stage" in `generate.sh`, they edit the generated (by ssg and lowdown) HTML pages.
This allows for custom Markdown syntax and special formatting, for more info, read about [SS-Law](https://syndamia.gitlab.io/ss-law).

Without the `devs` argument, `generate.sh` concatenates all of them into one big file, where the order is dependant on the first character of the filename: at the top are those starting with "#", then those with capital letter and finally those with a lowercase letter (however there is no order between files with the same type of starting character).

For this reason:

- Files, starting with "#" are "libraries", just AWK scripts that contain only functions
- Files, starting with a capital letter, are (their code) executed before the line printing (more on that in a bit)
- Files, starting with a lower case letter, are (their code) executed after the line printing

This is why the following constraints have to be considered:

- If every script printed `$0`, then every line would be duplicated multiple times.
  That is why you **mustn't** print the current line in your own script, `generate.sh` passes the appropriate AWK code.
  If you don't want the line to be printed, do `$0 = 0`.
- Variable names between scripts must be unique, because when combined, unexpected behaviour might occur.
- `exit` **mustn't** be used!
  Instead, use a variable and then add a check in the "pattern" part for every rule where you would want to exit.

## ftags

ftags are strings/words in the Markdown source files, that start with "&&" and continue with any (valid) sequence of characters.
AWK scripts replace ftags with corresponding HTML.

List of scripts that handle ftags:

|Script|ftags|
|---|---|
|`File-lists.awk`|`&&folder`, `&&file`, `&&/files`, `&&fd`, `&&ft-TYPE` *(TYPE is one of `photo`, `text`, `zip`, `movie`, `markdown`)*|
|`Flags.awk`|`&&gb`, `&&bg`|
|`Presentation.awk`|`&&presentation`, `&&slide`, `&&/slide`, `&&clide`, `&&/clide`, `&&table`, `&&ftable`, `&&tarow`, `&&/table`, `&&linebN` *(N is one of 1, 2, ..., 9)*, `&&fontSIZE` *(SIZE is one of `18`, `20`, `22`, `25`, `27`, `35`)*, `&&/font`, `&&centered`, `&&/centered`, `&&fright`, `&&/fright`, `&&cont`, `&&/cont`|

Information regarding their usage can be found at the top of their respective script file.

The name initially stood for "file tags", since I got the idea while making the file lists functionality.
Now I like to think of them as "format tags", since they are used for so much more.
