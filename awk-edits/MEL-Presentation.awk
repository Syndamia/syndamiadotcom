# .*(slides|teaching)/.*html

BEGIN {
	usesMEL()
	#
	# Elements, dependent (and semidependent) on available font size
	#

	fontSizes[1] = "18"
	fontSizes[2] = "20"
	fontSizes[3] = "22"
	fontSizes[4] = "25"
	fontSizes[5] = "27"
	fontSizes[6] = "35"

	# Certain tags can exist without explicitly defining the font size
	# while others cannot
	# fontSizes[0] is used for simpler code for the tags which don't
	# require font size
	fontSizes[0] = ""

	# Tags which can exist without specified font-size
	for (ind in fontSizes) {
		fontClass = (ind != 0) ? " font" fontSizes[ind] : ""

		MEL["slide" fontSizes[ind], MELOPEN]  = "<div id=\"[SLIDEID]\" class=\"slide" fontClass "\">"
		MEL["slide" fontSizes[ind], MELCLOSE] = "</div>"
		MEL["middle"  fontSizes[ind], MELOPEN]  = "<div id=\"[SLIDEID]\" class=\"slide centered-slide" fontClass "\"><div></div><div>"
		MEL["middle"  fontSizes[ind], MELCLOSE] = "</div><div></div></div>"
		MEL["title" fontSizes[ind], MELOPEN]  = "<div id=\"[SLIDEID]\" class=\"slide centered-slide centered" fontClass "\"><div></div><div>"
		MEL["title" fontSizes[ind], MELCLOSE] = "</div><div></div></div>"

		standardPresentationTag("centered" fontSizes[ind], "centered" fontClass)
	}

	# refer to definition of fontSizes[0]
	delete fontSizes[0]

	# Tags which cannot exist without specified font-size
	for (ind in fontSizes) {
		standardPresentationTag("font" fontSizes[ind])
	}

	#
	# Special cases
	#

	MEL["presentation", MELINSERT] = ORS \
"<nav id=\"presentCont\">" ORS \
"	<span id=\"prevSlideBtn\" class=\"btn nodisplay\">&#x2190;</span>" ORS \
"	<a id=\"slideIndex\" class=\"nodisplay\" href=\"#slide-0\">0</a>" ORS \
"	<span id=\"nextSlideBtn\" class=\"btn nodisplay\">&#x2192;</span>" ORS \
"	<span id=\"articleBtn\" class=\"btn setting-btn\">Article view &#x2632;</span>" ORS \
"	<span id=\"presentBtn\" class=\"btn setting-btn\">Present &#x239A;</span>" ORS \
"	<span id=\"printBtn\" class=\"btn setting-btn\">Print &#x2399;</span>" ORS \
"		</nav><br><br>" ORS \
"		<script type=\"text/javascript\" src=\"/scripts/presentation.js\" crossorigin=\"anonymous\"></script>"

	MEL["row", MELOPEN]  = "<div class=\"layout-table\"><div class=\"layout-row\">" # <table><tr>  (layout-row > * is td)
	MEL["row", MELCLOSE] = "</div></div>"                                           # </tr><table>

	MEL["sqrow", MELOPEN]  = "<div class=\"layout-table fixed-layout\"><div class=\"layout-row\">" # <table><tr>  (layout-row > * is td)
	MEL["sqrow", MELCLOSE] = "</div></div>"

	MEL["newrow", MELINSERT] = "</div><div class=\"layout-row\">" # </tr><tr>

	#
	# Generated tags
	#

	for (i = 1; i <= 9; i++) {
		MEL["br" i, MELINSERT] = "<div class=\"br" i "\"></div>"
	}
	for (i = 5; i <= 95; i++) {
		standardPresentationTag("width" i "%", "width" i "per")
	}

	#
	# Ordinary
	#

	standardPresentationTag("fright", "float-right")
	standardPresentationTag("column")
}

function standardPresentationTag(name, class) {
	if (class == "") class = name
	MEL[name, MELOPEN]  = "<div class=\"" class "\">"
	MEL[name, MELCLOSE] = "</div>"
}

/\[SLIDEID\]/ {
	sub(/\[SLIDEID\]/, "slide-" slideIndex++)
}
