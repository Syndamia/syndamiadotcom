# .*(blog|slides|teaching|tutorials|misc)/.*/.*html
#
# == About ==
#
# I call pagenav the stuff directly below the first h1. These include file dates
# and GitLab source link.
# Currently it only works for blog and tutorial pages.
#
# This is by far the most hard-coded and inefficient awk script in awk-edits.
# Creation and last edited dates are taken from git commits, and getting the correct info
# by file name can only be done (easily) with git log, which is unfortunately very slow.
# Another issue is that it depends on the build directories in build scripts, meaning
# it won't work without modification if you try to build it somewhere else.
#
# Split into Pagenav.awk and post-pagenav.awk

nopagenav == 0 && /<h1/ {
	match(FILENAME, /(blog|slides|teaching|tutorials|misc)\/.*\//)
	path = substr(FILENAME, RSTART, length(FILENAME) - RSTART - 3)

	# Stats element
	stats = "<div id=\"pagenav-stats\">" ORS \
"	<strong>Reading: " (int(words / 180) + 1) " min</strong> || <strong>Created: " created "</strong> || <strong>Edited: " edited "</strong>" ORS \
"	|| <a href=\"https://gitlab.com/Syndamia/syndamiadotcom/-/blob/master/src/" path "md\"><strong>GitLab source</strong></a>" ORS \
"</div>"

	# Breadcrumbs element
	sub(/\/[^/]*$/, "", path)
	foldersLen = split(path, folders, "/")

	href = ".."
	breadcrumb = " &raquo; <em>" folders[foldersLen] "</em>"
	for (i = foldersLen-1; i > 0; i--) {
		breadcrumb = " &raquo; " "<a href=" href ">" folders[i] "</a>" breadcrumb
		href = "../" href
	}
	breadcrumb = \
"	<div id=\"pagenav-breadcrumb\">" ORS \
"		" breadcrumb ORS \
"	</div>"

	# Pagenav element
	$0 = $0 ORS \
"<div id=\"pagenav\">" ORS \
	stats ORS \
	breadcrumb ORS \
"</div>"

	nopagenav = 1
}
