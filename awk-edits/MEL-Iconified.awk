# .*html

BEGIN {
	usesMEL()
	#
	# Flags
	#

	melFlagTag("gb", "ENG")
	melFlagTag("bg", "BGN")

	#
	# Noteworthy paragraphs
	#

	melParTag("question",  "dialog-question.png")
	melParTag("warn",      "dialog-warning.png")
	melParTag("important", "dialog-important.png")

	#
	# File type
	#

	melFiletypeTag("ft-photo",    "&#xeb0a;")
	melFiletypeTag("ft-text",     "&#xeaa2;")
	melFiletypeTag("ft-zip",      "&#xed4e;")
	melFiletypeTag("ft-movie",    "&#xeafa;")
	melFiletypeTag("ft-markdown", "&#xec41;")
	melFiletypeTag("ft-browser",  "&#xebb7;")
}

function melFlagTag(name, alt) {
	MEL[name, MELINSERT] = "<img src=\"/img/" name ".svg\" class=\"flag unzoomable svg\" alt=\"" name "\" height=18>"
}

function melParTag(name, image) {
	MEL[name, MELPARAGRAPH] = "class=\"special-par " name "-par\"><img src=\"/img/" image "\" class=\"special-par-icon borderless unzoomable\" alt=\"" name "\">"
}

function melFiletypeTag(name, repl) {
	MEL[name, MELINSERT] = "<span class=\"file-type-icon\">" repl "</span>"
}
