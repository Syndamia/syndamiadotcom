function newStack(name) {
	__stacks[name, "length"] = 0
}

function topStack(name) {
	return __stacks[name, __stacks[name, "length"] - 1]
}

function lengthStack(name) {
	return __stacks[name, "length"]
}

function pushStack(name, value) {
	__stacks[name, __stacks[name, "length"]++] = value
}

function popStack(name, value) {
	delete __stacks[name, --__stacks[name, "length"]]
}

function deleteStack(name) {
	for (i = 0; i < __stacks[name, "length"]; i++) {
		delete __stacks[name, i]
	}
	delete __stacks[name, "length"]
}
