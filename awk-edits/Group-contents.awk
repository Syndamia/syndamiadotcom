# .*(blog|slides|teaching|tutorials|misc)/.*/.*html
#
# == About ==
#
# Some pages contain contents info, comprised of a "Contents" heading (h2) and a
# (usually ordered) list of anchors to headings in the current page.
# This script groups that heading and list into a wrapper element with id
# "contents-container", which can then be handled more specifically.
#
# In my case, the contents appear on the right of remaining text.

BEGIN {
	started = 0
}

/h2 id="contents"/ {
	print "<div id=\"contents-container\">"
	started = 1
}

started == 1 && /<p>/ {
	print "</div>"
	started = 0
}
