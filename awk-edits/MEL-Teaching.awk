# .*teaching/.*html

BEGIN {
	usesMEL()

	MEL["solution-link", MELLINEOPEN] = "<div class=\"exercise-solution\">&lt;"
	MEL["solution-link", MELLINECLOSE] = " &gt;</div>"
}
