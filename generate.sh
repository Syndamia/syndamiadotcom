#!/bin/bash

: ${REPO_FOLDER:=.}
: ${GEN_FOLDER:=./website}
: ${GEN_TINY_FOLDER:=./website.tiny}
: ${AWK_FOLDER:=./awk-edits}

# 0 Fix hardlinks {{{
	while read -r folder
	do
		if [ -f "$folder.md" ] && [ ! "$folder.md" -ef "$folder/index.md" ]
		then
			rm "$folder/index.md" >/dev/null 2>&1
			ln "$folder.md" "$folder/index.md"
		fi
	done << EOF
$(find "$REPO_FOLDER/src" -maxdepth 1 -type d)
EOF
	if [ ! "src/favicon.ico" -ef "src/img/favicon.ico" ]
	then
		rm "src/favicon.ico" >/dev/null 2>&1
		ln "src/img/favicon.ico" "src/favicon.ico"
	fi
	echo "[Stage 0] Fixed folder/index.md hardlinks"
# }}}
# 1. Markdown source to HTML {{{
	[ $# -eq 0 -a "$(id -u -n)" != "www-data" ] && echo "[Warning] Running in \"production\" by a user that isn't www-root!"
	
	[ ! -d "$GEN_FOLDER" ] && mkdir "$GEN_FOLDER"
	[ $# -eq 0 -a "$(stat -c %U "$GEN_FOLDER")" != "www-data" ] && echo "[Warning] Running in \"production\" while $GEN_FOLDER isn't owned by www-data!"
	
	/usr/bin/env dash ./csg/csg "$REPO_FOLDER/src" "$GEN_FOLDER" '& \\&mdash; Syndamia' 'https://syndamia.com' "$AWK_FOLDER"
# }}}
# 2. Tiny version, strip away styles and other stuff {{{
	[ ! -d "$GEN_TINY_FOLDER" ] && mkdir "$GEN_TINY_FOLDER"
	[ $# -eq 0 -a "$(stat -c %U "$GEN_TINY_FOLDER")" != "www-data" ] && echo "[Warning] Running in \"production\" while $GEN_TINY_FOLDER isn't owned by www-data!"

	# Just take what is already made, no need to regenerate everything
	cp -r "$GEN_FOLDER"/* "$GEN_TINY_FOLDER"

	# Remove unnecessary files
	rm -f  "$GEN_TINY_FOLDER"/*.css
	rm -rf "$GEN_TINY_FOLDER"/fonts/
	rm -f  "$GEN_TINY_FOLDER"/.files

	improveForTINY() {
		for file in $(find "$GEN_TINY_FOLDER" -type f -name "*.html"); do
			# Remove references to stylesheets in all files
			sed -i -E 's/^.*(rel="stylesheet|\<script|btn).*//g' $file
			sed -i 's/<span>Content/<br><span>Content/g' $file
			# Fix svg images being too big (scaling without limit)
			sed -i 's/<\/head>/\t<style>img[src$="svg"] { max-height: 1em; } #index-contents-img { max-width: 7.6em; }<\/style>\n<\/head>/' $file
		done
	}

	echo "[Stage 3] Tiny website"
	if [ $# -eq 0 ]; then
		improveForTINY

	
	else
		improveForTINY
	fi
# }}}
