#!/bin/bash
# This is a script for automatically generating my font.
# It will be ran only when I want to make changes to the font.

pushd .
cd /tmp

wget -N 'https://github.com/tabler/tabler-icons/releases/download/v2.10.0/tabler-icons-2.10.0.zip'
unzip -oj 'tabler-icons-2.10.0.zip' 'webfont/fonts/tabler-icons.ttf'

# I actually take the space and hyphen from noto-sans, because tabler-icons doesn't have it, and
# because it drastically helps with rendering on MacOS (and especially Safari)
wget -N 'https://www.fontsquirrel.com/fonts/download/noto-sans'
unzip -oj 'noto-sans' 'NotoSans-Regular.ttf'

cat << EOF > ./editfonts.ff
#!/usr/bin/fontforge
# Updates ./src/font/tabler-icons-subset.ttf font file
# Requires two arguments - filepaths for tabler-icons.ttf and NotoSans-Regular.ttf

# Icons on my website are just characters inside a special font.
# That font is a subset of the "Tabler Icons" font, version 2.10.0, containing only the glyphs I use
# https://github.com/tabler/tabler-icons
# The way I do it is oystersauce's idea
# https://stackoverflow.com/a/35754448/12036073
# Basically I select the icons I want, invert selection, then detach and remove
# all other icons.
# This is a FontForge script which automates the whole process.


#
# Tabler icons
#

Open(\$1)

    Select("uea19") # arrow-left
SelectMore("uea1f") # arrow-right
SelectMore("uf7d6") # arrow-badge-down-filled
SelectMore("uf7d9") # arrow-badge-up-filled
SelectMore("uf1e2") # article
SelectMore("uea66") # chevrons-up
SelectMore("ueaa2") # file-text
SelectMore("ued4e") # file-zip
SelectMore("ueaaa") # folder-minus
SelectMore("ueaab") # folder-plus
SelectMore("ueade") # link
SelectMore("uec41") # markdown
SelectMore("ueafa") # movie
SelectMore("ueb0a") # photo
SelectMore("ueb70") # presentation
SelectMore("ueb0e") # printer
SelectMore("uebb7") # browser

SelectInvert()
DetachAndRemoveGlyphs()

#
# Move characters from PUA to established glyphs
# This "makes" sure the character is useable even if font fails
# Only done to functional characters
#

# arrow-left to ← leftwards arrow
Select("uea19"); Cut(); Select("u2190"); Paste()

# arrow-right to → rightwards arrow
Select("uea1f"); Cut(); Select("u2192"); Paste()

# arrow-badge-down-filled to ▼ black down-pointing triangle
Select("uf7d6"); Cut(); Select("u25BC"); Paste()

# arrow-badge-up-filled to ▲ black up-pointing triangle
Select("uf7d9"); Cut(); Select("u25B2"); Paste()

# article to ☲ trigram for fire
Select("uf1e2"); Cut(); Select("u2632"); Paste()

# chevrons-up to ︽ presentation form for vertical left double angle bracket
Select("uea66"); Cut(); Select("uFE3D"); Paste()

# folder-minus to － fullwidth hyphen-minus
Select("ueaaa"); Cut(); Select("uFF0D"); Paste()

# folder-plus to ＋ fullwidth plus sign
Select("ueaab"); Cut(); Select("uFF0B"); Paste()

# link to § section sign
Select("ueade"); Cut(); Select("u00A7"); Paste()

# presentation to ⎚ clear screen
Select("ueb70"); Cut(); Select("u239A"); Paste()

# printer to ⎙ print screen symbol
Select("ueb0e"); Cut(); Select("u2399"); Paste()

#
# Noto-sans icons
#

Open(\$2)
    Select("u0020") # space
SelectMore("u002D") # hyphen
Copy()

Open(\$1)
    Select("u0020") # space
SelectMore("u002D") # hyphen
Paste()

Generate("tabler-icons-subset.ttf")
EOF
chmod +x ./editfonts.ff

./editfonts.ff 'tabler-icons.ttf' 'NotoSans-Regular.ttf'
popd
mv /tmp/tabler-icons-subset.ttf ./src/font/
